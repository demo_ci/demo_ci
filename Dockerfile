FROM spngtwreg.tigo.net.bo/htc/oracle-jdk:8u131
#FROM registry.gitlab.com/vhuezo/registry/htc/oracle-jdk:8u171

ARG VERSION
ARG JAR
ARG XML
ARG YML

ENV  VERSION=$VERSION \
     LOG_BASE="/app/logback" \
     PROP_BASE="/app/production" \
     APP_BASE="/app/" \
     JAR_FILE=tigo-ea-porta-debitcut-service-1.0.0-RELEASE.jar \
     LOG_FILE=porta-debitcut-service.production-logback.xml \
     PROP_FILE=porta-debitcut-service.production.yml

WORKDIR $APP_BASE
RUN chmod 755 /app && chown -R nobody:nobody /app

COPY ./build/libs/$JAR_FILE $APP_BASE/
#COPY $PROP_FILE $PROP_BASE/
#COPY $LOG_FILE $LOG_BASE/

#HEALTHCHECK --interval=1m CMD  curl --fail --silent  localhost:8035/ggsn/env || exit 1

#Dir for Home DEVOPS
USER nobody

EXPOSE 8029

#Command to execute the Java App Docker
CMD java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -Dapp.env=$ENVIRONMENT -Duser.timezone=America/La_Paz -Dapp.log=/app/log  -Dapplication.container.version=$VERSION -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dspring.boot.admin.url=http://admin-service:8006/$ENVIRONMENT/admin-boot -Dlogging.config=$LOG_BASE/$LOG_FILE -Dspring.config.location=$PROP_BASE/$PROP_FILE -jar $JAR_FILE
