package com.tigo.ea.porta.debitcut.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.retry.annotation.EnableRetry;
 
import com.tigo.ea.porta.debitcut.service.core.CacheBusinessManager;

@SpringBootApplication
@EnableRetry
@PropertySource("classpath:app.properties")
public class Application implements CommandLineRunner {
	
	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	@Autowired
	private Environment env;
	
	@Autowired
	private CacheBusinessManager cacheBusinessManager;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
	}
	
	@Override
	public void run(String... args) throws Exception {
		String applicationId = env.getProperty("application.id");
		String environment = env.getProperty("app.env");
		String version = env.getProperty("application.version");
		String app = String.format("%s-%s in %s", applicationId, version, environment);
		logger.info("{} Started Application", app);
		cacheBusinessManager.refreshCataloguesAll();
	}
	
	
}
