package com.tigo.ea.porta.debitcut.service.catalogue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum CatalogueBusiness {

	SATELITE("SateliteData"), PROCESSTYPE("ProcessTypeData"), CONFIGURATION_DATA("ConfigurationData");

	private String catalogue;

	private CatalogueBusiness(String catalogue) {
		this.catalogue = catalogue;
	}

	public String getCatalogue() {
		return catalogue;
	}

	public static CatalogueBusiness fromValue(String value) {
		for (CatalogueBusiness item : values()) {
			if (item.catalogue.equalsIgnoreCase(value)) {
				return item;
			}
		}
		throw new IllegalArgumentException(
				"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
	}

	private static final Map<String, CatalogueBusiness> lookup = new HashMap<>();

	static {
		for (CatalogueBusiness ti : CatalogueBusiness.values()) {
			lookup.put(ti.getCatalogue(), ti);
		}
	}

	public static CatalogueBusiness get(String catalogue) {
		return lookup.get(catalogue);
	}
}
