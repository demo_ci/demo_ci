package com.tigo.ea.porta.debitcut.service.catalogue;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

import java.util.Map;

/**
 * 
 * @author Alexander Castillo HTC
 *
 */
public enum CatalogueNPServImplRetryOperation {

	npDebtRequest(1);

	private int value;
	private static final Map<Integer, CatalogueNPServImplRetryOperation> map = stream(CatalogueNPServImplRetryOperation.values())
			.collect(toMap(catalogueBCCSRetryOperationEnum -> catalogueBCCSRetryOperationEnum.value,
					catalogueBCCSRetryOperationEnum -> catalogueBCCSRetryOperationEnum));

	public int getValue() {
		return value;
	}

	private CatalogueNPServImplRetryOperation(final int value) {
		this.value = value;
	}

	public static CatalogueNPServImplRetryOperation valueOf(int value) {
		return map.get(value);
	}

}
