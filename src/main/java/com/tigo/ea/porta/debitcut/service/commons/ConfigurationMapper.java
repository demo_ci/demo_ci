package com.tigo.ea.porta.debitcut.service.commons;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Alex
 *
 */
@Component
@ConfigurationProperties(prefix = "config-data")
public class ConfigurationMapper {

  private List<String> statusList;
  private List<String> prefixList;
  private List<String> statusListOther;
  private List<String> prefixADADOList;

  public ConfigurationMapper() {
    super();
  }

  public List<String> getStatusList() {
    return statusList;
  }

  public void setStatusList(List<String> statusList) {
    this.statusList = statusList;
  }

  public List<String> getPrefixList() {
    return prefixList;
  }

  public void setPrefixList(List<String> prefixList) {
    this.prefixList = prefixList;
  }

public List<String> getStatusListOther() {
	return statusListOther;
}

public void setStatusListOther(List<String> statusListOther) {
	this.statusListOther = statusListOther;
}

public List<String> getPrefixADADOList() {
	return prefixADADOList;
}

public void setPrefixADADOList(List<String> prefixADADOList) {
	this.prefixADADOList = prefixADADOList;
}
  
  

}