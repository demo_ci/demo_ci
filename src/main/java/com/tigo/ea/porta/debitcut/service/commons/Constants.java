package com.tigo.ea.porta.debitcut.service.commons;


public class Constants
{
	// REDIS
	public static final String REDIS_CONNECTION_IP = "redis.ip";
	public static final String REDIS_CONNECTION_PORT = "redis.port";
	public static final String REDIS_CHANNEL_NAME = "redis.channel.name";

	//LOGGER LEVEL
	public static final String CATEGORY_SERVICE = "service";
	public static final String CATEGORY_CONSUMER = "consumer";
	public static final String CATEGORY_TARGET = "target";
	
	public static final String OPERATOR_DONOR = "8903";
	//SQL
	public static final String QUERY_SATELITE= "sql.query.satelites";
	
	public static final String SERVICE_NPC_WSDL = "service.ascp.wsdl";
}
