package com.tigo.ea.porta.debitcut.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.htc.ea.connections.config.RabbitConnectConfig;
import com.htc.ea.util.configuration.UtilAppConfig;
import com.tigo.ea.porta.debitcut.service.listener.CacheBusinessListener;
import com.tigo.ea.porta.debitcut.service.util.HttpServletInterceptor;

@Configuration
@Import({ UtilAppConfig.class, WebServiceConfig.class, RabbitConnectConfig.class })
@ComponentScan({ "com.tigo.ea.porta.model" })
@EntityScan(basePackages = "com.tigo.ea.porta.model.dao")
@EnableJpaRepositories(basePackages = { "com.tigo.ea.porta.model.repository" })
public class AppConfig {

	static {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
//        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "false");
//        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "false");
//        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "false");
    }
	
	@Autowired
	Environment env;

	/**
	 * Cron scheduler satelite config.
	 *
	 * @return the task scheduler
	 */
	@Bean(name="cronSchedulerConfig")
    public TaskScheduler cronSchedulerSateliteConfig() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix("sateliteScheduler");
        scheduler.setPoolSize(15);
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        return scheduler;
    }


	@Bean
	public FilterRegistrationBean httpInterceptorFilter() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new HttpServletInterceptor());
		return registration;
	}

	
	@Bean
	public CacheBusinessListener cacheBusinessListener() {
		CacheBusinessListener cacheBusinessListener = new CacheBusinessListener();
		return cacheBusinessListener;
	}	
}