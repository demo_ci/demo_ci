package com.tigo.ea.porta.debitcut.service.config;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import com.tigo.ea.porta.debitcut.service.exception.ApplicationException;
import com.tigo.ea.porta.debitcut.service.listener.CacheBusinessListener;

/**
 * @author Alexcst
 *
 */
@Configuration
public class CacheBusinessConfig {

	private static final Logger logger = LoggerFactory.getLogger(CacheBusinessConfig.class);

	@Autowired
	public ChannelTopic channelTopic;
	@Autowired
	private RedisMessageListenerContainer redisContainer;
	@Autowired
	private CacheBusinessListener cacheBusinessListener;
	
	@Bean("messageListenerCacheBusiness")
	public MessageListenerAdapter messageListenerCacheBusiness() {
		return new MessageListenerAdapter(cacheBusinessListener);
	}
	
	@Bean
	public RedisMessageListenerContainer redisContainerAddLocal(@Qualifier("messageListener") MessageListenerAdapter messageListener,																
	 	    													@Qualifier("messageListenerCacheBusiness") MessageListenerAdapter messageListenerCacheBusiness) {
		try {
			logger.debug("Creating addListener cacheListenerUtil y cacheListenerBusiness to RedisContainer");
			redisContainer.addMessageListener(messageListener, Collections.singleton(channelTopic));
			redisContainer.addMessageListener(messageListenerCacheBusiness, Collections.singleton(channelTopic));
			logger.debug("Succesful addListener cacheListenerUtil y cacheListenerBusiness in RedisContainer");
			return redisContainer;
		} catch (Exception e) {
			throw new ApplicationException("Error addListener cacheListenerUtil in RedisContainer", e);
		}
	}
}
