package com.tigo.ea.porta.debitcut.service.config;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com._4gtss.npc.soap.service.consumer.NpcServiceImpl;
import com.tigo.ea.porta.debitcut.service.commons.Constants;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.debitcut.service.util.WSSecurityHeaderSOAPHandler;
import com.tigo.ea.porta.debitcut.service.util.WsAuthentication;
import com.tigo.ea.porta.model.util.HttpsProtocol;


@Configuration
public class NpcServiceImplConfig {

	@Autowired
	private Environment env;
	
	@Autowired
	private WsAuthentication wsAuthentication;
	
	@Autowired
	private LoggerUtil loggerUtil;
	
	@Bean
	@RefreshScope
	public NpcServiceImpl npcServiceImpl(){
		
		JaxWsPortProxyFactoryBean jaxWsPortProxyFactoryBean = new JaxWsPortProxyFactoryBean();
//		String wsdl = "http://localhost:8088/mockNpcServiceImplServiceSoapBinding?wsdl";
		String wsdl = env.getProperty("porta.service.npc.portability.wsdl");
		String endpoint = wsdl.replace("?WSDL", "");
		String targetNamespace = "http://implementations.service.soap.npc._4gtss.com/";
		String serviceName = "NpcServiceImplService";
		String portName = "NpcServiceImplPort";
		
		try {
			URL wsdlDocumentUrl = new URL(wsdl);
			HttpsProtocol.hostnameVerifier(wsdl, null, wsdlDocumentUrl, "NpcServiceImpl");											  
			jaxWsPortProxyFactoryBean.setServiceInterface(NpcServiceImpl.class);
			jaxWsPortProxyFactoryBean.setEndpointAddress(endpoint);
			jaxWsPortProxyFactoryBean.setNamespaceUri(targetNamespace);
			jaxWsPortProxyFactoryBean.setServiceName(serviceName);
			jaxWsPortProxyFactoryBean.setPortName(portName);
			jaxWsPortProxyFactoryBean.setWsdlDocumentUrl(wsdlDocumentUrl);
			jaxWsPortProxyFactoryBean.afterPropertiesSet();

		} catch (Exception e) {
			throw new RuntimeException("Error creating @Bean NpcServiceImpl WS impl with wsdl = " + wsdl, e);
		}
		
		String authorization = null;
		NpcServiceImpl port = (NpcServiceImpl) jaxWsPortProxyFactoryBean.getObject();
		try {
			// This is the block that apply the Ws Security to the request
            BindingProvider bindingProvider = (BindingProvider) port;
            @SuppressWarnings("rawtypes")
            List<Handler> handlerChain = new ArrayList<Handler>();
            handlerChain.add(new WSSecurityHeaderSOAPHandler(
            		env.getProperty(AppConstants.SERVICE_BCCS_PORTABILITY_USER),
            		env.getProperty(AppConstants.SERVICE_BCCS_PORTABILITY_PASS), env, loggerUtil));
            bindingProvider.getBinding().setHandlerChain(handlerChain);
            
            // Setting username & password
            Map<String, Object> req_ctx = bindingProvider.getRequestContext();
            req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsdl);

            authorization = wsAuthentication.getCredentialsAuthorization();
            if (authorization==null) throw new Exception("Error getCredentialsAuthorization");
            Map<String, List<String>> headers = new HashMap<>();
            headers.put("Authorization", Collections.singletonList(authorization));
            req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
		} catch (Exception e) {
			throw new RuntimeException("Error setting credentiales SSO in @Bean NpcServiceImpl " + authorization, e);
		}
		return port;
	}
}
