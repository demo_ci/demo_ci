package com.tigo.ea.porta.debitcut.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class RetryTemplateConfig {

	private static final String RETRY_EXECUTE_TRANSACTION_BACK_OF_PERIOD = "throughput.wssoap.delayretries.ascpConsumer";
	private static final String RETRY_DELAY_RETRIES = "throughput.wssoap.retries.ascpConsumer";
//	--throughput.wssoap.retries.ascpConsumer
//	--throughput.wssoap.delayretries.ascpConsumer
	/**
	   * Retry template.
	   *
	   * @return the retry template
	   */
	@Autowired
	private Environment env;
	
	  @Primary
	  @Bean(name= "retryServices")
	  @RefreshScope
	  public RetryTemplate retryTemplate() {
		  
	    FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
	    //Agregando constante backofperiod
	    fixedBackOffPolicy.setBackOffPeriod(env.getProperty(RETRY_EXECUTE_TRANSACTION_BACK_OF_PERIOD,Integer.class)); // in milliseconds

	    
	    SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
	    retryPolicy.setMaxAttempts(env.getProperty(RETRY_DELAY_RETRIES,Integer.class));
	    
	    RetryTemplate template = new RetryTemplate();
	    //agregando constante maxattemps
	    template.setRetryPolicy(retryPolicy);
	    template.setBackOffPolicy(fixedBackOffPolicy);

	    return template;
	  }
}
