package com.tigo.ea.porta.debitcut.service.consumer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.stereotype.Service;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com._4gtss.npc.soap.service.consumer.NPDebtRequestResponse;
import com._4gtss.npc.soap.service.consumer.NpcServiceImpl;
import com._4gtss.npc.soap.service.consumer.Reject;
import com._4gtss.npc.soap.service.consumer.RejectReason;
import com._4gtss.npc.soap.service.consumer.Status;
import com.tigo.ea.porta.debitcut.service.dto.AscpGenericResponse;
import com.tigo.ea.porta.debitcut.service.dto.ServiceSoap;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.commons.CatalogueMonitorStatus;

@Service
public class DefaultNpcServiceConsumer extends ServiceSoap implements NpcServiceConsumer {

	private static final Logger log = LoggerFactory.getLogger(DefaultNpcServiceConsumer.class);

	@Autowired
	private NpcServiceImpl npcServiceImpl;

	private final Class<?> clazz = this.getClass();

	private static final String CATEGORY = "service";

	@Autowired
	private LoggerUtil loggerUtil;

	@Autowired
	Environment env;

	// Envia solicitud al ascp
	public AscpGenericResponse npDebtRequest(NPDebtRequest1 request, Integer retry)
			throws Exception, RemoteAccessException {
		AscpGenericResponse response = null;
		String methodName = "npDebtRequestToAscp";
		try {
			NPDebtRequestResponse responseAscp = null;

			loggerUtil.info(AppConstants.CATEGORY_HANDLER_SOAP, request, clazz.getCanonicalName(), methodName,
					"Request", request.toString(), true, "0", 0L);
			// envio de solicitud al ascp
			responseAscp = npcServiceImpl.npDebtRequest(request);
			loggerUtil.info(AppConstants.CATEGORY_HANDLER_SOAP, responseAscp, clazz.getCanonicalName(), methodName,
					"Response", responseAscp.toString(), true, "0", 0L);
			response = new AscpGenericResponse();
			if (Status.SUCCESS.equals(responseAscp.getReturn().getStatus())) {
				// ASCP respondio EXITO
				log.info("Servicio npDebtRequest respondio con EXITO");
				loggerUtil.info(CATEGORY, responseAscp, clazz.getCanonicalName(), methodName, "Response Ascp",
						"Servicio npDebtRequest respondio con EXITO", true, "0", 0L);
				response.setStatus(responseAscp.getReturn().getStatus());
				response.setAck(responseAscp.getReturn().getAck());
			} else {
				// ASCP respondio ERROR
				log.info("Servicio npDebtRequest respondio con ERROR");
				loggerUtil.info(CATEGORY, responseAscp, clazz.getCanonicalName(), methodName, "Response Ascp",
						"Servicio npDebtRequest respondio con ERROR", true, "0", 0L);
				response = new AscpGenericResponse();
				response.setStatus(responseAscp.getReturn().getStatus());
				response.setNpOrderId(responseAscp.getReturn().getError().getNpOrderId() != null
						? responseAscp.getReturn().getError().getNpOrderId()
						: null);

				Map<String, List<RejectReason>> responseMap = new HashMap<>();

				List<Reject> rejectsList = responseAscp.getReturn().getError().getNpcRejectList().getRejections();

				// Iteramos la lista de rechazos
				rejectsList.forEach(reject -> {

					// Verificamos si los rechazos perteneces a la solicituda o a cada numero
					if (reject.getPhoneNumberStart() == null && reject.getPhoneNumberEnd() == null) {
						// si los numeros estan vacios las reject son de la solicitud
						responseMap.put("portRequest", reject.getRejectReasons());
					} else {
						// numero no vacios reject son del numero
						String msisdn = reject.getPhoneNumberStart() != null ? reject.getPhoneNumberStart()
								: (reject.getPhoneNumberEnd() != null ? reject.getPhoneNumberEnd() : null);
						responseMap.put(msisdn, reject.getRejectReasons());
					}
				});
				response.setRejectReasonMsisdnMap(responseMap);
			}

		} catch (RemoteAccessException remoteAccessException) {
			log.info("Servicio npDebtRequest respondio con TIMEOUT");
			loggerUtil.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR,
					"Servicio npDebtRequest respondio con TIMEOUT", 0L, remoteAccessException);
			throw new RemoteAccessException("REQUEST_ASCP_TIMEOUT");
		} catch (Exception e) {
			log.info("Servicio npDebtRequest respondio con ERROR DE EJECUCION");
			loggerUtil.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR,
					"Servicio npDebtRequest respondio con ERROR DE EJECUCION", 0L, e);
			throw new Exception(CatalogueMonitorStatus.ERROR_PROCESAR.getName());
		}
		return response;
	}
}