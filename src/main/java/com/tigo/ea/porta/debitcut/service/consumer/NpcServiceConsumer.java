package com.tigo.ea.porta.debitcut.service.consumer;

import org.springframework.remoting.RemoteAccessException;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com.tigo.ea.porta.debitcut.service.dto.AscpGenericResponse;

public interface NpcServiceConsumer {
	
	public AscpGenericResponse npDebtRequest(NPDebtRequest1 request, Integer retry) throws Exception, RemoteAccessException;
	
}
