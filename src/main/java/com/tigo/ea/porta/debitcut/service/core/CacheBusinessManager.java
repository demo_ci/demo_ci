package com.tigo.ea.porta.debitcut.service.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.ea.util.parsing.ParsingUtil;
import com.htc.ea.util.redis.dao.ConfigurationDataRepository;
import com.htc.ea.util.util.Constants;
import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.catalogue.CatalogueBusiness;
import com.tigo.ea.porta.debitcut.service.exception.ApplicationException;
import com.tigo.ea.porta.debitcut.service.scheduled.ScheduledConfigCronService;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.dto.SateliteDto;

/**
 * @author Alexcst
 *
 */
@Component
public class CacheBusinessManager {

	private final Class<?> clazz = this.getClass();
	private static final String EVENT_CATEGORY = "refresh-catalogue";

	@Autowired
	private Environment env;
	@Autowired
	private ParsingUtil parsingUtil;
	@Autowired
	private LoggerUtil logger;
	@Autowired
	private ConfigurationDataRepository configurationDataRepository;
	@Autowired
	private CatalogueBusinessService catalogueBusiness;

	@Autowired
	private ScheduledConfigCronService scheduledService;
	
	public void refreshCataloguesAll() {
		TransactionIdUtil.begin();
		for (CatalogueBusiness catalogue: CatalogueBusiness.values()) {
			refreshCatalogues(catalogue);
		}
		TransactionIdUtil.end();
	}
	
	public void refreshCatalogues(CatalogueBusiness catalogue) {
		switch (catalogue) {
		case SATELITE:
			refreshSatelite(catalogue);
			break;
		case CONFIGURATION_DATA:
			scheduledService.reloadConfigureCronTreads();
		case PROCESSTYPE:
			break;
		default:
			throw new IllegalArgumentException("Unknown enum type " + catalogue + ", Allowed values are "
					+ Arrays.toString(CatalogueBusiness.values()));
		}
	}

	public void refreshSatelite(CatalogueBusiness catalogue) {
		String methodName = "refreshSatelite";
		List<SateliteDto> sateliteList = new ArrayList<>();
		String json;
		String platform = env.getProperty(Constants.GENERAL_CONFIGURATION);
		String cat = catalogue.getCatalogue();
		try {
			json = (String) configurationDataRepository
					.findAllOtherObject(platform, cat);
			sateliteList = parsingUtil.convertJsonToList(SateliteDto.class, json);
		} catch (Exception e) {
			throw new ApplicationException("Error getting SateliteData from Redis => " + e.getMessage());
		}
		if (sateliteList == null) {
			sateliteList = new ArrayList<>();
			logging(methodName, String.format("SateliteData not ready in TargetPlatform: %s", env.getProperty(Constants.GENERAL_CONFIGURATION)));
		} else {
			catalogueBusiness.getCatalogueSatelite().clear();
			catalogueBusiness.getCatalogueSatelite().putAll(sateliteList.stream().collect(Collectors.toMap(SateliteDto::getId, Function.identity())));
			logging(methodName, String.format("SateliteData ready in TargetPlatform: %s", env.getProperty(Constants.GENERAL_CONFIGURATION)));	
		}
	}
	
	private void logging(String methodName, String detail) {
		logger.debug(EVENT_CATEGORY, null, clazz.getCanonicalName(), methodName, 
				"Refreshing Catalogue", detail, 
				 null, null, null);	
		
	}
}