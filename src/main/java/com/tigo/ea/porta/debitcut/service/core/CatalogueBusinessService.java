package com.tigo.ea.porta.debitcut.service.core;

import java.util.Map;

import com.tigo.ea.porta.model.dto.SateliteDto;
 

/**
 * @author Stanley Rodriguez - srodriguez@hightech-corp.com
 *
 */
public interface CatalogueBusinessService {
	
	public Map<Long, SateliteDto> getCatalogueSatelite();
	public SateliteDto getSatelite(Long id);
	
}