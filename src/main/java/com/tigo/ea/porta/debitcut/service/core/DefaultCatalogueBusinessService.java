package com.tigo.ea.porta.debitcut.service.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.tigo.ea.porta.model.dto.SateliteDto;

/**
 * @author AlexCst
 *
 */
@Service
public class DefaultCatalogueBusinessService implements CatalogueBusinessService {

	private Map<Long, SateliteDto> catalogueSateliteMap = new ConcurrentHashMap<>();

	public Map<Long, SateliteDto> getCatalogueSatelite() {
		return catalogueSateliteMap;
	}

	@Override
	public SateliteDto getSatelite(Long id) {
		return catalogueSateliteMap.get(id);
	}
}