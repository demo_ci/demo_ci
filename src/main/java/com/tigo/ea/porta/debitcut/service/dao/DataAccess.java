package com.tigo.ea.porta.debitcut.service.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.tigo.ea.porta.debitcut.service.dto.QueryCustomerDto;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.catalog.Satelite;
import com.tigo.ea.porta.model.commons.TransactionInfo;
import com.tigo.ea.porta.model.repository.NpcportingDetailRepository;

@Component
public class DataAccess {

	private static final String CATEGORY = "service";

	private final Class<?> clazz = this.getClass();

	@Autowired
	JdbcTemplate jdbcTemplate;

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private LoggerUtil logger;

	public List<Map<String, Object>> result(String sqlQuery) {
		List<Map<String, Object>> result = null;
		try {
			result = jdbcTemplate.queryForList(sqlQuery);
		} catch (Exception ex) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					ex.getMessage(), 0L, ex);
		}
		return result;
	}

	public List<TransactionInfo> resultList(String sqlQuery) {
		List<TransactionInfo> result = null;
		String methodName = "resultList";
		try {
			result = jdbcTemplate.query(sqlQuery, new BeanPropertyRowMapper<TransactionInfo>(TransactionInfo.class));
		} catch (Exception ex) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, ex.getMessage(), 0L);
		}
		return result;
	}

	public DataAccess(org.springframework.jdbc.core.JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	public Satelite autenticateSatelite(String sqlQuery) {
		Satelite sat = new Satelite();
		String methodName = "autenticateSatelite";
		try {
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sqlQuery);
			for (Map<String, Object> rs : rows) {
				sat.setRequestUser(String.valueOf(rs.get("requestUser")));
				sat.setRequestChannel(String.valueOf(rs.get("requestChannel")));
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L);
		}

		return sat;
	}

	@SuppressWarnings("unchecked")
	public QueryCustomerDto getNpcOrderDetailByMsisdn(String number, List<String> prefix) {
		final List<QueryCustomerDto> responseDb = new ArrayList<>();
		String methodName = "getNpcOrderDetailByMsisdn";
		QueryCustomerDto response = new QueryCustomerDto();
		responseDb.clear();

		try {
			String parameterMsisdn = "'" + number + "'";
			String paramPrefix = prefix.stream().collect(Collectors.joining("','", "'", "'"));
			// Obtenemos el query y agregamos el numero
			String querySql = NpcportingDetailRepository.GET_ORDERID_PORTACION.replace("msisdnList", parameterMsisdn);
			querySql = querySql.replace("prefixList", paramPrefix);
			Query query = em.createNativeQuery(querySql);
			List<Object[]> dataList = query.getResultList();
//			logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName, "Query Portacion", querySql,
//					dataList != null, "0", 0L);
			if(dataList != null) {
				dataList.forEach(row -> {
					response.setMsisdn(((BigInteger) row[0]).intValue());
					response.setNporderid(((String) row[1]));
					response.setProcessdate((Date) row[2]);
					response.setIdmaximo(((BigInteger) row[3]).intValue());
					response.setEventType((String) row[4]);
					response.setRangeholderid(String.valueOf(row[5]));
					response.setDonorid(String.valueOf(row[6]));
					response.setRecipientid(String.valueOf(row[7]));
					responseDb.add(response);
				});

				if (responseDb.isEmpty()) {
					return null;
				}
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
		}
		return response;
	}
}