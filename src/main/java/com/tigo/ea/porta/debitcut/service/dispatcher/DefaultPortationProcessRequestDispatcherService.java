package com.tigo.ea.porta.debitcut.service.dispatcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.ea.connections.beans.ConnectionData;
import com.htc.ea.connections.factory.ConnectionRabbitManager;
import com.htc.ea.jaxws.dto.AsyncUpdateTransactionComplete;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppServiceException;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

/**
 * The Class DefaultPortationProcessRequestDispatcherService.
 *
 * @author HTC-AlexCst
 */
@Service
public class DefaultPortationProcessRequestDispatcherService implements PortationProcessRequestDispatcherService  {

	private final Class<?> clazz = this.getClass();
	@Autowired
	private ConnectionRabbitManager connectionRabbitManager;

	@Autowired
	private LoggerUtil logger;
	@Autowired
	@Qualifier("appUtilLocal")
	private AppUtil appUtil;
	
	@Autowired
	private Environment env;
	
	@Override
	public void send(AsyncUpdateTransactionComplete transactionCompleteRequest, String messagingType) {
		String methodName = "send";
		try {
			ConnectionData messageDataSelected = appUtil.getMessagingDataSelected(connectionRabbitManager,
					messagingType);
			boolean success = appUtil.validateQueueProducer(connectionRabbitManager, messageDataSelected,
					messagingType);
			if (success) {
				connectionRabbitManager.getConnectionMessagingManager().get(messagingType).convertAndSend(
						messageDataSelected.getExchange(), messageDataSelected.getRoutingKey(), transactionCompleteRequest);
				logger.info("service", transactionCompleteRequest, clazz.getCanonicalName(), methodName, 
						"Despachador mensajes", "Success. La orden se envio a la cola." + messagingType, true, "0", 0L);
			}
		}catch(Exception e) {
			logger.error("service", clazz.getCanonicalName(), methodName,
					"Despachador mensajes",
					"Error, no fue posible enviar la orden a la cola"
							+ messagingType,0L, e);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_SERVICE),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_SERVICE), "No fue posible enviar notificacion al satelite"));
		}
	} 
}
