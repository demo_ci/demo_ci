package com.tigo.ea.porta.debitcut.service.dispatcher;

import com.htc.ea.jaxws.dto.AsyncUpdateTransactionComplete;

public interface PortationProcessRequestDispatcherService {

	public void send(AsyncUpdateTransactionComplete updateTransactionCompleteRequest, String messagingType);
	
}
