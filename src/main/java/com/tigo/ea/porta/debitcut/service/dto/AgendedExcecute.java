package com.tigo.ea.porta.debitcut.service.dto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com._4gtss.npc.soap.service.consumer.NpDebtRequest;
import com._4gtss.npc.soap.service.consumer.PhoneNumberList;
import com._4gtss.npc.soap.service.consumer.Range;
import com.htc.ea.jaxws.dto.AdditionalParameters;
import com.htc.ea.jaxws.dto.AsyncUpdateTransactionComplete;
import com.htc.ea.jaxws.dto.Country;
import com.htc.ea.jaxws.dto.Number;
import com.htc.ea.jaxws.dto.Reasons;
import com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequest;
import com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequestBody;
import com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequestHeader;
import com.htc.ea.jaxws.dto.Updatedate;
import com.htc.ea.util.configentry.ConfigurationService;
import com.htc.ea.util.parsing.ParsingUtil;
import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.dispatcher.DefaultPortationProcessRequestDispatcherService;
import com.tigo.ea.porta.debitcut.service.repository.OperationPortrequestRepository;
import com.tigo.ea.porta.debitcut.service.retry.NpDebtRequestRetry;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppServiceException;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.commons.CataloguePortRequestStatus;
import com.tigo.ea.porta.model.dao.Portnumber;
import com.tigo.ea.porta.model.dao.Portrequest;
import com.tigo.ea.porta.model.dao.Reason;
import com.tigo.ea.porta.model.dto.SateliteDto;

@Service
public class AgendedExcecute {

	public static final Logger log = LoggerFactory.getLogger(AgendedExcecute.class);

	private final Class<?> clazz = this.getClass();

	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private OperationPortrequestRepository operationPortrequestRepository;
	@Autowired
	private DefaultPortationProcessRequestDispatcherService sendRequest;
	@Autowired
	private Environment env;
	@Autowired
	private NpDebtRequestRetry npDebtRequestRetry;

	@Autowired
	private EvaluateResponseAscp evaluateResponseAscp;

	@Autowired
	private AppUtil appUtil;
	
	@Autowired
	private ParsingUtil parsingUtil;

	private static final String CATEGORY = "service";

	@Autowired
	private LoggerUtil logger;

	public void execute(Portrequest request) {
		DebitCutSendASCPResponse response = new DebitCutSendASCPResponse();
		List<String> validStatus = new ArrayList<>();
		List<String> validType = new ArrayList<>();
		String methodName = "execute";
		Portrequest portRequest = request;
		Boolean satNotification = false;
		SateliteDto satelite = null;
		List<Number> numberList = new ArrayList<>();
		try {
			if (log.isInfoEnabled())
				log.info(String.format("--------------------- Solicitud #: %s  Con estado: %s", portRequest.getId(),
						portRequest.getStatus()));
			if (this.inTime()) {
				satelite = appUtil.getSateliteById(portRequest.getSateliteId().getId());
				if (satelite == null) {
					log.info("Satelite no encontrado.");
					// appUtil.throwServiceException(ErrorConstants.ERROR_CODE_99); // Unexpected
					// Error
				}
				if (satelite != null)
					satNotification = satelite.getIsNotificationRequest();
				// El numero tiene otra solicitud en estado pendiente o cortado
				Portrequest requestToAscp = null;
				for (Portnumber currentNumber : portRequest.getPortnumberList()) {
					requestToAscp = operationPortrequestRepository.getPortrequestAscp(currentNumber.getMsisdn());
				}
				if (requestToAscp == null) {
					NpDebtRequest npDebtRequest = new NpDebtRequest();
					npDebtRequest.setOriginalNpOrderId(portRequest.getOriginalNporderId());
					String routingnumber = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
					String senderId = routingnumber.substring(2, 4);
					npDebtRequest.setSenderId(senderId);
					PhoneNumberList phone = new PhoneNumberList();
					List<Portnumber> portnumberList = null;
					portnumberList = portRequest.getPortnumberList();
					for (Portnumber currentNumber : portnumberList) {
						Range rango = new Range();
						rango.setPhoneNumberEnd(currentNumber.getMsisdn());
						rango.setPhoneNumberStart(currentNumber.getMsisdn());
						phone.getRanges().add(rango);
					}
					npDebtRequest.setOriginalNpOrderId(portRequest.getOriginalNporderId());
					npDebtRequest.setPhoneNumberList(phone);
					NPDebtRequest1 requestAscp = new NPDebtRequest1();
					requestAscp.setArg0(npDebtRequest);

					// Envio request al ascp
					AscpGenericResponse responseAscp = npDebtRequestRetry.npDebtRequestRetry(requestAscp);
					logger.info(CATEGORY, responseAscp, clazz.getCanonicalName(), methodName, "Response",
							responseAscp.toString(), true, "0", 0L);
					portRequest = evaluateResponseAscp.evaluateAscpResponseProcess(true, responseAscp, portRequest);

					for (Portnumber currentNumber : portRequest.getPortnumberList()) {
						Reasons reason = new Reasons();
						reason.setReason(portRequest.getStatus());
						List<Reasons> reasonlist = new ArrayList<>();
						reasonlist.add(reason);

						// numeros
						Number number = new Number();
						number.setNumber(Integer.parseInt(currentNumber.getMsisdn()));
						number.getReasons().addAll(reasonlist);
						numberList.add(number);

					}

					operationPortrequestRepository.save(portRequest);

					if (log.isInfoEnabled())
						log.info(String.format("Notificar Satelite: %s", satNotification));
					// envio de confirmacion al satelite si lo requiere
					if (satNotification)
						fillUtcRequest(portRequest, numberList);
					if (log.isInfoEnabled())
						log.info(String.format(AppConstants.ESTADO_SOLICITUD, portRequest.getStatus()));
					logger.info(CATEGORY, portRequest, clazz.getCanonicalName(), methodName, AppConstants.RESPONSE,
							portRequest.toString(), true, "0", 0L);
				} else {

					portRequest.setStatus(requestToAscp.getStatus());
					portRequest.setTransactionId(requestToAscp.getTransactionId());
					portRequest.setOperationidAscp(requestToAscp.getOperationidAscp());
					portRequest.setReceptorId(requestToAscp.getReceptorId());
					portRequest.setDonorId(requestToAscp.getDonorId());

					for (Portnumber currentNumber : portRequest.getPortnumberList()) {
						Reasons reason = new Reasons();
						reason.setReason(requestToAscp.getStatus());
						List<Reasons> reasonlist = new ArrayList<>();
						reasonlist.add(reason);

						// numeros
						Number number = new Number();
						number.setNumber(Integer.parseInt(currentNumber.getMsisdn()));
						number.getReasons().addAll(reasonlist);
						numberList.add(number);
					}
					// Envia notificacion al satelite

					if (log.isInfoEnabled())
						log.info(String.format("Notificar Satelite: %s", satNotification));
					if (satNotification)
						fillUtcRequest(portRequest, numberList);

					// guarda la solicitud con es estado de la solicitud enviada al ascp
					operationPortrequestRepository.save(portRequest);
					logger.info(CATEGORY, portRequest, clazz.getCanonicalName(), AppUtil.getMethodName(),
							"Solicitud agendada procesada. ", portRequest.getStatus(), true,
							env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL), 0L);
				}
			}
		} catch (AppServiceException e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
			response.setCodeResponse(e.getCode());
			response.setCodeDescription(e.getMessage());
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
			response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_ERROR));
			response.setCodeDescription(env.getProperty(AppConstants.PORTA_MSJ_ERROR));
		}
		logger.info(CATEGORY, response, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.RESPONSE,
				response.getCodeDescription() != null ? response.getCodeDescription() : "", true,
				response.getCodeResponse() != null ? response.getCodeResponse() : "", 0L);
		validStatus.clear();
		validType.clear();
	}

	private boolean inTime() {
		boolean value = false;
		try {
			int s = 7;
			Date f = null;
			Date f2 = null;
			Set<String> schedules = configurationService.getValues(AppConstants.DEBITCUTIN_ONLINE_TIME);
			for (String schedule : schedules) {
				CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(schedule);
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.MILLISECOND, 0);
				f = calendar.getTime();
				f2 = f;
				calendar.add(Calendar.SECOND, s * -1);
				f = calendar.getTime();
				int n = s + 1;
				for (int i = 0; i < n; i++) {
					f = cronSequenceGenerator.next(f);
					value = f.equals(f2);
					if (value) {
						if (log.isInfoEnabled())
							log.info(String.format("Corte por deuda on line ---> %s ", schedule));
						logger.info(CATEGORY, null, clazz.getCanonicalName(), AppUtil.getMethodName(), "",
								"Corte por deuda on line --->" + schedule, true, "0", 0L);
						return value;
					}
				}
			}
		} catch (Exception ex) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), "ERROR", ex.getMessage(), 0L, ex);
		}
		return value;
	}

	
	public void fillUtcRequest(Portrequest portrequest, List<Number> numberList) {

		AsyncUpdateTransactionComplete asyncUtc = new AsyncUpdateTransactionComplete();
		UpdateTransactionCompleteRequest utcRequest = new UpdateTransactionCompleteRequest();
		UpdateTransactionCompleteRequestHeader utcHeader = new UpdateTransactionCompleteRequestHeader();
		com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequest.UpdateTransactionCompleteRequestBody utcBody = new com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequest.UpdateTransactionCompleteRequestBody();

		Updatedate updatedate = new Updatedate();
		Integer satelite = Integer.valueOf(portrequest.getSateliteId().getId().toString());

		asyncUtc.setSateliteId(satelite);
		asyncUtc.setOperationRefId(TransactionIdUtil.getId());
		asyncUtc.setPlatform(portrequest.getChannel());
		asyncUtc.setUntilLastSequence(String.valueOf(TransactionIdUtil.getUntilLastSequence()));
		asyncUtc.setUpdateTransactionCompleteRequest(utcRequest);
		asyncUtc.setUser(env.getProperty(AppConstants.PORTA_GATEWAY_USER_WEB));
		asyncUtc.setPortRequestId(portrequest.getId());
		asyncUtc.setRequestTypeId(portrequest.getRequesttypeId().getId());
		utcRequest.setUpdateTransactionCompleteRequestHeader(utcHeader);
		utcRequest.setUpdateTransactionCompleteRequestBody(utcBody);
		
		utcHeader.setConsumerId(TransactionIdUtil.getId());
		utcHeader.setCorrelationId(String.valueOf(TransactionIdUtil.getUntilLastSequence()));
		utcHeader.setCountry(Country.BOL);
		utcHeader.setTransactionId(String.valueOf(portrequest.getId()));//

		utcBody.setUpdatedate(updatedate);

		updatedate.setChannel(env.getProperty(AppConstants.PORTA_GATEWAY_CHANNEL_WEB));
		updatedate.setDateTime(new Date());
		updatedate.setEventType(portrequest.getRequesttypeId().getPrefix());
		updatedate.getNumber().addAll(numberList);

		updatedate.setStatus(portrequest.getStatus());
		updatedate.setUser(env.getProperty(AppConstants.PORTA_GATEWAY_USER_WEB));
		List<AdditionalParameters> parametersList = new ArrayList<>();
		Reasons1 reasonsO = new Reasons1();
		List<Reasons1> reasonList = new ArrayList<>();
		try {
			for (Portnumber currentNumber : portrequest.getPortnumberList()) {
				String reasons = "";
				if (!currentNumber.getReasonList().isEmpty()) {
					AdditionalParameters parametrosAdicionales = null;
					for (Reason currentReason : currentNumber.getReasonList()) {
						parametrosAdicionales = new AdditionalParameters();
						parametrosAdicionales.setParameterName(currentNumber.getMsisdn());
						reasonsO.setCode(currentReason.getReasoncode());
						reasonsO.setMessage(currentReason.getReasondescription());
						reasonList.add(reasonsO);

					}
					reasons = parsingUtil.convertListToJson(reasonList);
					parametrosAdicionales.setParameterValue(reasons);
					parametersList.add(parametrosAdicionales);
					reasonList.clear();
				}
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), "ERROR", e.getMessage(), 0L,
					e);
		}

		updatedate.getAdditionalParameters().addAll(parametersList);

		sendRequest.send(asyncUtc, AppConstants.SPINTERFACE_QUEUE);

	}
	
	
	
//	public void fillUtcRequest(Portrequest portrequest, List<Number> numberList) {
//
//		AsyncUpdateTransactionComplete asyncUtc = new AsyncUpdateTransactionComplete();
//		UpdateTransactionCompleteRequest utcRequest = new UpdateTransactionCompleteRequest();
//		UpdateTransactionCompleteRequestHeader utcHeader = new UpdateTransactionCompleteRequestHeader();
//		com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequest.UpdateTransactionCompleteRequestBody utcBody = new com.htc.ea.jaxws.dto.UpdateTransactionCompleteRequest.UpdateTransactionCompleteRequestBody();
//
//		Updatedate updatedate = new Updatedate();
//		Integer satelite = Integer.valueOf(portrequest.getSateliteId().getId().toString());
//
//		asyncUtc.setSateliteId(satelite);
//		asyncUtc.setOperationRefId(TransactionIdUtil.getId());
//		asyncUtc.setPlatform(portrequest.getChannel());
//		asyncUtc.setUntilLastSequence(String.valueOf(TransactionIdUtil.getUntilLastSequence()));
//		asyncUtc.setUpdateTransactionCompleteRequest(utcRequest);
//		asyncUtc.setUser(portrequest.getUserlogin());
//		asyncUtc.setPortRequestId(portrequest.getId());
//		asyncUtc.setRequestTypeId(portrequest.getRequesttypeId().getId());
//		utcRequest.setUpdateTransactionCompleteRequestHeader(utcHeader);
//		utcRequest.setUpdateTransactionCompleteRequestBody(utcBody);
//
//		utcHeader.setConsumerId(TransactionIdUtil.getId());
//		utcHeader.setCorrelationId(String.valueOf(TransactionIdUtil.getUntilLastSequence()));
//		utcHeader.setCountry(Country.BOL);
//		utcHeader.setTransactionId(String.valueOf(portrequest.getId()));//
//
//		utcBody.setUpdatedate(updatedate);
//
//		updatedate.setChannel(portrequest.getChannel());
//		updatedate.setDateTime(new Date());
//		updatedate.setEventType(portrequest.getRequesttypeId().getPrefix());
//		updatedate.getNumber().addAll(numberList);
//
//		updatedate.setStatus(portrequest.getStatus());
//		updatedate.setUser(portrequest.getUserlogin());
//
//		sendRequest.send(asyncUtc, AppConstants.SPINTERFACE_QUEUE);
//
//	}

}
