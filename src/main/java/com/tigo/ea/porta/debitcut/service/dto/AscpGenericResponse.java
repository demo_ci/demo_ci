package com.tigo.ea.porta.debitcut.service.dto;

import java.util.List;
import java.util.Map;

import com._4gtss.npc.soap.service.consumer.Ack;
import com._4gtss.npc.soap.service.consumer.RejectReason;
import com._4gtss.npc.soap.service.consumer.Status;

/**
 * The Class AscpGenericResponse.
 *
 * @author HTC-Alexcst
 */
public class AscpGenericResponse {

	private Status status;
	private String descriptionError;
	private String npOrderId;
	private Ack ack;
	private Map<String, List<RejectReason>> rejectReasonMsisdnMap;
	
	
	/**
	 * Instantiates a new ascp generic response.
	 */
	public AscpGenericResponse() {
		super();
	}

	/**
	 * Instantiates a new ascp generic response.
	 *
	 * @param status the status
	 * @param descriptionError the description error
	 * @param npOrderId the np order id
	 * @param ack the ack
	 * @param rejectReasonMsisdnMap the reject reason msisdn map
	 */
	public AscpGenericResponse(Status status, String descriptionError, String npOrderId, Ack ack,
			Map<String, List<RejectReason>> rejectReasonMsisdnMap) {
		super();
		this.status = status;
		this.descriptionError = descriptionError;
		this.npOrderId = npOrderId;
		this.ack = ack;
		this.rejectReasonMsisdnMap = rejectReasonMsisdnMap;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Gets the description error.
	 *
	 * @return the description error
	 */
	public String getDescriptionError() {
		return descriptionError;
	}

	/**
	 * Sets the description error.
	 *
	 * @param descriptionError the new description error
	 */
	public void setDescriptionError(String descriptionError) {
		this.descriptionError = descriptionError;
	}

	/**
	 * Gets the np order id.
	 *
	 * @return the np order id
	 */
	public String getNpOrderId() {
		return npOrderId;
	}

	/**
	 * Sets the np order id.
	 *
	 * @param npOrderId the new np order id
	 */
	public void setNpOrderId(String npOrderId) {
		this.npOrderId = npOrderId;
	}

	/**
	 * Gets the ack.
	 *
	 * @return the ack
	 */
	public Ack getAck() {
		return ack;
	}

	/**
	 * Sets the ack.
	 *
	 * @param ack the new ack
	 */
	public void setAck(Ack ack) {
		this.ack = ack;
	}

	/**
	 * Gets the reject reason msisdn map.
	 *
	 * @return the reject reason msisdn map
	 */
	public Map<String, List<RejectReason>> getRejectReasonMsisdnMap() {
		return rejectReasonMsisdnMap;
	}

	/**
	 * Sets the reject reason msisdn map.
	 *
	 * @param rejectReasonMsisdnMap the reject reason msisdn map
	 */
	public void setRejectReasonMsisdnMap(Map<String, List<RejectReason>> rejectReasonMsisdnMap) {
		this.rejectReasonMsisdnMap = rejectReasonMsisdnMap;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AscpGenericResponse [status=" + status + ", descriptionError=" + descriptionError + ", npOrderId="
				+ npOrderId + ", ack=" + ack + ", rejectReasonMsisdnMap=" + rejectReasonMsisdnMap + "]";
	}

}
