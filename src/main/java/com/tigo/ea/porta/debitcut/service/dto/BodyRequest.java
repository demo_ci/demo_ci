//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.10.04 a las 07:49:29 PM BOT 
//


package com.tigo.ea.porta.debitcut.service.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BodyRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BodyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msisdnList" type="{http://com/tigo/ea/porta/debitcut/service/dto}msisdnList"/>
 *         &lt;element name="remarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="additionalParameterList" type="{http://com/tigo/ea/porta/debitcut/service/dto}AdditionalParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BodyRequest", propOrder = {
    "user",
    "channel",
    "dateTime",
    "msisdnList",
    "remarks",
    "additionalParameterList"
})
@XmlSeeAlso({
    BodyDonorRequest.class,
    BodyReceptorRequest.class
})
public class BodyRequest {

    @XmlElement(required = true)
    protected String user;
    protected String channel;
    @XmlElement(required = true)
    protected String dateTime;
    @XmlElement(required = true)
    protected MsisdnList msisdnList;
    protected String remarks;
    protected AdditionalParameterList additionalParameterList;

    /**
     * Obtiene el valor de la propiedad user.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Define el valor de la propiedad user.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

    /**
     * Obtiene el valor de la propiedad channel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Define el valor de la propiedad channel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannel(String value) {
        this.channel = value;
    }

    /**
     * Obtiene el valor de la propiedad dateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Define el valor de la propiedad dateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(String value) {
        this.dateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad msisdnList.
     * 
     * @return
     *     possible object is
     *     {@link MsisdnList }
     *     
     */
    public MsisdnList getMsisdnList() {
        return msisdnList;
    }

    /**
     * Define el valor de la propiedad msisdnList.
     * 
     * @param value
     *     allowed object is
     *     {@link MsisdnList }
     *     
     */
    public void setMsisdnList(MsisdnList value) {
        this.msisdnList = value;
    }

    /**
     * Obtiene el valor de la propiedad remarks.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Define el valor de la propiedad remarks.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarks(String value) {
        this.remarks = value;
    }

    /**
     * Obtiene el valor de la propiedad additionalParameterList.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalParameterList }
     *     
     */
    public AdditionalParameterList getAdditionalParameterList() {
        return additionalParameterList;
    }

    /**
     * Define el valor de la propiedad additionalParameterList.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalParameterList }
     *     
     */
    public void setAdditionalParameterList(AdditionalParameterList value) {
        this.additionalParameterList = value;
    }

}
