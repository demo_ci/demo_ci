//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.10.04 a las 07:49:29 PM BOT 
//


package com.tigo.ea.porta.debitcut.service.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Country.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="Country">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SLV"/>
 *     &lt;enumeration value="GTM"/>
 *     &lt;enumeration value="HND"/>
 *     &lt;enumeration value="BOL"/>
 *     &lt;enumeration value="COL"/>
 *     &lt;enumeration value="PRY"/>
 *     &lt;enumeration value="TCD"/>
 *     &lt;enumeration value="COG"/>
 *     &lt;enumeration value="GHA"/>
 *     &lt;enumeration value="MUS"/>
 *     &lt;enumeration value="RWA"/>
 *     &lt;enumeration value="SEN"/>
 *     &lt;enumeration value="TZA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Country")
@XmlEnum
public enum Country {

    SLV,
    GTM,
    HND,
    BOL,
    COL,
    PRY,
    TCD,
    COG,
    GHA,
    MUS,
    RWA,
    SEN,
    TZA;

    public String value() {
        return name();
    }

    public static Country fromValue(String v) {
        return valueOf(v);
    }

}
