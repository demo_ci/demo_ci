//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.10.04 a las 07:49:29 PM BOT 
//


package com.tigo.ea.porta.debitcut.service.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DebitCutReceptor complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DebitCutReceptor">
 *   &lt;complexContent>
 *     &lt;extension base="{http://com/tigo/ea/porta/debitcut/service/dto}DebitCutRequest">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://com/tigo/ea/porta/debitcut/service/dto}HeaderResquest"/>
 *         &lt;element name="body" type="{http://com/tigo/ea/porta/debitcut/service/dto}BodyReceptorRequest"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitCutReceptor", propOrder = {
    "header",
    "body"
})
@XmlSeeAlso({
    DebitCutReceptorRequest.class
})
public class DebitCutReceptor
    extends DebitCutRequest
{

    @XmlElement(required = true)
    protected HeaderResquest header;
    @XmlElement(required = true)
    protected BodyReceptorRequest body;

    /**
     * Obtiene el valor de la propiedad header.
     * 
     * @return
     *     possible object is
     *     {@link HeaderResquest }
     *     
     */
    public HeaderResquest getHeader() {
        return header;
    }

    /**
     * Define el valor de la propiedad header.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderResquest }
     *     
     */
    public void setHeader(HeaderResquest value) {
        this.header = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     * 
     * @return
     *     possible object is
     *     {@link BodyReceptorRequest }
     *     
     */
    public BodyReceptorRequest getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     * 
     * @param value
     *     allowed object is
     *     {@link BodyReceptorRequest }
     *     
     */
    public void setBody(BodyReceptorRequest value) {
        this.body = value;
    }

}
