//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.10.04 a las 07:49:29 PM BOT 
//


package com.tigo.ea.porta.debitcut.service.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://com/tigo/ea/porta/debitcut/service/dto}DebitCutResponse">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "DebitCutReceptorResponse")
public class DebitCutReceptorResponse
    extends DebitCutResponse
{

	public DebitCutReceptorResponse() {
		super();
	}

	public DebitCutReceptorResponse(String requestId, String gatewayID, String status, String transactionID,
			String codeResponse, String codeDescription, MsisdnResponseList msisdnResponseList) {
		super(requestId, gatewayID, status, transactionID, codeResponse, codeDescription, msisdnResponseList);
	}

	public DebitCutReceptorResponse(String codeResponse, String codeDescription) {
		super(codeResponse, codeDescription);
	}

	public DebitCutReceptorResponse(DebitCutResponse debitCutResponse) {
		super(debitCutResponse.getRequestId(), debitCutResponse.getGatewayID(), debitCutResponse.getStatus(),
				debitCutResponse.getTransactionID(), debitCutResponse.getCodeResponse(),
				debitCutResponse.getCodeDescription(), debitCutResponse.getMsisdnResponseList());
	}
}
