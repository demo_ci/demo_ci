//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.10.04 a las 07:49:29 PM BOT 
//


package com.tigo.ea.porta.debitcut.service.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DebitCutResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DebitCutResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gatewayID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codeResponse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codeDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msisdnResponseList" type="{http://com/tigo/ea/porta/debitcut/service/dto}MsisdnResponseList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitCutResponse", propOrder = {
    "requestId",
    "gatewayID",
    "status",
    "transactionID",
    "codeResponse",
    "codeDescription",
    "msisdnResponseList"
})
@XmlSeeAlso({
    DebitCutReceptorResponse.class,
    DebitCutDonorResponse.class
})
public class DebitCutResponse {
    public DebitCutResponse(String codeResponse, String codeDescription) {
		super();
		this.codeResponse = codeResponse;
		this.codeDescription = codeDescription;
	}

	public DebitCutResponse() {
		super();
	}

	public DebitCutResponse(String requestId, String gatewayID, String status, String transactionID,
			String codeResponse, String codeDescription, MsisdnResponseList msisdnResponseList) {
		super();
		this.requestId = requestId;
		this.gatewayID = gatewayID;
		this.status = status;
		this.transactionID = transactionID;
		this.codeResponse = codeResponse;
		this.codeDescription = codeDescription;
		this.msisdnResponseList = msisdnResponseList;
	}
    protected String requestId;
    protected String gatewayID;
    @XmlElement(required = true)
    protected String status;
    protected String transactionID;
    @XmlElement(required = true)
    protected String codeResponse;
    @XmlElement(required = true)
    protected String codeDescription;
    protected MsisdnResponseList msisdnResponseList;

    /**
     * Obtiene el valor de la propiedad requestId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Define el valor de la propiedad requestId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Obtiene el valor de la propiedad gatewayID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGatewayID() {
        return gatewayID;
    }

    /**
     * Define el valor de la propiedad gatewayID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGatewayID(String value) {
        this.gatewayID = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Define el valor de la propiedad transactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Obtiene el valor de la propiedad codeResponse.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeResponse() {
        return codeResponse;
    }

    /**
     * Define el valor de la propiedad codeResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeResponse(String value) {
        this.codeResponse = value;
    }

    /**
     * Obtiene el valor de la propiedad codeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeDescription() {
        return codeDescription;
    }

    /**
     * Define el valor de la propiedad codeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeDescription(String value) {
        this.codeDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad msisdnResponseList.
     * 
     * @return
     *     possible object is
     *     {@link MsisdnResponseList }
     *     
     */
    public MsisdnResponseList getMsisdnResponseList() {
        return msisdnResponseList;
    }

    /**
     * Define el valor de la propiedad msisdnResponseList.
     * 
     * @param value
     *     allowed object is
     *     {@link MsisdnResponseList }
     *     
     */
    public void setMsisdnResponseList(MsisdnResponseList value) {
        this.msisdnResponseList = value;
    }

}
