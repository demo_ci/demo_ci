package com.tigo.ea.porta.debitcut.service.dto;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.model.repository.OperatorRepository;
import com.tigo.ea.porta.model.repository.PortNumberRepository;
import com.tigo.ea.porta.model.repository.PortRequestRepository;
import com.tigo.ea.porta.model.repository.RequestTypeRepository;

@Endpoint
public class DebitCutService extends ServiceSoap {

	private static final Logger logger = LoggerFactory.getLogger(DebitCutService.class);

	@Autowired
	Environment env;

	private final Class<?> clazz = this.getClass();

	private static final String CATEGORY = "service";

	private static final String NAMESPACE_URI = "http://com/tigo/ea/porta/debitcut/service/dto";

	@Autowired
	PortRequestRepository portRequestRepository;

	@Autowired
	RequestTypeRepository requestTypeRepository;

	@Autowired
	OperatorRepository operatorRepository;

	@Autowired
	PortNumberRepository portNumberRepository;

	@Autowired
	AppUtil appUtil;

	@Autowired
	ExecuteService execute;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DebitCutDonorRequest")
	@ResponsePayload
	public DebitCutDonorResponse debitCutDonor(@RequestPayload DebitCutDonorRequest request) {
		TransactionIdUtil.setMsisdn("");
		DebitCutDonorResponse response = null;
		long init = System.currentTimeMillis();
		String methodName = "debitCutDonor";
		String xmlString = "";

		try {
			// Impresión del sobre xml
			JAXBContext jaxbContext = JAXBContext.newInstance(DebitCutDonorRequest.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			StringWriter sw = new StringWriter();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(request, sw);
			xmlString = sw.toString();
			TransactionIdUtil.begin();
			if (logger.isInfoEnabled())
				logger.info(String.format("Request < %s >", xmlString));
			logRequest(request, clazz.getCanonicalName(), methodName, xmlString, CATEGORY);
			DebitCutResponse res = execute.process(request);
			response = new DebitCutDonorResponse(res);

			jaxbContext = JAXBContext.newInstance(DebitCutDonorResponse.class);
			jaxbMarshaller = jaxbContext.createMarshaller();
			sw = new StringWriter();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(response, sw);
			xmlString = sw.toString();
			
			logResponse(response, clazz.getCanonicalName(), methodName, xmlString, 0, init, CATEGORY);
		} catch (Exception ex) {
			logError(ex, clazz.getCanonicalName(), methodName, init, CATEGORY);
		}
		if (logger.isInfoEnabled())
			logger.info(String.format("Response: %s", xmlString));

		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DebitCutSendASCPRequest")
	@ResponsePayload
	public DebitCutSendASCPResponse executeASCP(@RequestPayload DebitCutSendASCPRequest request) {
		String methodName = "executeASCP";
		long init = System.currentTimeMillis();
		DebitCutSendASCPResponse response = null;
		String xmlString = "";
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(DebitCutSendASCPRequest.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			StringWriter sw = new StringWriter();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(request, sw);
			xmlString = sw.toString();
			
			TransactionIdUtil.setMsisdn("");
			if (logger.isInfoEnabled())
				logger.info(String.format("Request: %s", xmlString));
			logRequest(request, clazz.getCanonicalName(), methodName, xmlString, CATEGORY);
			response = execute.sendASCP(request);
			logResponse(response, clazz.getCanonicalName(), methodName, "Response<DentCit>",
					response.getCodeResponse() != null ? Integer.valueOf(response.getCodeResponse()) : 0, init,
					CATEGORY);
			jaxbContext = JAXBContext.newInstance(DebitCutSendASCPResponse.class);
			jaxbMarshaller = jaxbContext.createMarshaller();
			sw = new StringWriter();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(response, sw);
			xmlString = sw.toString();
			logResponse(response, clazz.getCanonicalName(), methodName, xmlString, 0, init, CATEGORY);
			if (logger.isInfoEnabled())
				logger.info(String.format("Request: %s", xmlString));

		} catch (Exception e) {
			logError(e, clazz.getCanonicalName(), methodName, init, CATEGORY);
		}
	
		return response;
	}

}
