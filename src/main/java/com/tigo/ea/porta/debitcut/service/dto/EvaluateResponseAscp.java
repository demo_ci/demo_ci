package com.tigo.ea.porta.debitcut.service.dto;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.rule.DebitCutRule;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.commons.CatalogueMonitorStatus;
import com.tigo.ea.porta.model.commons.CataloguePortRequestStatus;
import com.tigo.ea.porta.model.dao.Changestatus;
import com.tigo.ea.porta.model.dao.Operator;
import com.tigo.ea.porta.model.dao.Portnumber;
import com.tigo.ea.porta.model.dao.Portrequest;
import com.tigo.ea.porta.model.dao.Reason;
import com.tigo.ea.porta.model.repository.OperatorRepository;

@Component
public class EvaluateResponseAscp {

	@Autowired
	private LoggerUtil logger;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private AppUtil appUtil;

	@Autowired
	private OperatorRepository operatorRepository;
	
	private final Class<?> clazz = this.getClass();
	
	
	private static final Logger log = LoggerFactory.getLogger(DebitCutRule.class);

	private static final String CATEGORY = "service";
	
	public Portrequest evaluateAscpResponseProcess(boolean isAccept, AscpGenericResponse responseService,
			Portrequest portRequest) {
		String methodName = "evaluateAscpResponseProcess";
		try {
			if (responseService.getStatus() == com._4gtss.npc.soap.service.consumer.Status.SUCCESS) {
				String senderId = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
				// Actualizamos la solicitud y finalizamos proceso
				Changestatus changeStatus = new Changestatus();
				changeStatus.setOperationReferenceId(TransactionIdUtil.getId());
				changeStatus.setAutomatic(false);
				changeStatus.setDatechange(new Date());
				changeStatus.setIdPortrequest(portRequest);

				if (isAccept) {
					Operator operador = null;
					changeStatus.setStatus(CataloguePortRequestStatus.PENDIENTE.getName());
					changeStatus.setRemarks(CataloguePortRequestStatus.PENDIENTE.getName());
					changeStatus.setUserlogin(portRequest.getChannel());
					operador = operatorRepository.findOperatorByRoutingNumber(senderId);
					changeStatus.setSenderId(operador);
					portRequest.setTransactionId(String.valueOf(responseService.getAck().getMessageId()));
					portRequest.setRequestDate(new Date());
					portRequest.setOperationidAscp(responseService.getAck().getNpOrderId());
					portRequest.setStatus(CataloguePortRequestStatus.PENDIENTE.getName());
					portRequest.setRemarks(CataloguePortRequestStatus.PENDIENTE.getName());
					String receptor = getRoutingNumber(responseService.getAck().getDonorId());
		            operador  = operatorRepository.findOperatorByRoutingNumber(receptor);
					portRequest.setSenderId(operador);
					for (Portnumber currenteNumber : portRequest.getPortnumberList()) {
						currenteNumber.setStatus(CataloguePortRequestStatus.PENDIENTE.getName());
						if(responseService.getAck().getRangeHolderId()!= null) {
							receptor = getRoutingNumber(responseService.getAck().getRangeHolderId());
							operador  = operatorRepository.findOperatorByRoutingNumber(receptor);
							currenteNumber.setOwnerId(operador);
						}
					}
				} else {
					changeStatus.setStatus(CataloguePortRequestStatus.RECHAZADO.getName());
					changeStatus.setRemarks(CataloguePortRequestStatus.RECHAZADO.getName());

					portRequest.setStatus(CataloguePortRequestStatus.RECHAZADO.getName());
					portRequest.setRemarks(CataloguePortRequestStatus.RECHAZADO.getName());
				}

				portRequest.getChangestatusList().add(changeStatus);

			} else if (responseService.getStatus() == com._4gtss.npc.soap.service.consumer.Status.FAILURE
					&& responseService.getRejectReasonMsisdnMap() != null) {
				// servicio NP retorno Error

				// Actualizamos la solicitud y finalizamos proceso
				Changestatus changeStatus = new Changestatus();
				changeStatus.setOperationReferenceId(TransactionIdUtil.getId());
				changeStatus.setAutomatic(false);
				changeStatus.setDatechange(new Date());
				changeStatus.setIdPortrequest(portRequest);
				changeStatus.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
				changeStatus.setRemarks(CataloguePortRequestStatus.ERROR_INTERNO.getName());

				// iniciamos actualizacion de la solicitud
				portRequest.setResponseAscpdate(new Date());
				portRequest.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
				portRequest.setRemarks(CataloguePortRequestStatus.ERROR_INTERNO.getName());
				portRequest.getChangestatusList().add(changeStatus);
				for (Portnumber currenteNumber : portRequest.getPortnumberList()) {
					currenteNumber.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
				}

				// Verificamos si las razones de fallo se adjuntaran al portRequest o al
				// portNumber
				if (responseService.getRejectReasonMsisdnMap().containsKey(AppConstants.PORT_REQUEST)) {
					// las reason pertenecen al portRequest
					List<com._4gtss.npc.soap.service.consumer.RejectReason> reasonList = responseService
							.getRejectReasonMsisdnMap().get(AppConstants.PORT_REQUEST);
					for (com._4gtss.npc.soap.service.consumer.RejectReason curReason : reasonList) {
						Reason reason = new Reason();
						reason.setPortrequestId(portRequest);
						reason.setReasoncode(String.valueOf(curReason.getRejectCode()));
						reason.setReasondescription(curReason.getRejectMessage());
						portRequest.getReasonList().add(reason);
					}
				}
 
				// iteramos las reason que pertenecen al portNumber si existe
				for (Portnumber portNumber : portRequest.getPortnumberList()) {

					// verificamos si existen raason para el portNumber
					if (responseService.getRejectReasonMsisdnMap().containsKey(portNumber.getMsisdn())) {

						List<com._4gtss.npc.soap.service.consumer.RejectReason> reasonList = responseService
								.getRejectReasonMsisdnMap().get(portNumber.getMsisdn());
						for (com._4gtss.npc.soap.service.consumer.RejectReason curReason : reasonList) {
							Reason reason = new Reason();
							reason.setPortnumberId(portNumber);
							reason.setReasoncode(String.valueOf(curReason.getRejectCode()));
							reason.setReasondescription(appUtil.translateSpanish(
									String.valueOf(curReason.getRejectCode()),
									curReason.getRejectMessage(), clazz.getCanonicalName(), methodName));
//							reason.setReasondescription(appUtil.translateSpanish(String.valueOf(curReason.getRejectCode() + "R"),
//							curReason.getRejectMessage(), clazz.getCanonicalName(), AppUtil.getMethodName()));
							portNumber.getReasonList().add(reason);
						}
					}
				}
			} else {
				// si el error fue por timeout o error general
				log.info("El servicio NPDebtRecipientReject respondio con ERROR timeout o ERROR de ejecucion");

				// Actualizamos la solicitud y finalizamos proceso
				Changestatus changeStatus = new Changestatus();
				changeStatus.setOperationReferenceId(TransactionIdUtil.getId());
				changeStatus.setAutomatic(false);
				changeStatus.setDatechange(new Date());
				changeStatus.setIdPortrequest(portRequest);
				changeStatus.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
				changeStatus.setRemarks(responseService.getDescriptionError());

				portRequest.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
				portRequest.setRemarks(responseService.getDescriptionError());
				portRequest.getChangestatusList().add(changeStatus);
				logger.info(CATEGORY, portRequest, clazz.getCanonicalName(), methodName, AppConstants.RESPONSE,
						portRequest.toString(), false, "0", 0L);
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
			// Actualizamos la solicitud
			Changestatus changeStatus = new Changestatus();
			changeStatus.setOperationReferenceId(TransactionIdUtil.getId());
			changeStatus.setAutomatic(false);
			changeStatus.setDatechange(new Date());
			changeStatus.setIdPortrequest(portRequest);
			changeStatus.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
			changeStatus.setRemarks("npConnect process: " + CatalogueMonitorStatus.ERROR_PROCESAR.getName());

			portRequest.setStatus(CataloguePortRequestStatus.ERROR_INTERNO.getName());
			portRequest.setRemarks("npConnect process: " + CatalogueMonitorStatus.ERROR_PROCESAR.getName());
			portRequest.getChangestatusList().add(changeStatus);
		}
		return portRequest;
	}

	private String getRoutingNumber(String begin) {
		  if (begin.length() == 2) {
		      begin = "89".concat(begin);
		    } else if (begin.length() < 2) {
		      begin = "890".concat(begin);
		    }
		return begin;
	}
	
}
