package com.tigo.ea.porta.debitcut.service.dto;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.repository.OperationPortrequestRepository;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.dao.Portrequest;

@Service
public class ExecuteAgendedRequest {
	
	private static final Logger log = LoggerFactory.getLogger(ExecuteAgendedRequest.class);
	
	private static final String CATEGORY = "service";
	
	private final Class<?> clazz = this.getClass();
	
	@Autowired
	private Environment env;
	
	@Autowired
	private LoggerUtil logger;
	
	@Autowired
	private OperationPortrequestRepository operationPortrequestRepository;
	
	@Autowired
	private AgendedExcecute agendedExcecute;

	public void excecuteAgendedTransactions() {
		String methodName = "excecuteAgendedTransactions";
		TransactionIdUtil.begin();
		List<Portrequest> portrequestList = new ArrayList<>();
		//Obtenemos las solicitudes que se hayan agendado.
		portrequestList = operationPortrequestRepository.getPortRequestAgended(env.getProperty(AppConstants.PORTA_STATUS_AGENDADO),
				env.getProperty(AppConstants.PORTA_PREFIX_DEVCUT_OUT));
		if(log.isInfoEnabled()) 
			log.info(String.format("-------------------------------Solicitudes a procesar: %s",  portrequestList.size()));
		for(Portrequest currentRequest :portrequestList) {
			try {
				agendedExcecute.execute(currentRequest);
			}catch(Exception e) {
				logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.PORTA_MSJ_SERVICE, "", 0L, e);
			}
		}
		TransactionIdUtil.end();
	}
}
