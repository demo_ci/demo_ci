package com.tigo.ea.porta.debitcut.service.dto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.tigo.ea.porta.debitcut.service.rule.ChangeStatusRule;
import com.tigo.ea.porta.debitcut.service.rule.DebitCutRule;
import com.tigo.ea.porta.debitcut.service.rule.ValidateDebitCut;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

@Service
public class ExecuteService {

	private static final Logger log = LoggerFactory.getLogger(ExecuteService.class);
	
	private final Class<?> clazz = this.getClass();

	private static String CATEGORY = "service";
	
	@Autowired
	private LoggerUtil logger;
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ValidateDebitCut validateDebitCut;
	
	@Autowired
	private DebitCutRule debitCutRule;
	
	@Autowired
	private ChangeStatusRule changeStatusRule;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DebitCutResponse process(DebitCutRequest request) {
		DebitCutResponse response =  new DebitCutResponse();
		try {
			String methodName = "process";
			logger.info(CATEGORY, request, clazz.getName(), methodName, "Request<", request.toString(), true,
					"0", 0L);
			response = validateDebitCut.execute(request);
			if (response != null) {
				logger.info(CATEGORY, response, clazz.getName(), methodName, "Response <",  response.toString() + " >",
						true, "0", 0L);
				return response;
			}
			log.info("Request valido.....");
			response = debitCutRule.execute(request);
			logger.info(CATEGORY, response, clazz.getName(), methodName, "Response < ", "", 
					true, "0", 0L);
		}catch(Exception e) {
			response =  new DebitCutResponse();
			response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_ERROR));
			response.setCodeDescription(e.getMessage());
		}
		return response;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DebitCutSendASCPResponse sendASCP(DebitCutSendASCPRequest request) {
		DebitCutSendASCPResponse response;
		String methodName = "sendASCP";
		logger.info(CATEGORY, request, clazz.getName(), methodName, "Request <", request.toString(),
				true, "0", 0L);
		response = changeStatusRule.execute(request);
		logger.info(CATEGORY, response, clazz.getName(), methodName, "Response", response.toString(),
				true, response.getCodeResponse(), 0L);
		return response;
	}
}
