//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2017.10.04 a las 07:49:29 PM BOT 
//


package com.tigo.ea.porta.debitcut.service.dto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tigo.ea.porta.debitcut.service.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tigo.ea.porta.debitcut.service.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DebitCutSendASCPResponse }
     * 
     */
    public DebitCutSendASCPResponse createDebitCutSendASCPResponse() {
        return new DebitCutSendASCPResponse();
    }

    /**
     * Create an instance of {@link SendASCPResponse }
     * 
     */
    public SendASCPResponse createSendASCPResponse() {
        return new SendASCPResponse();
    }

    /**
     * Create an instance of {@link DebitCutDonorRequest }
     * 
     */
    public DebitCutDonorRequest createDebitCutDonorRequest() {
        return new DebitCutDonorRequest();
    }

    /**
     * Create an instance of {@link DebitCutDonor }
     * 
     */
    public DebitCutDonor createDebitCutDonor() {
        return new DebitCutDonor();
    }

    /**
     * Create an instance of {@link DebitCutRequest }
     * 
     */
    public DebitCutRequest createDebitCutRequest() {
        return new DebitCutRequest();
    }

    /**
     * Create an instance of {@link HeaderResquest }
     * 
     */
    public HeaderResquest createHeaderResquest() {
        return new HeaderResquest();
    }

    /**
     * Create an instance of {@link BodyDonorRequest }
     * 
     */
    public BodyDonorRequest createBodyDonorRequest() {
        return new BodyDonorRequest();
    }

    /**
     * Create an instance of {@link DebitCutSendASCPRequest }
     * 
     */
    public DebitCutSendASCPRequest createDebitCutSendASCPRequest() {
        return new DebitCutSendASCPRequest();
    }

    /**
     * Create an instance of {@link SendASCP }
     * 
     */
    public SendASCP createSendASCP() {
        return new SendASCP();
    }

    /**
     * Create an instance of {@link DebitCutDonorResponse }
     * 
     */
    public DebitCutDonorResponse createDebitCutDonorResponse() {
        return new DebitCutDonorResponse();
    }

    /**
     * Create an instance of {@link DebitCutResponse }
     * 
     */
    public DebitCutResponse createDebitCutResponse() {
        return new DebitCutResponse();
    }

    /**
     * Create an instance of {@link MsisdnResponseList }
     * 
     */
    public MsisdnResponseList createMsisdnResponseList() {
        return new MsisdnResponseList();
    }

    /**
     * Create an instance of {@link DebitCutReceptorRequest }
     * 
     */
    public DebitCutReceptorRequest createDebitCutReceptorRequest() {
        return new DebitCutReceptorRequest();
    }

    /**
     * Create an instance of {@link DebitCutReceptor }
     * 
     */
    public DebitCutReceptor createDebitCutReceptor() {
        return new DebitCutReceptor();
    }

    /**
     * Create an instance of {@link BodyReceptorRequest }
     * 
     */
    public BodyReceptorRequest createBodyReceptorRequest() {
        return new BodyReceptorRequest();
    }

    /**
     * Create an instance of {@link DebitCutReceptorResponse }
     * 
     */
    public DebitCutReceptorResponse createDebitCutReceptorResponse() {
        return new DebitCutReceptorResponse();
    }

    /**
     * Create an instance of {@link AdditionalParameterList }
     * 
     */
    public AdditionalParameterList createAdditionalParameterList() {
        return new AdditionalParameterList();
    }

    /**
     * Create an instance of {@link BodyRequest }
     * 
     */
    public BodyRequest createBodyRequest() {
        return new BodyRequest();
    }

    /**
     * Create an instance of {@link MsisdnList }
     * 
     */
    public MsisdnList createMsisdnList() {
        return new MsisdnList();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

}
