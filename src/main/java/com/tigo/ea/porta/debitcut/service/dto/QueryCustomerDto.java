package com.tigo.ea.porta.debitcut.service.dto;

import java.util.Date;

public class QueryCustomerDto {

	Integer msisdn;
	Date processdate;
	String nporderid;
	Integer idmaximo;
	String eventType;
	Integer recordType;
	String rangeholderid;
	String donorid;
	String recipientid;
	Integer portingcounter;
	Integer numbertype;
	
	
	public String getNporderid() {
		return nporderid;
	}
	public void setNporderid(String nporderid) {
		this.nporderid = nporderid;
	}
	public Integer getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Integer msisdn) {
		this.msisdn = msisdn;
	}
	public Date getProcessdate() {
		return processdate;
	}
	public void setProcessdate(Date processdate) {
		this.processdate = processdate;
	}
	public Integer getIdmaximo() {
		return idmaximo;
	}
	public void setIdmaximo(Integer idmaximo) {
		this.idmaximo = idmaximo;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public Integer getRecordType() {
		return recordType;
	}
	public void setRecordType(Integer recordType) {
		this.recordType = recordType;
	}
	public String getRangeholderid() {
		return rangeholderid;
	}
	public void setRangeholderid(String rangeholderid) {
		this.rangeholderid = rangeholderid;
	}
	public String getDonorid() {
		return donorid;
	}
	public void setDonorid(String donorid) {
		this.donorid = donorid;
	}
	public String getRecipientid() {
		return recipientid;
	}
	public void setRecipientid(String recipientid) {
		this.recipientid = recipientid;
	}
	public Integer getPortingcounter() {
		return portingcounter;
	}
	public void setPortingcounter(Integer portingcounter) {
		this.portingcounter = portingcounter;
	}
	public Integer getNumbertype() {
		return numbertype;
	}
	public void setNumbertype(Integer numbertype) {
		this.numbertype = numbertype;
	}
	
}
