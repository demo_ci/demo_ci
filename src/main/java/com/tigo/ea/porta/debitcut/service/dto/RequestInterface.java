package com.tigo.ea.porta.debitcut.service.dto;

public interface RequestInterface {
	
	public HeaderResquest getHeader() ;

	public BodyRequest getBody();
}
