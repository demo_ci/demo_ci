package com.tigo.ea.porta.debitcut.service.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

@Service
public class SendAcspProcess {

	private static final String CATEGORY = "service";
	
	private final Class<?> clazz = this.getClass();
	
	@Autowired
	private Environment env;
	
	@Autowired
	private AppUtil appUtil;
	
	@Autowired
	private LoggerUtil logger;

	
//	public NPDebtRequestResponse executeRetryNpcDebtRequest(NpDebtRequest debtRequest) throws RetryPortException  {
//		String methodName = "executeRetryNpcDebtRequest";
//		NPDebtRequestResponse response = new NPDebtRequestResponse();
//		NpcRejectList rejectList = new NpcRejectList();
//		Reject reject = new Reject();
//		Response responser = new Response();
//		Error error = new Error();
//		
//		try {
//			RetryPolicy retryPolicy = new RetryPolicy(env, "ascpDebtReq");
//			ThroughputData tpdata = retryPolicy.getWsSoapResource().getThroughputData();
//			int maxAttempts = tpdata.getRetries().intValue();
//			long backOffPolicy = tpdata.getDelayBetweenRetries();
//			RetryTemplate retryTemplate = RetryTemplateFactory.retryTemplate(maxAttempts, backOffPolicy, RetryPortException.class);
////			retryData			
////			NpcServiceConsumerRetryData retryData = RetryDataFactory.createNpcServiceNPDebtRequestData(appUtil, logger,
////					CatalogueNPServImplRetryOperation.npDebtRequest, npcServiceConsumer, debtRequest);
////			retryCallback
////			NpcServiceImplConsumerRetry retryCallback = new NpcServiceImplConsumerRetry(retryData);
//			//recoveryData
//			NpcServiceConsumerRetryData recoveryData = RetryDataFactory.createNPCServiceNPRequestRecoveryData(appUtil, logger,
//					CatalogueNPServImplRetryOperation.npDebtRequest);
////			recoveryCallback
//			NpcServiceConsumerRecovery recoveryCallBack = new NpcServiceConsumerRecovery(recoveryData);
//			
////			Object result = retryTemplate.execute(retryCallback, recoveryCallBack);
////			if(result.equals(3)) {
////				RejectReason rej = new RejectReason(); 
////				rej.setRejectCode(1);
////				rej.setRejectMessage(env.getProperty(CataloguePortRequestStatus.ERROR_INTERNO.getName()));
////				reject.getRejectReasons().add(rej);
////				rejectList.getRejections().add(reject);
////				error.setNpcRejectList(rejectList);
////				responser.setError(error);
////			}else {
////				response = (NPDebtRequestResponse) result;
////			}
//		}catch(Exception e) {
//			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, "Error", "",
//					0L, e);
//		}
//		return response;
//	}
}
