package com.tigo.ea.porta.debitcut.service.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

@Component
public class ServiceSoap {

	@Autowired
	private LoggerUtil loggerUtil;

	public void logRequest(Object data, String clas, String serviceName, String detail, String category) {
		loggerUtil.info(category, data, clas, serviceName, "Request <DebitCut" + serviceName + ">", detail, true, "0",
				0L);
		
	}
	
	public void logResponse(Object data, String clas, String serviceName, String detail, int responseCode, long init,
			String category) {
		long duration = System.currentTimeMillis() - init;
		loggerUtil.info(category, data, clas, serviceName, "Response <DebitCut." + serviceName + ">", detail, true,
				String.valueOf(responseCode), duration);
	}

	public void logDebug(Object data, String clas, String serviceName, String detail, int responseCode, long init,
			String category) {
		long duration = System.currentTimeMillis() - init;
		loggerUtil.debug(category, data, clas, serviceName, "Response <DebitCut." + serviceName + ">", detail, true,
				String.valueOf(responseCode), duration);
	}

	public void logServiceException(String clas, String serviceName, String detail, int responseCode, long init,
			Exception exception, String category) {
		long duration = System.currentTimeMillis() - init;
		loggerUtil.serviceException(category, clas, serviceName, "ServiceException <DebitCut." + serviceName + ">",
				detail, duration, exception, String.valueOf(responseCode));
	}

	protected void logError(Exception exception, String clas, String serviceName, long init, String category) {
		long duration = System.currentTimeMillis() - init;
		loggerUtil.error(category, clas, serviceName, "Error <DebitCut." + serviceName + ">", null, duration, exception);
	}
}