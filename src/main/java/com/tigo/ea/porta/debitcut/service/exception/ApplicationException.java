package com.tigo.ea.porta.debitcut.service.exception;

/**
 * @author Alexcst
 *
 */
public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private static final long DEFAULT_CODE = 100;
	private final long code;
	private final String description;
	
	public ApplicationException(Exception ex) {
		super(ex);
		this.code = DEFAULT_CODE;		
		description = ex.getMessage();
	}
	
	public ApplicationException(String description, Exception ex) {
		super(ex);
		this.code = DEFAULT_CODE;
		this.description = description;
	}
	
	public ApplicationException(long code, String description, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
	}
	
	public ApplicationException(long code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public ApplicationException(String description) {
		this.code = DEFAULT_CODE;		
		this.description = description;
	}

	@Override
	public String getMessage() {
		return toString();
	}
		
	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApplicationException [code=").append(code).append(", description=").append(description)
				.append("]");
		return builder.toString();
	}	
}
