package com.tigo.ea.porta.debitcut.service.listener;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

import com.htc.ea.util.util.Constants;
import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.catalogue.CatalogueBusiness;
import com.tigo.ea.porta.debitcut.service.core.CacheBusinessManager;
import com.tigo.ea.porta.debitcut.service.exception.ApplicationException;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

/**
 * @author Alexcst
 *
 */
public class CacheBusinessListener implements MessageListener {

	private static final Logger loggerFile = LoggerFactory.getLogger(CacheBusinessListener.class);
	private final Class<?> clazz = this.getClass();
	private static final String MESSAGE = "Refresh Catalogos de Negocio";
	private static final String METHOD_NAME = "refreshCacheBusinessManager";
	private static final String EVENT_CATEGORY = "refresh-catalogue";
	private CountDownLatch latch = new CountDownLatch(1);

	@Autowired
	private Environment env;
	@Autowired
	private LoggerUtil logger;
	@Autowired
	private CacheBusinessManager cacheBusinessManager;

	@Override
	public void onMessage(Message message, byte[] pattern) {
		String msg = new String(message.getBody());
		try {
			refresh(msg);
		} catch (InterruptedException e) {
			logger.error(EVENT_CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					e.getMessage(), 0L, e);
		}
	}

	public void refresh(String message) throws InterruptedException {
		Thread.sleep(1000);
		TransactionIdUtil.begin();
		TransactionIdUtil.generateId();
		loggingInit();
		String[] messageArray = message.split(":");
		String appIdReaded = "";
		String detail = "AppId: %s => AppId lei�do recibido: %s";
		String detail2 = "Catalogos: %s => catalogo lei�do recibido: %s";
		String detail3 = "Mensaje equivocado: %s";
		if (messageArray != null && messageArray.length == 2) {
			appIdReaded = messageArray[0];
			if (loggerFile.isInfoEnabled())
				loggerFile.info(String.format("Aplication: %s", appIdReaded));
			String data = messageArray[1];
			String appId = env.getProperty(Constants.GENERAL_CONFIGURATION);
			if (appIdReaded.equals(appId)) {
				CatalogueBusiness catalogue = CatalogueBusiness.get(data);
				String cataloguePretty = Arrays.deepToString(CatalogueBusiness.values());
				detail2 = String.format(detail2, cataloguePretty, data);
				if (catalogue != null) {
					refreshCacheBusinessManager(catalogue);
				} else {
					loggingError(String.format(detail2, appId, appIdReaded));
				}
			} else {
				loggingError(String.format(detail, appId, appIdReaded));
			}

			if (appIdReaded.equals(env.getProperty("application.id") + "-" + env.getProperty("app.env") + "-"
					+ env.getProperty("application.version"))) {

				refreshCacheBusinessManager(CatalogueBusiness.CONFIGURATION_DATA);
			}
		} else {
			detail3 = String.format(detail3,
					(messageArray == null ? "Mensaje equivocado" : Arrays.deepToString(messageArray)));
			loggingError(detail3);
		}
		loggingEnd();
		TransactionIdUtil.end();
	}

	private void refreshCacheBusinessManager(CatalogueBusiness catalogue) {
		try {
			cacheBusinessManager.refreshCatalogues(catalogue);
		} catch (ApplicationException e) {
			loggingError2(e);
		}
	}

	private void loggingInit() {
		logger.info(EVENT_CATEGORY, null, clazz.getCanonicalName(), METHOD_NAME, MESSAGE,
				"Inicia actualizacion de catalogo de negocio", null, null, null);
	}

	private void loggingEnd() {
		logger.info(EVENT_CATEGORY, null, clazz.getCanonicalName(), METHOD_NAME, MESSAGE,
				"Finaliza actualizacionn de catalogo de negocio", null, null, null);
	}

	private void loggingError(String detail) {
		logger.error(EVENT_CATEGORY, clazz.getCanonicalName(), METHOD_NAME, "",
				"Error actualizando catalogo de negocio. " + detail, null);
	}

	private void loggingError2(Exception e) {
		logger.error(EVENT_CATEGORY, clazz.getCanonicalName(), METHOD_NAME, MESSAGE,
				"Error actualizando catalogo de negocio; ocurrio un error inesperado", null, e);
	}

	@SuppressWarnings("unused")
	private void delay() {
		long elapsedTime = 0;
		long startTime = System.currentTimeMillis();
		long timeout = Long.parseLong(EVENT_CATEGORY);
		while (elapsedTime < timeout) {
			elapsedTime = System.currentTimeMillis() - startTime;
		}
	}

	@SuppressWarnings("unused")
	private void receiver(String cacheData) {
		receiveMessage(cacheData);
	}

	private void receiveMessage(String message) {
		String data = env.getProperty("application.id");
		loggerFile.info(data);
		loggerFile.info("Received <{}>", message);
		latch.countDown();
		loggerFile.info("{}", getLatch());
	}

	private CountDownLatch getLatch() {
		return latch;
	}
}
