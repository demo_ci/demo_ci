package com.tigo.ea.porta.debitcut.service.localcache;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.scheduled.ScheduledConfigCronService;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

/**
 * The Class InitLoadLocalCache.
 *
 * @author HTC-Alexcst
 */
@Component
public class InitLoadLocalCache {
	
	private final Class<?> clazz = this.getClass();
	
	private static final String CATEGORY = "service";
	
	@Autowired 
	private LoggerUtil logger;
	
	@Autowired
	private ScheduledConfigCronService scheduledConfigCronService;
	
//	private String cronExpresion = "0 41 * ? * *"; //ejecutar a las 23 horas 55 minutos --> 0 55 23 ? * * *

	/**
	 * Inits the local cache satelite.
	 */
	@PostConstruct
	public void initLocalCacheSatelite() {
		TransactionIdUtil.begin();
		TransactionIdUtil.generateId();
		String methodName = "initLocalCacheSatelite";
		try{
			//cargando CRON config en cache local
			scheduledConfigCronService.initConfigureCronTreads();
		}catch (Exception e) {			
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, "ERROR", e.getMessage(), 0L, e);
			//Si algo falla durante la carga de la cache detenemos la ejecucion de la Virtual Machine Java para el boot
			Runtime.getRuntime().exit(0);
		}
		TransactionIdUtil.end();
	}
}
