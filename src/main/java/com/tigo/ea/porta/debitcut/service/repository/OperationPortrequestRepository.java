package com.tigo.ea.porta.debitcut.service.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tigo.ea.porta.debitcut.service.commons.ConfigurationMapper;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppServiceException;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.dao.Portnumber;
import com.tigo.ea.porta.model.dao.Portrequest;
import com.tigo.ea.porta.model.dao.Requesttype;
import com.tigo.ea.porta.model.repository.PortNumberRepository;
import com.tigo.ea.porta.model.repository.PortRequestRepository;
import com.tigo.ea.porta.model.repository.RequestTypeRepository;

@Component
public class OperationPortrequestRepository {

	private final Class<?> clazz = this.getClass();

	@Autowired
	private Environment env;

	@Autowired
	private PortRequestRepository portRequestRepository;

	@Autowired
	private PortNumberRepository portNumberRepository;
	
	@Autowired
	private RequestTypeRepository requestTypeRepository;
	
	@Autowired
	private ConfigurationMapper configurationMapper;

	@Autowired
	private LoggerUtil logger;

	private static final String CATEGORY = "repository";

	@Transactional
	public Portrequest getPortRequestById(String id) {
		Portrequest data = null;
		String methodName = "getPortRequestById";
		try {
			data = portRequestRepository.findByIdForNumberOrders(id);
			for (Portnumber current : data.getPortnumberList()) {
				current.getReasonList();
			}
			logger.debug(CATEGORY, data, clazz.getCanonicalName(), methodName, "Query portrequest ", "", true, "0", 0L);
		} catch (Exception e) {
			data = null;
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, "Error getting portrequest ", e.getMessage(), 0L);
		}
		return data;
	}

	@Transactional
	public Portrequest getByIdPortrequest(Integer id) {

		Portrequest data = null;
		String methodName = "getByIdPortrequest";
		try {
			data = portRequestRepository.findByIdPortRequest(id);
			for (Portnumber current : data.getPortnumberList()) {
				current.getReasonList();
			}
			logger.debug(CATEGORY, data, clazz.getCanonicalName(), methodName, "Query portrequest", "", true, "0", 0L);

		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR, e.getMessage(), 0L);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), AppConstants.FAIL_MSJ));
		}

		return data;
	}

	@Transactional
	public Portrequest getPortrequestWithoutSatelite(String id) {
		Portrequest data = null;
		String methodName = "getPortrequestWithoutSatelite";
		try {
			data = portRequestRepository.findByIdAscpWithoutSatelite(id);
			for (Portnumber current : data.getPortnumberList()) {
				current.getReasonList();
			}
			logger.debug(CATEGORY, data, clazz.getCanonicalName(), methodName, "Query portrequest", "", true, "0", 0L);

		} catch (Exception e) {
			data = null;
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, "Error getting portrequest", e.getMessage(), 0L);
		}
		return data;
	}

	@Transactional
	public List<Portnumber> getPortNumberById(Integer id) {
		List<Portnumber> data = null;
		String methodName = "getPortNumberById";
		try {
			data = portNumberRepository.findByReasonByPortrequestId(id);
			for (Portnumber current : data) {
				current.getReasonList();
			}
		} catch (Exception e) {
			data = null;
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
		}
		return data;
	}

	public Portrequest save(Portrequest portrequest) {
		try {
			portrequest = portRequestRepository.save(portrequest);
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR, e.getMessage(), 0L);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), "Falla al guardar solicitud."));
		}
		return portrequest;
	}

	public Requesttype getRequestType(String prefix) {
		Requesttype requesttypeId = null;
		try {
			requesttypeId = requestTypeRepository.findRequestTypeByPrefix(prefix);
		}catch(Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR, e.getMessage(), 0L);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), AppConstants.FAIL_MSJ));
		}
		
		
	return requesttypeId;
	}
	
	public Integer getTransactionsForNumber(String msisdn, List<String> prefixList, List<String> statusList){
		Integer solicitudes = 0;
		try {
			solicitudes = portRequestRepository.countPortRequestByNumberAndRequestTypeAndStatus(msisdn, prefixList,
					statusList);
		}catch(Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					e.getMessage(), 0L, e);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), AppConstants.FAIL_MSJ));
		}
		
		return solicitudes;
	}
	
	public Integer getPortrequestStatusBySat(String msisdn, List<String> prefixList, List<String> statusList, Long sateliteId) {
		Integer solicitudes = 0;
		try {
			solicitudes = portRequestRepository.countPortRequestStatusBySatelite(msisdn, prefixList, statusList,
					sateliteId);
		}catch(Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					e.getMessage(), 0L, e);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), AppConstants.FAIL_MSJ));
		}
		
		return solicitudes;
	}
	
	public Integer getPortrequestByOthers(String msisdn, List<String> prefixList, List<String> statusList, Long sateliteId) {
		Integer solicitudes = 0;
		try {
			solicitudes = portRequestRepository.countPortRequestStatusByDiferentSatelite(msisdn, prefixList, statusList,
					sateliteId);
		}catch(Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					e.getMessage(), 0L, e);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), AppConstants.FAIL_MSJ));
		}
		return solicitudes;
	}
	
	public Portrequest getPortrequestAscp(String msisdn) {
		Portrequest portRequest =  null;
		List<Portrequest> portRequestList = null;
		List<String> statusList = null;
		List<String> prefixList = null;
		statusList = configurationMapper.getStatusListOther();
		prefixList = configurationMapper.getPrefixList();
		try {
			portRequestList = portRequestRepository.findOrderIdByMsisdn(prefixList, msisdn, statusList);
			if(!portRequestList.isEmpty()) {
				portRequest = portRequestList.get(0);
			}
		}catch(Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					e.getMessage(), 0L, e);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), AppConstants.FAIL_MSJ));
		}
		return portRequest;
	}
	
	@Transactional
	public List<Portrequest> getPortRequestAgended(String status, String prefix){
		List<Portrequest> data = new ArrayList<>();
	    String methodName = "getPortRequestById";
	    try{
	      data = portRequestRepository.findRequestAgended(status, prefix);
	      for(Portrequest currentRequest : data) {
	    	  for(Portnumber currentNumber : currentRequest.getPortnumberList()) {
	    		  currentNumber.getReasonList();  
	    	  }
	      }
	    }catch (Exception e) {
	      data = null;
	      logger.error(CATEGORY,  clazz.getCanonicalName(), methodName, 
	    		  "Error getting portrequest", 
	    		  "Error getting the record",
	    		  0L, e);
	    }
	    return data;  
	  }
}