package com.tigo.ea.porta.debitcut.service.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppServiceException;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.model.dao.Operator;
import com.tigo.ea.porta.model.repository.OperatorRepository;


/**
 * The Class OperationsPortRequestRepository.
 *
 * @author HTC-Alexcst
 */

@Component
public class OperationsRepository {

	private static final Logger logger = LoggerFactory.getLogger(OperationsRepository.class);
	
	private final Class<?> clazz = this.getClass();

	@Autowired
	private OperatorRepository operatorRepository;


	private static final String CATEGORY = "repository";
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired 
	private Environment env;

	/**
	 * Find operator by routing number.
	 *
	 * @param routingNumber the routing number
	 * @return the operator
	 */
	public Operator findOperatorByRoutingNumberNull(String routingNumber){
		Operator operator = null;
		try{
			if(routingNumber.length() <= 1){
				routingNumber = "890"+routingNumber;
			}else if(routingNumber.length() != 4){
				routingNumber = "89"+routingNumber;
			}

			operator = operatorRepository.findOperatorByRoutingNumber(routingNumber);

		}catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR, e.getMessage(), 0L);
			throw new AppServiceException(env.getProperty(AppConstants.PORTA_CODE_DB),
					String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), "Falla en consulta."));
		}	
		return operator;	
	}
	
	

}
