package com.tigo.ea.porta.debitcut.service.retry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com._4gtss.npc.soap.service.consumer.Status;
import com.htc.ea.util.util.TimeChronometer;
import com.tigo.ea.porta.debitcut.service.consumer.NpcServiceConsumer;
import com.tigo.ea.porta.debitcut.service.dto.AscpGenericResponse;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

@Component
public class NpDebtRequestRetry {

	@Autowired
	private LoggerUtil logger;
	@Autowired
	@Qualifier("retryServices")
	private RetryTemplate retryServices;
	@Autowired
	private NpcServiceConsumer npcServiceConsumer;
	
	private static final Logger log = LoggerFactory.getLogger(NpDebtRequestRetry.class);
	private static final String CATEGORY = "service";
	private final Class<?> clazz = this.getClass();
	
	public AscpGenericResponse npDebtRequestRetry(NPDebtRequest1 request) {
		AscpGenericResponse result = new AscpGenericResponse();
		String methodName = "recipientRejectRetry";
		TimeChronometer time = new TimeChronometer();
		try {
			logger.info(AppConstants.CATEGORY_HANDLER_SOAP, request, clazz.getCanonicalName(), methodName,
					"Request retry", request.toString(), true, "0", 0L);
			result = retryServices.execute(context -> {
				log.info(String.format("\tRetry count  npDebtRequest ->  %s ", context.getRetryCount()));
				return npcServiceConsumer.npDebtRequest(request, context.getRetryCount());
			});
			time.stop();
			logger.info(AppConstants.CATEGORY_HANDLER_SOAP, result, clazz.getCanonicalName(), methodName,
					AppConstants.RESPONSE, result.toString(), true, "0", time.elapsedMilisUntilLastStop());
		} catch (RemoteAccessException remoteException) {
			log.info("retry executeTransaction Error generado por remoteAccessException...........");
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR,
					"retry executeTransaction Error generado por remoteAccessException", time.elapsedMilisUntilLastStop(), remoteException);
			result = new AscpGenericResponse();
			result.setStatus(Status.FAILURE);
			result.setDescriptionError(remoteException.getMessage());
		} catch (Exception e) {
			log.info("retry executeTransaction Error generado por Exception.......................");
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR,
					"\"retry executeTransaction Error generado por Exception.......................\"", time.elapsedMilisUntilLastStop(), e);
			result = new AscpGenericResponse();
			result.setStatus(Status.FAILURE);
			result.setDescriptionError(e.getMessage());
		}
		return result;
	}
}
