package com.tigo.ea.porta.debitcut.service.rule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com._4gtss.npc.soap.service.consumer.NpDebtRequest;
import com._4gtss.npc.soap.service.consumer.PhoneNumberList;
import com._4gtss.npc.soap.service.consumer.Range;
import com.htc.ea.util.configentry.ConfigurationService;
import com.tigo.ea.porta.debitcut.service.dto.AscpGenericResponse;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutSendASCPRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutSendASCPResponse;
import com.tigo.ea.porta.debitcut.service.dto.EvaluateResponseAscp;
import com.tigo.ea.porta.debitcut.service.repository.OperationPortrequestRepository;
import com.tigo.ea.porta.debitcut.service.retry.NpDebtRequestRetry;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppServiceException;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.commons.CataloguePortRequestStatus;
import com.tigo.ea.porta.model.dao.Operator;
import com.tigo.ea.porta.model.dao.Portnumber;
import com.tigo.ea.porta.model.dao.Portrequest;
import com.tigo.ea.porta.model.repository.OperatorRepository;

@Service
public class ChangeStatusRule {

	public static final Logger log = LoggerFactory.getLogger(ChangeStatusRule.class);

	private final Class<?> clazz = this.getClass();

	@Autowired
	private AppUtil appUtil;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private OperatorRepository operatorRepository;
	@Autowired
	private Environment env;
	@Autowired
	private OperationPortrequestRepository operationPortrequestRepository;
	@Autowired 
	private NpDebtRequestRetry npDebtRequestRetry;
	@Autowired
	private EvaluateResponseAscp evaluateResponseAscp;

	private static final String CATEGORY = "service";

	@Autowired
	private LoggerUtil logger;

	public DebitCutSendASCPResponse execute(DebitCutSendASCPRequest request) {
		List<String> validStatus = new ArrayList<>();
		List<String> validType = new ArrayList<>();
		
		DebitCutSendASCPResponse response = new DebitCutSendASCPResponse();
		String methodName = "execute";
		Portrequest portRequest = null;
		if (this.appUtil.isIntegerNumber(request.getRequestId())) {
			validStatus.add(env.getProperty(AppConstants.PORTA_STATUS_VALID));

			validType.add(env.getProperty(AppConstants.PORTA_PREFIX_DEVCUT_OUT));
			Integer id = Integer.valueOf(request.getRequestId());
			try {
				portRequest = operationPortrequestRepository.getByIdPortrequest(id);
				if (portRequest != null) {
					logger.info(CATEGORY, request, clazz.getCanonicalName(), methodName,
							"Request< " + request.getRequestId(), ">", null, null, 0L);
					if (validStatus.contains(portRequest.getStatus())
							&& validType.contains(portRequest.getRequesttypeId().getPrefix())) {
						portRequest.setStatus(CataloguePortRequestStatus.PENDIENTE.getName());
						operationPortrequestRepository.save(portRequest);
						if (this.inTime()) {
							log.info("Horario permitido..... ");
							NpDebtRequest npDebtRequest = new NpDebtRequest();
							npDebtRequest.setOriginalNpOrderId(portRequest.getOriginalNporderId());
							String routingnumber = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
							String senderId = routingnumber.substring(2, 4);
							npDebtRequest.setSenderId(senderId);
							PhoneNumberList phone = new PhoneNumberList();
							List<Portnumber> portnumberList = null;
							portnumberList = portRequest.getPortnumberList();

							for (Portnumber currentNumber : portnumberList) {
								Range rango = new Range();
								rango.setPhoneNumberEnd(currentNumber.getMsisdn());
								rango.setPhoneNumberStart(currentNumber.getMsisdn());
								phone.getRanges().add(rango);
							}
							npDebtRequest.setOriginalNpOrderId(portRequest.getOriginalNporderId());
							npDebtRequest.setPhoneNumberList(phone);
							NPDebtRequest1 requestAscp = new NPDebtRequest1();
							requestAscp.setArg0(npDebtRequest);
							AscpGenericResponse responseAscp = npDebtRequestRetry.npDebtRequestRetry(requestAscp);
							logger.info(CATEGORY, responseAscp, clazz.getCanonicalName(), methodName, "Response",
									responseAscp.toString(), true, "0", 0L);
							portRequest = evaluateResponseAscp.evaluateAscpResponseProcess(true, responseAscp, portRequest);
							for (Portnumber currentNumber : portRequest.getPortnumberList()) {
								currentNumber.setStatus(portRequest.getStatus());
							}
							portRequest = operationPortrequestRepository.save(portRequest);
							String message = "";
							message = validStatus.contains(portRequest.getStatus()) ? ""
									: String.format(AppConstants.ESTADO_SOLICITUD, portRequest.getStatus());
							if (log.isInfoEnabled())
								log.info(String.format(AppConstants.ESTADO_SOLICITUD, portRequest.getStatus()));
							response.setStatus(portRequest.getStatus());
							response.setGatewayId(portRequest.getGatewayId());
							response.setTransactionId(String.valueOf(portRequest.getId()));
							response.setCodeResponse(
									portRequest.getStatus().equals(CataloguePortRequestStatus.PENDIENTE.getName())
											? env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL)
											: env.getProperty(AppConstants.PORTA_CODE_ERROR));
							response.setCodeDescription(
									portRequest.getStatus().equals(CataloguePortRequestStatus.PENDIENTE.getName())
											? String.format(env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL), message)
											: String.format(env.getProperty(AppConstants.PORTA_MSJ_ERROR), message));
							if (log.isInfoEnabled())
								log.info(String.format("Estado de la solicitud: changestatus %s",
										portRequest.getStatus()));
							logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName, "Response ",
									response.getCodeDescription(), true, response.getCodeResponse(), 0L);

						} else {// ok
							log.info("---------------Horario no permitido...");			
							String message = "";
							message = validStatus.contains(portRequest.getStatus()) ? ""
									: ",Solicitud agendada no se encuentra en horario permitido.";
							portRequest.setStatus(env.getProperty(AppConstants.PORTA_STATUS_AGENDADO));
							for (Portnumber currentNumber : portRequest.getPortnumberList()) {
								currentNumber.setStatus(env.getProperty(AppConstants.PORTA_STATUS_AGENDADO));
							}
							Portrequest portRequestResponse = operationPortrequestRepository.save(portRequest);
							response.setStatus(portRequestResponse.getStatus());
							response.setGatewayId(portRequestResponse.getGatewayId());
							response.setTransactionId(portRequestResponse.getTransactionId());
							response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL));
							response.setCodeDescription(
									String.format(env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL), message));
							if (log.isInfoEnabled())
								log.info(String.format(AppConstants.ESTADO_SOLICITUD,
										CataloguePortRequestStatus.AGENDADO.getName()));
							logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName, AppConstants.RESPONSE,
									response.getCodeDescription(), true, response.getCodeResponse(), 0L);
						}
					} else {
						String message = "";
						message = validStatus.contains(portRequest.getStatus()) ? ""
								: ",Status de la Solicitud no acepta la accion";
						message += validType.contains(portRequest.getRequesttypeId().getPrefix()) ? ""
								: ", Tipo Solicitud no corresponde al servicio";
						response.setStatus(portRequest.getStatus());
						response.setGatewayId(portRequest.getGatewayId());
						response.setTransactionId(String.valueOf(portRequest.getId()));
						response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_SERVICE));
						response.setCodeDescription(
								String.format(env.getProperty(AppConstants.PORTA_MSJ_SERVICE), message));
						logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName, AppConstants.RESPONSE,
								response.getCodeDescription(), true, response.getCodeResponse(), 0L);
					}
				} else {
					response.setStatus(env.getProperty(AppConstants.PORTA_STATUS_ERROR));
					response.setGatewayId(UUID.randomUUID().toString().replaceAll("-", ""));
					response.setTransactionId("");
					response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_DB));
					response.setCodeDescription(
							String.format(env.getProperty(AppConstants.PORTA_MSJ_DB), ",Solicitud no encontrada"));
					logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName, AppConstants.ERROR,
							response.getCodeDescription(), false, response.getCodeResponse(), 0L);
				}

			} catch (AppServiceException e) {
				logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
				response.setCodeResponse(e.getCode());
				response.setCodeDescription(e.getMessage());
			} catch (Exception e) {
				logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
				response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_ERROR));
				response.setCodeDescription(env.getProperty(AppConstants.PORTA_MSJ_ERROR));
			}
		} else {
			response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_ERROR));
			response.setCodeDescription(env.getProperty(AppConstants.PORTA_MSJ_ERROR));
			logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName, AppConstants.ERROR,
					response.getCodeDescription(), false, response.getCodeResponse(), 0L);
		}
		return response;
	}

	public Operator getOwnerOperator(String number) {
		List<Operator> operators = operatorRepository.findAll();
		for (Operator owner : operators) {
			if (executeValidateRegex(number, owner.getRegex())) {
				return owner;
			}
		}
		return null;
	}

	private boolean executeValidateRegex(String data, String regex) {
		boolean result = false;
		try {
			Pattern queryLangPattern = Pattern.compile(regex);
			Matcher matcher = queryLangPattern.matcher(data);
			result = matcher.matches();
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					e.getMessage(), 0L, e);
			result = false;
		}

		return result;
	}

	private boolean inTime() {
		boolean value = false;
		try {
			int s = 7;
			Date f = null;
			Date f2 = null;
			Set<String> schedules = configurationService.getValues(AppConstants.DEBITCUTIN_ONLINE_TIME);
			for (String schedule : schedules) {
				CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(schedule);
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.MILLISECOND, 0);
				f = calendar.getTime();
				f2 = f;
				calendar.add(Calendar.SECOND, s * -1);
				f = calendar.getTime();
				int n = s + 1;
				for (int i = 0; i < n; i++) {
					f = cronSequenceGenerator.next(f);
					value = f.equals(f2);
					if (value) {
						if (log.isInfoEnabled())
							log.info(String.format("Corte por deuda on line ---> %s", schedule));
						return value;
					}
				}
			}
		} catch (Exception ex) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					ex.getMessage(), 0L, ex);
		}
		return value;
	}

}
