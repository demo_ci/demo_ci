package com.tigo.ea.porta.debitcut.service.rule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com._4gtss.npc.soap.service.consumer.NpDebtRequest;
import com._4gtss.npc.soap.service.consumer.NpcServiceImpl;
import com._4gtss.npc.soap.service.consumer.PhoneNumberList;
import com._4gtss.npc.soap.service.consumer.Range;
import com.htc.ea.util.configentry.ConfigurationService;
import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.commons.ConfigurationMapper;
import com.tigo.ea.porta.debitcut.service.consumer.NpcServiceConsumer;
import com.tigo.ea.porta.debitcut.service.core.CatalogueBusinessService;
import com.tigo.ea.porta.debitcut.service.dao.DataAccess;
import com.tigo.ea.porta.debitcut.service.dto.AscpGenericResponse;
import com.tigo.ea.porta.debitcut.service.dto.BodyRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutResponse;
import com.tigo.ea.porta.debitcut.service.dto.Detail;
import com.tigo.ea.porta.debitcut.service.dto.EvaluateResponseAscp;
import com.tigo.ea.porta.debitcut.service.dto.MsisdnResponseList;
import com.tigo.ea.porta.debitcut.service.dto.QueryCustomerDto;
import com.tigo.ea.porta.debitcut.service.dto.SendAcspProcess;
import com.tigo.ea.porta.debitcut.service.repository.OperationPortrequestRepository;
import com.tigo.ea.porta.debitcut.service.repository.OperationsRepository;
import com.tigo.ea.porta.debitcut.service.retry.NpDebtRequestRetry;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.AppServiceException;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.commons.CatalogueMonitorStatus;
import com.tigo.ea.porta.model.commons.CataloguePortRequestStatus;
import com.tigo.ea.porta.model.dao.Changestatus;
import com.tigo.ea.porta.model.dao.Operator;
import com.tigo.ea.porta.model.dao.Portnumber;
import com.tigo.ea.porta.model.dao.Portrequest;
import com.tigo.ea.porta.model.dao.Reason;
import com.tigo.ea.porta.model.dao.Requesttype;
import com.tigo.ea.porta.model.dao.Satelite;
import com.tigo.ea.porta.model.dto.SateliteDto;
import com.tigo.ea.porta.model.repository.OperatorRepository;
import com.tigo.ea.porta.model.repository.PortNumberRepository;
import com.tigo.ea.porta.model.repository.PortRequestRepository;

@SuppressWarnings("unused")
@Service
public class DebitCutRule {

	private static final Logger log = LoggerFactory.getLogger(DebitCutRule.class);

	private static final String CATEGORY = "service";

	@Autowired
	private AppUtil appUtil;

	@Autowired
	private LoggerUtil logger;

	private final Class<?> clazz = this.getClass();

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private DataAccess data;

	@Autowired
	private Environment env;

	@Autowired
	private NpcServiceImpl npcServiceImpl;

	@Autowired
	private OperatorRepository operatorRepository;

	@Autowired
	private PortNumberRepository portNumberRepository;

	@Autowired
	private PortRequestRepository portRequestRepository;

	@Autowired
	private OperationPortrequestRepository operationPortrequestRepository;

	@Autowired
	private OperationsRepository operationsRepository;

	@Autowired
	private SendAcspProcess sendAcspProcess;

	@Autowired
	private CatalogueBusinessService catalogueBusinessService;

	@Autowired
	private NpcServiceConsumer npcServiceConsumer;

	@Autowired
	private NpDebtRequestRetry npDebtRequestRetry;

	@Autowired
	private EvaluateResponseAscp evaluateResponseAscp;

	@Autowired
	@Qualifier("retryServices")
	private RetryTemplate retryServices;

	@Autowired
	private ConfigurationMapper configurationMapper;

	public DebitCutResponse execute(DebitCutRequest request) {
		String methodName = "execute";
		String originalNpOrderId = "";
		boolean validNumber = false;
		String codeResponse = "";
		String codeDescription = "";
		Portrequest portRequest = new Portrequest();
		Long sateliteId = 0L;
		Boolean sendAscp = false;
		Map<String, String> originalOrderId = new HashMap<>();
		Map<String, Integer> parameter = new HashMap<>();
		Map<String, Date> portacionDates = new HashMap<>();
		DebitCutResponse response = new DebitCutResponse();
		response.setGatewayID(UUID.randomUUID().toString().replace("-", ""));
		response.setTransactionID(request.getHeader().getTransaccionID());
		response.setMsisdnResponseList(new MsisdnResponseList());
		SateliteDto satelite = null;
		try {
			for (Map.Entry<Long, SateliteDto> currentSatelite : catalogueBusinessService.getCatalogueSatelite()
					.entrySet()) {
				satelite = currentSatelite.getValue();
				if (request.getBody().getChannel().equals(satelite.getRequestChannel())
						&& request.getBody().getUser().equals(satelite.getRequestUser())) {
					sateliteId = satelite.getId();
					sendAscp = satelite.getSendASCP();
				}
			}
			saveNewRequest(request, portRequest, sateliteId);
			TransactionIdUtil.setMsisdn(String.valueOf(portRequest.getId()));
			Boolean isOut = false;
			Boolean isOutfromNetwork = false;
			Boolean isValid = false;
			Boolean inProgres = false;
			Boolean sameSatelite = false;
			Boolean isInTime = false;
			Boolean alreadyInProcess = false;
			List<Detail> responseItemList = new ArrayList<>();
			List<String> requestNumberList = new ArrayList<>();
			requestNumberList.addAll(request.getBody().getMsisdnList().getMsisdn());
			// validamos si el numero tiene un BP y no ha sido retormado AD/ADO
			log.info("Validando regla: El numero esta en otro operador y no fue retormado.");
			isOut = processRule(requestNumberList, responseItemList, originalOrderId, parameter, portacionDates);
			if (!isOut) {
				// Validamos si el numero fue portado desde tigo
				log.info("Validando regla: El numero fue portado desde tigo");
				isOutfromNetwork = outNetworkRule(requestNumberList, responseItemList);
			} else {
				// preparar el response
				if (log.isInfoEnabled())
					log.info(String.format("Estado de la solicitud: %s", portRequest.getStatus()));
				fillRejectedPortrequest(response, portRequest, responseItemList, request, requestNumberList,
						originalOrderId, parameter);
				return response;
			}
			if (!isOutfromNetwork) {
				for (String currentNumber : requestNumberList) {
					// verificamos si existe fecha de AD/ADO
					log.info("Validando regla: El numero ha sido retornado?");
					if (portacionDates.containsKey(AppConstants.DATE_ADADO + currentNumber)) {
						// validamos si la fecha de AD/ADO fue antes de un BP
						log.info("Validando regla: La fecha del ADCNT/ADCNO es mayor al BP?");
						if (portacionDates.get(AppConstants.DATE_ADADO + currentNumber)
								.before(portacionDates.get(AppConstants.DATE_BP + currentNumber))) {
							log.info("No cumple regla. El numero fue retornado...");

							logger.info(CATEGORY, null, clazz.getCanonicalName(), methodName,
									String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
											"No cumple regla. El numero fue retornado..."),
									"No cumple regla. El numero fue retornado...", true, AppConstants.PORTA_CODE_RULE,
									0L);
							responseItemList.add(detailResponse(currentNumber, AppConstants.PORTA_STATUS_INVALID,
									AppConstants.PORTA_CODE_RULE, AppConstants.PORTA_MSJ_RULE,
									": El numero fue retornado.", null));
							isValid = false;
							break;
						} else {
							isValid = true;
						}
					} else {
						isValid = true;
					}
				}
			}
			if (isValid) {
				// validamos si el numero tiene otra solicitud en curso por el mismo satelite
				log.info("Validando regla: El numero tiene otra solicitud en curso por el mismo satelite?");
				sameSatelite = this.debitcutSameSatelite(requestNumberList, responseItemList, portRequest, sateliteId);
				if (!sameSatelite) {
					// //Validamos si existe otra solicitud en curso
					log.info("Validando regla: el numero tiene otra solicitud en curso ");
					inProgres = this.debitCutValidate(requestNumberList, responseItemList);
					if (!inProgres) {
						log.info("Valindando regla: El numero corresponde a la solicitud de portout");
						validNumber = this.validateNumbersforOrder(requestNumberList, responseItemList,
								originalOrderId);
					} else {
						fillRejectedPortrequest(response, portRequest, responseItemList, request, requestNumberList,
								originalOrderId, parameter);
						return response;
					}
					if (validNumber) {
						fillRejectedPortrequest(response, portRequest, responseItemList, request, requestNumberList,
								originalOrderId, parameter);
						return response;
					} else {
						// validamos si el numero tiene otra solicitud de corte por otro satelite
						alreadyInProcess = this.validateProcessOther(requestNumberList, sateliteId);
						if (alreadyInProcess) {
							// Actualizar estado y copiar los datos de la solicitud enviada al ascp
							Portrequest requestAscp = null;
							for (String currentNumber : requestNumberList) {
								requestAscp = operationPortrequestRepository.getPortrequestAscp(currentNumber);
							}
							portRequest.setTransactionId(requestAscp.getTransactionId());
							portRequest.setOperationidAscp(requestAscp.getOperationidAscp());
							portRequest.setDonorId(requestAscp.getDonorId());

							// actualizar solicitud en estado pendiente.....
							for (String item : requestNumberList) {
								response.getMsisdnResponseList().getDetail()
										.add(new Detail(requestAscp.getStatus(),
												env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL),
												env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL)
														+ "; ya existe una o mas solicitudes de corte.",
												item));
							}
							codeResponse = env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL);
							codeDescription = env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL);

							fillChangeStatus(portRequest, requestAscp.getStatus());

							Portrequest responseData = null;
							this.parseEntities(request, portRequest, response, requestAscp.getStatus(), originalOrderId,
									parameter);
							log.info(
									"------------------------- -------------------------------------------------------");
							if (log.isInfoEnabled())
								log.info(String.format("Estado de la solicitud: %s ", portRequest.getStatus()));
							responseData = operationPortrequestRepository.save(portRequest);

							prepareResponse(response, responseData, codeResponse, codeDescription);

							if (log.isInfoEnabled())
								log.info(String.format(AppConstants.ESTADO_SOLICITUD, responseData.getStatus()));
							logger.info(CATEGORY, requestNumberList, clazz.getCanonicalName(), methodName,
									AppConstants.PORTA_MSJ_SUCCESSFUL,
									String.format(AppConstants.ESTADO_SOLICITUD, requestAscp.getStatus()),
									// CataloguePortRequestStatus.PENDIENTE.getName()),

									true, codeResponse, 0L);
						} else {
							// Enviar solicitud al ASCP?
							if (sendAscp) {
								log.info("Envio de solicitud al ASCP");
								// valida el horario permitido para el envio al ascp
								isInTime = isInTimeRule(requestNumberList, responseItemList);
								if (isInTime) {
									for (String item : requestNumberList) {
										response.getMsisdnResponseList().getDetail()
												.add(new Detail(CataloguePortRequestStatus.VALIDO.getName(),
														env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL),
														env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL),	item));
									}
									this.parseEntities(request, portRequest, response, CataloguePortRequestStatus.VALIDO.getName(), originalOrderId,
											parameter);
									response.getMsisdnResponseList().getDetail().clear();
									log.info("En horario permitido.....");
									String routingnumber = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
									String tigo = routingnumber.substring(2, 4);
									NpDebtRequest npDebtRequest = new NpDebtRequest();
									npDebtRequest.setOriginalNpOrderId(
											originalOrderId.get(AppConstants.ORIGINAL_NP_ORDERID));
									npDebtRequest.setSenderId(tigo);
									PhoneNumberList phone = new PhoneNumberList();
									Iterator<String> items = requestNumberList.iterator();
									while (items.hasNext()) {
										String item = items.next();
										Range rango = new Range();
										rango.setPhoneNumberEnd(item);
										rango.setPhoneNumberStart(item);
										phone.getRanges().add(rango);
									}
									npDebtRequest.setPhoneNumberList(phone);
									logger.info(CATEGORY, npDebtRequest, clazz.getCanonicalName(), methodName,
											"Request to ascp <", npDebtRequest.toString() + ">", true, null, 0L);
									NPDebtRequest1 requestToAscp = new NPDebtRequest1();
									requestToAscp.setArg0(npDebtRequest);

									// enviamos request al ascp
									AscpGenericResponse ascpResponse = npDebtRequestRetry
											.npDebtRequestRetry(requestToAscp);

									// Envio de solicitud al ascp
									portRequest = evaluateResponseAscp.evaluateAscpResponseProcess(true, ascpResponse,
											portRequest);
									if (log.isInfoEnabled())
										log.info(String.format("Estado de la solicitud: sendAscp %s",
												portRequest.getStatus()));
									log.info("----------estado correcto: "
											+ portRequest.getStatus());
									// preparar response
									
									for (String item : requestNumberList) {
										response.getMsisdnResponseList().getDetail()
												.add(new Detail(portRequest.getStatus(), portRequest.getStatus()
														.equals(CataloguePortRequestStatus.PENDIENTE.getName())
																? env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL)
																: env.getProperty(AppConstants.PORTA_CODE_ERROR),
														portRequest.getStatus()
																.equals(CataloguePortRequestStatus.PENDIENTE.getName())
																		? env.getProperty(
																				AppConstants.PORTA_MSJ_SUCCESSFUL)
																		: env.getProperty(AppConstants.PORTA_MSJ_ERROR),
														item));
									}
									codeResponse = portRequest.getStatus()
											.equals(CataloguePortRequestStatus.PENDIENTE.getName())
													? env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL)
													: env.getProperty(AppConstants.PORTA_CODE_ERROR);
									codeDescription = portRequest.getStatus()
											.equals(CataloguePortRequestStatus.PENDIENTE.getName())
													? env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL)
													: env.getProperty(AppConstants.PORTA_MSJ_ERROR);

									Portrequest responseData = null;
									this.parseEntities(request, portRequest, response, portRequest.getStatus(),
											originalOrderId, parameter);
									responseData = operationPortrequestRepository.save(portRequest);

									prepareResponse(response, responseData, codeResponse, codeDescription);
									if (log.isInfoEnabled())
										log.info(
												String.format(AppConstants.ESTADO_SOLICITUD, responseData.getStatus()));
									logger.info(CATEGORY, response, clazz.getCanonicalName(), AppUtil.getMethodName(),
											AppConstants.RESPONSE, portRequest.getStatus(), codeResponse.equals("0"),
											codeResponse, 0L);

								} else {
									log.info("------------------------------------ Horario no permitido...");
									for (String item : requestNumberList) {
										response.getMsisdnResponseList().getDetail()
												.add(new Detail(env.getProperty(AppConstants.PORTA_STATUS_AGENDADO),
														env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL),
														env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL), item));
									}
									codeResponse = env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL);
									codeDescription = env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL);

									// metodo para almacenar el changeStatus de la solicitud
									fillChangeStatus(portRequest, CataloguePortRequestStatus.AGENDADO.getName());

									Portrequest responseData = null;
									this.parseEntities(request, portRequest, response,
											CataloguePortRequestStatus.AGENDADO.getName(), originalOrderId, parameter);
									responseData = operationPortrequestRepository.save(portRequest);
									if (log.isInfoEnabled())
										log.info("Estado de la solicitud: horario no permitido-----------------");
									prepareResponse(response, responseData, codeResponse, codeDescription);

									if (log.isInfoEnabled())
										log.info(
												String.format(AppConstants.ESTADO_SOLICITUD, responseData.getStatus()));
									logger.info(CATEGORY, requestNumberList, clazz.getCanonicalName(), methodName,
											AppConstants.RESPONSE,
											String.format(AppConstants.ESTADO_SOLICITUD,
													CataloguePortRequestStatus.AGENDADO.getName()),
											true, codeResponse, 0L);
								}
							} else {
								for (String item : requestNumberList) {
									response.getMsisdnResponseList().getDetail()
											.add(new Detail(CataloguePortRequestStatus.VALIDO.getName(),
													env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL),
													env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL), item));
								}
								codeResponse = env.getProperty(AppConstants.PORTA_CODE_SUCCESSFUL);
								codeDescription = env.getProperty(AppConstants.PORTA_MSJ_SUCCESSFUL);

								portRequest.setOperationidAscp(originalNpOrderId);
								Portrequest responseData = null;
								this.parseEntities(request, portRequest, response,
										CataloguePortRequestStatus.VALIDO.getName(), originalOrderId, parameter);
								log.info(
										"--------------------------------------------------------------------------------");
								if (log.isInfoEnabled())
									log.info(String.format("Estado de la solicitud: en dos pasos... %s ",
											portRequest.getStatus()));
								responseData = operationPortrequestRepository.save(portRequest);
								prepareResponse(response, responseData, codeResponse, codeDescription);
								if (log.isInfoEnabled())
									log.info(String.format(AppConstants.ESTADO_SOLICITUD, responseData.getStatus()));
								logger.info(CATEGORY, requestNumberList, clazz.getCanonicalName(), methodName,
										AppConstants.RESPONSE, String.format(AppConstants.ESTADO_SOLICITUD,
												CataloguePortRequestStatus.VALIDO.getName()),
										false, codeResponse, 0L);
							}
						}
					}
				} else {
					fillRejectedPortrequest(response, portRequest, responseItemList, request, requestNumberList,
							originalOrderId, parameter);
				}
			} else {
				fillRejectedPortrequest(response, portRequest, responseItemList, request, requestNumberList,
						originalOrderId, parameter);
			}
		} catch (AppServiceException e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
			response.setCodeResponse(e.getCode());
			response.setCodeDescription(e.getMessage());
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
			response.setCodeResponse(env.getProperty(AppConstants.PORTA_CODE_ERROR));
			response.setCodeDescription(env.getProperty(AppConstants.PORTA_MSJ_ERROR));
		}
		return response;
	}

	public void saveNewRequest(DebitCutRequest request, Portrequest portRequest, Long sateliteId) {
		Operator operador = null;
		Requesttype requesttypeId = null;

		String prefix = env.getProperty(AppConstants.PORTA_PREFIX_DEVCUT_OUT);
		requesttypeId = operationPortrequestRepository.getRequestType(prefix);
		Satelite sa = new Satelite();
		BodyRequest body = null;
		body = request.getBody();
		portRequest.setChannel(body.getChannel());
		portRequest.setUserlogin(body.getUser());
		portRequest.setRequesttypeId(requesttypeId);
		portRequest.setRemarks(body.getRemarks());
		portRequest.setStatus(CataloguePortRequestStatus.VALIDO.getName());
		sa.setId(sateliteId);
		portRequest.setSateliteId(sa);

		String donorID = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
		operador = operatorRepository.findOperatorByRoutingNumber(donorID);

		portRequest.setSenderId(operador);
		portRequest.setRequestDate(new Date());
		fillChangeStatus(portRequest, CataloguePortRequestStatus.VALIDO.getName());
		portRequest = operationPortrequestRepository.save(portRequest);
		if (log.isInfoEnabled())
			log.info(String.format(AppConstants.ESTADO_INICIAL, CataloguePortRequestStatus.VALIDO.getName()));
		logger.info(CATEGORY, portRequest, clazz.getCanonicalName(), AppUtil.getMethodName(), "Solicitud  ",
				portRequest.toString(), true, null, 0L);
	}

	public void prepareResponse(DebitCutResponse response, Portrequest portRequest, String responseCode,
			String responseDescription) {
		response.setCodeResponse(responseCode);
		response.setCodeDescription(responseDescription);
		response.setRequestId(String.valueOf(portRequest.getId()));
		response.setStatus(portRequest.getStatus());
		response.setTransactionID(String.valueOf(portRequest.getId()));
	}

	public void fillRejectedPortrequest(DebitCutResponse response, Portrequest portRequest,
			List<Detail> responseItemList, DebitCutRequest request, List<String> requestNumberList,
			Map<String, String> originalOrderId, Map<String, Integer> parameter) {
		String methodName = "fillRejectedPortrequest";
		String codeResponse = "";
		String codeDescription = "";
		response.getMsisdnResponseList().getDetail().addAll(responseItemList);
		codeResponse = env.getProperty(AppConstants.PORTA_CODE_OBSERVED);
		codeDescription = env.getProperty(AppConstants.PORTA_MSJ_OBSERVED);
		response.setCodeResponse(codeResponse);
		response.setCodeDescription(codeDescription);
		fillChangeStatus(portRequest, CataloguePortRequestStatus.RECHAZADO.getName());
		Portrequest responseData = null;
		this.parseEntities(request, portRequest, response, CataloguePortRequestStatus.RECHAZADO.getName(),
				originalOrderId, parameter);
		responseData = operationPortrequestRepository.save(portRequest);
		response.setRequestId(String.valueOf(responseData.getId()));
		response.setStatus(CataloguePortRequestStatus.RECHAZADO.getName());
		response.setTransactionID(String.valueOf(responseData.getId()));
		if (log.isInfoEnabled())
			log.info(String.format(AppConstants.ESTADO_SOLICITUD, responseData.getStatus()));
		logger.info(CATEGORY, response, clazz.getCanonicalName(), methodName,  "No cumplio regla",
				String.format(AppConstants.ESTADO_SOLICITUD, CataloguePortRequestStatus.RECHAZADO.getName()), false,
				codeResponse, 0L);
	}

	public void fillChangeStatus(Portrequest portRequest, String status) {
		Operator operador = null;
		// Elmacenar estado de solicitud como VALIDO
		String donor = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
		operador = operationsRepository.findOperatorByRoutingNumberNull(donor);
		Changestatus changestatus = new Changestatus();
		changestatus.setAutomatic(false);
		changestatus.setDatechange(new Date());
		changestatus.setIdPortrequest(portRequest);
		changestatus.setUserlogin(portRequest.getUserlogin());
		changestatus.setSenderId(operador);
		changestatus.setStatus(status);
		changestatus.setOperationReferenceId(TransactionIdUtil.getId());
		changestatus.setRemarks("Cambio de estado generado por Servicio");
		portRequest.getChangestatusList().add(changestatus);
		if (log.isInfoEnabled())
			log.info(String.format("Estado de la solicitud: fillChangeStatus - %s", portRequest.getStatus()));
	}

	// DebitCut pf2
	private boolean validateNumbersforOrder(List<String> requestNumberList, List<Detail> responseItemList,
			Map<String, String> originalOrderId) {
		boolean value = false;
		String methodName = "validateNumbersforOrder";
		Portrequest portrequestNumbers = null;
		List<String> portnumberList = new ArrayList<>();
		Iterator<String> items = requestNumberList.iterator();
		while (items.hasNext()) {
			String item = items.next();
			String originalNpOrderId = originalOrderId.get("idOrder" + item);
			portrequestNumbers = operationPortrequestRepository.getPortrequestWithoutSatelite(originalNpOrderId);

			if (portrequestNumbers != null) {
				for (Portnumber currentPortnumber : portrequestNumbers.getPortnumberList()) {
					String num = null;
					num = currentPortnumber.getMsisdn();
					portnumberList.add(num);
				}
				if (portnumberList.contains(item)) {
					originalOrderId.put(AppConstants.ORIGINAL_NP_ORDERID, portrequestNumbers.getOperationidAscp());
					value = false;
				} else {
					responseItemList.add(detailResponse(item, AppConstants.PORTA_STATUS_REJECTED,
							AppConstants.PORTA_CODE_RULE, AppConstants.PORTA_MSJ_RULE,
							": El numero no pertenece a la misma solicitud.", null));
					log.info("No cumple regla: El numero no pertenece a la misma solicitud");
					logger.info(CATEGORY, responseItemList, clazz.getCanonicalName(), methodName,
							String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
									"No cumple regla: El n�mero no pertenece a la misma solicitud"),
							": El n�mero no pertenece a la misma solicitud" + env.getProperty(AppConstants.PORTA_STATUS_OBSERVED), true,
							env.getProperty(AppConstants.PORTA_CODE_RULE), 0L);
					value = true;
					break;
				}
			} else {
				responseItemList
						.add(detailResponse(item, AppConstants.PORTA_STATUS_REJECTED, AppConstants.PORTA_CODE_DB,
								AppConstants.PORTA_MSJ_DB, ": Incongruencia en los datos del numero", null));
				log.info("No cumple regla. Incongruencia en los datos del numero.");
				logger.info(CATEGORY, responseItemList, clazz.getCanonicalName(), methodName,
						String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
								"No cumple regla: El n�mero no pertenece a la misma solicitud."),
						": El numero no pertenece a la misma solicitud." + env.getProperty(AppConstants.PORTA_STATUS_OBSERVED), true,
						env.getProperty(AppConstants.PORTA_CODE_RULE), 0L);
				value = true;
				items.remove();
			}
		}
		return value;
	}

	public boolean equalLists(List<String> a, List<String> b) {
		// comprobar que tienen el mismo tama�o y que no son nulos
		if ((a.size() != b.size()) || (a == null && b != null) || (a != null && b == null)
				|| (a == null && b == null)) {
			return false;
		}
		// ordenar las ArrayList y comprobar que son iguales
		Collections.sort(a);
		Collections.sort(b);
		return a.equals(b);
	}

	private boolean isInTimeRule(List<String> requestNumberList, List<Detail> responseItemList) {
		boolean value = false;
		Iterator<String> items = requestNumberList.iterator();
		while (items.hasNext()) {
			String item = items.next();
			if (this.inTime()) {
				value = true;
			} else {
				responseItemList.add(detailResponse(item, AppConstants.PORTA_STATUS_AGENDADO,
						AppConstants.PORTA_CODE_RULE, AppConstants.PORTA_MSJ_SUCCESSFUL,
						": Horario no permitido; solicitud almacenada", null));
				value = false;
			}
		}
		return value;
	}

	private void parseEntities(DebitCutRequest request, Portrequest portRequest, DebitCutResponse response,
			String portaStatus, Map<String, String> originalOrderId, Map<String, Integer> parameter) {
		List<Portnumber> portnumberList = new ArrayList<>();
		try {
			String receptor = "";
			Operator receptorId = null;
			Operator donorId = null;
			Requesttype requesttypeId = null;
			String prefix = env.getProperty(AppConstants.PORTA_PREFIX_DEVCUT_OUT);
			requesttypeId = operationPortrequestRepository.getRequestType(prefix);
			List<Detail> detailList = response.getMsisdnResponseList().getDetail();
			portRequest.setOriginalNporderId(originalOrderId.get("originalNpOrderId"));// senderId
			for (Detail number : detailList) {
				Portnumber portnumber;
				portnumber = new Portnumber();
				portnumber.setStatus(number.getStatus());
				portnumber.setStatuscode(number.getCode());
				portnumber.setStatusdescription(number.getCodeDescripcion());
				portnumber.setMsisdn(number.getMsisdn());
				portnumber.setPortrequestId(portRequest);
				portnumberList.add(portnumber);
				log.info("------------------------------" + "portnumber " + portnumber.getMsisdn() + "originalNpOrderId"
						+ portRequest.getOriginalNporderId() != null ? portRequest.getOriginalNporderId() : " ");
			}
			portRequest.setChannel(request.getBody().getChannel());
			receptor = String.valueOf(parameter.get("recipientId"));
			receptorId = operationsRepository.findOperatorByRoutingNumberNull(receptor);
			donorId = operatorRepository.findOne(1);
			portRequest.setReceptorId(receptorId);
			portRequest.setDonorId(donorId);
			portRequest.setPortnumberList(portnumberList);
			Date receptionDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(request.getBody().getDateTime());
			portRequest.setReceptiondate(new Date());
			portRequest.setRequestDate(receptionDate);
			portRequest.setRequesttypeId(requesttypeId);
			portRequest.setStatus(portaStatus);
			portRequest.setGatewayId(String.valueOf(response.getGatewayID()));
			portRequest.setUserlogin(request.getBody().getUser());
		} catch (AppServiceException e) {
			throw e;
		} catch (Exception ex) {
			logger.error(CATEGORY, clazz.getCanonicalName(), AppUtil.getMethodName(), AppConstants.ERROR,
					ex.getMessage(), 0L, ex);
		}
	}

	// validamos si el numero tiene una solicitud de corte por otro satelite
	private boolean validateProcessOther(List<String> requestNumberList, Long idSatelite) {
		boolean value = false;
		Iterator<String> items = requestNumberList.iterator();
		while (items.hasNext()) {
			String item = items.next();
			if (otherInProgress(item, idSatelite)) {
				value = true;
			}
		}
		return value;
	}

	// Metodo para validar si el numero tiene una solicitud de corte por otro
	// satelite
	private boolean otherInProgress(String msisdn, Long sateliteId) {
		int solicitudes = 0;
		boolean value = false;
		String methodName = "sameInProgress";
		try {
			List<String> statusList = null;
			List<String> prefixList = null;
			statusList = configurationMapper.getStatusListOther();
			prefixList = configurationMapper.getPrefixList();
			solicitudes = operationPortrequestRepository.getPortrequestByOthers(msisdn, prefixList, statusList,
					sateliteId);
			if (solicitudes > 0) {
				value = true;
			} else {
				value = false;
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
		}
		return value;
	}

	// validamos si el numero tiene una solicitud de corte por el mismo satelite
	private boolean debitcutSameSatelite(List<String> requestNumberList, List<Detail> responseItemList,
			Portrequest portRequest, Long idSatelite) {
		boolean value = false;
		String methodName = "debitcutSameSatelite";
		Iterator<String> items = requestNumberList.iterator();
		Integer id = portRequest == null ? null : portRequest.getId();
		while (items.hasNext()) {
			String item = items.next();
			if (sameInProgress(item, idSatelite)) {
				responseItemList.add(detailResponse(item, AppConstants.PORTA_STATUS_REJECTED,
						AppConstants.PORTA_CODE_RULE, AppConstants.PORTA_MSJ_RULE,
						": El numero tiene una solicitud de corte por el mismo sat�lite.", null));

				log.info("No cumple regla. El numero tiene una solicitud de corte por el mismo sat�lite.");

				logger.info(CATEGORY, null, clazz.getCanonicalName(), methodName,
						String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
								"No cumple regla. \"El numero tiene una solicitud de corte por el mismo sat�lite."),
						"El numero tiene una solicitud de corte por el mismo sat�lite. Status: "
								+ env.getProperty(AppConstants.PORTA_STATUS_REJECTED),
						true, env.getProperty(AppConstants.PORTA_CODE_RULE), 0L);

				items.remove();
				value = true;
			}
		}
		return value;
	}

	// Metodo para validar si el numero tiene una solicitud de corte por el mismo
	// satelite
	private boolean sameInProgress(String msisdn, Long sateliteId) {
		int solicitudes = 0;
		boolean value = false;
		String methodName = "sameInProgress";

		try {
			List<String> statusList = null;
			List<String> prefixList = null;
			statusList = configurationMapper.getStatusList();
			prefixList = configurationMapper.getPrefixList();
			solicitudes = operationPortrequestRepository.getPortrequestStatusBySat(msisdn, prefixList, statusList,
					sateliteId);
			if (solicitudes > 0) {
				value = true;
			} else {
				value = false;
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
		}
		return value;
	}

	// validamos si el numero tiene una solicitud en curso
	private boolean debitCutValidate(List<String> requestNumberList, List<Detail> responseItemList) {
		boolean value = false;
		Iterator<String> items = requestNumberList.iterator();
		while (items.hasNext()) {
			String item = items.next();
			if (inProgress(item)) {
				responseItemList.add(detailResponse(item, AppConstants.PORTA_STATUS_REJECTED,
						AppConstants.PORTA_CODE_RULE, AppConstants.PORTA_MSJ_RULE,
						": El numero tiene una solicitud de corte por otro satelite en curso.", null));
				log.info("No cumple regla. El numero tiene una solicitud de corte por otro sat�lite en curso.");
				logger.info(CATEGORY, null, clazz.getCanonicalName(), AppUtil.getMethodName(),
						String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
								"El numero tiene una solicitud de corte por otro satelite en curso."),
						"El numero tiene una solicitud de corte por otro satelite en curso.", true,
						env.getProperty(AppConstants.PORTA_CODE_RULE), 0L);
				items.remove();
				value = true;
			}
		}
		return value;
	}

	// Busca las solicitudes para el numero que esten en curso
	private boolean inProgress(String msisdn) {
		int solicitudes = 0;
		boolean value = false;
		String methodName = "inProgress";
		try {
			List<String> statusList = null;
			List<String> prefixList = null;
			statusList = configurationMapper.getStatusList();
			prefixList = configurationMapper.getPrefixList();
			solicitudes = operationPortrequestRepository.getTransactionsForNumber(msisdn, prefixList, statusList);
			if (solicitudes > 0) {
				value = true;
			} else {
				value = false;
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
		}
		return value;
	}

	// Metodo para validar si el numero ha sido portado desde tigo
	private boolean outNetworkRule(List<String> requestNumberList, List<Detail> responseItemList) {
		Boolean value = false;
		String methodName = "outNetworkRule";
		Iterator<String> items = requestNumberList.iterator();
		while (items.hasNext()) {
			String item = items.next();
			if (processOut(item)) {
				responseItemList
						.add(detailResponse(item, AppConstants.PORTA_STATUS_REJECTED, AppConstants.PORTA_CODE_RULE,
								AppConstants.PORTA_MSJ_RULE, ": El numero no ha sido portado desde Tigo.", null));
				log.info("No cumple regla. El numero no ha sido portado desde TIGO.");
				logger.info(CATEGORY, null, clazz.getCanonicalName(), methodName,
						String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
								"El numero no ha sido portado desde Tigo."),
						"El numero no ha sido portado desde Tigo.", true, env.getProperty(AppConstants.PORTA_CODE_RULE),
						0L);
				items.remove();
				value = true;
			}
		}
		return value;
	}

	private boolean processOut(String number) {
		Boolean value = false;
		String methodName = "processOut";
		List<String> type = new ArrayList<>();
		type.add(env.getProperty(AppConstants.PORTA_PREFIX_PORT_OUT));
		QueryCustomerDto request = new QueryCustomerDto();
		String network = env.getProperty(AppConstants.PORTA_TIGO_ROUTING_NUMBER);
		try {
			request = data.getNpcOrderDetailByMsisdn(number, type);
			if (request == null) {
				return true;
			}
			String operator = network.substring(3, 4);
			if (!request.getDonorid().equals(operator)) {
				return true;
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
		}

		return value;

	}

	// Metodo para validar si el numero ha sido portado
	private boolean processRule(List<String> requestNumberList, List<Detail> responseItemList,
			Map<String, String> originalOrderId, Map<String, Integer> parameter, Map<String, Date> portacionDates) {
		boolean value = false;
		String methodName = "processRule";
		Iterator<String> items = requestNumberList.iterator();
		while (items.hasNext()) {
			String item = items.next();
			if (process(item, originalOrderId, parameter, portacionDates)) {
				responseItemList
						.add(detailResponse(item, AppConstants.PORTA_STATUS_REJECTED, AppConstants.PORTA_CODE_RULE,
								AppConstants.PORTA_MSJ_RULE, ": El numero no ha sido portado.", null));
				log.info("No cumple regla. El numero no ha sido portado.");
				logger.info(CATEGORY, null, clazz.getCanonicalName(), methodName,
						String.format(env.getProperty(AppConstants.PORTA_MSJ_RULE),
								"El numero no ha sido portado. Status: " + env.getProperty(AppConstants.PORTA_STATUS_REJECTED)),
						"El numero no ha sido portado. Status: " + env.getProperty(AppConstants.PORTA_STATUS_REJECTED), true,
						env.getProperty(AppConstants.PORTA_CODE_RULE), 0L);
				items.remove();
				value = true;
			}
		}
		return value;
	}

	private boolean process(String number, Map<String, String> originalOrderId, Map<String, Integer> parameter,
			Map<String, Date> portacionDates) {
		boolean value = false;
		String methodName = "process";
		List<String> status = new ArrayList<>();
		status.add(env.getProperty(AppConstants.PORTA_STATUS_PROCESS));
		List<String> type = new ArrayList<>();
		type.add(env.getProperty(AppConstants.PORTA_PREFIX_PORT_OUT));
		QueryCustomerDto request = new QueryCustomerDto();

		try {
			request = data.getNpcOrderDetailByMsisdn(number, type);
			if (request == null) {
				return true;
			}
			String idorder = request.getNporderid();
			parameter.put("recipientId", Integer.parseInt(request.getRecipientid()));
			originalOrderId.put("idOrder" + number, idorder != null ? request.getNporderid() : "");
			originalOrderId.put("senderId" + number, idorder != null ? request.getRecipientid() : "");
			portacionDates.put("dateBp" + number, request.getProcessdate());
			type.clear();
			type = configurationMapper.getPrefixADADOList();
			request = data.getNpcOrderDetailByMsisdn(number, type);
			if (request != null) {
				portacionDates.put("dateADADO" + number, request.getProcessdate());
				if (portacionDates.get("dateADADO" + number).after((portacionDates.get("dateBp" + number)))) {
					value = false;
				} else {
					value = true;
				}
			}
		} catch (Exception ex) {
			logger.error(CATEGORY, null, methodName, AppConstants.ERROR, ex.getMessage(), 0L, ex);
		}
		return value;
	}

	private Detail detailResponse(String number, String statusProperty, String codeProperty, String messageProperty,
			String messageText, Detail response) {
		String statusCode = env.getProperty(statusProperty);
		String code = env.getProperty(codeProperty);
		String message = env.getProperty(messageProperty);
		if (message != null && !message.isEmpty()) {
			if (message.contains("%s")) {
				message = String.format(message, messageText);
			}
		}
		if (response == null) {
			return new Detail(statusCode, code, message, number);
		} else {
			response.setCode(code);
			response.setStatus(statusCode);
			response.setCodeDescripcion(message);
			response.setMsisdn(number);
			return response;
		}
	}

	public Operator getOwnerOperator(String number) {
		List<Operator> operators = operatorRepository.findAll();
		for (Operator owner : operators) {
			if (executeValidateRegex(number, owner.getRegex())) {
				return owner;
			}
		}
		return null;
	}

	private boolean inTime() {
		boolean value = false;
		String methodName = "inTime";
		try {
			int s = 7;
			Date f = null;
			Date f2 = null;
			Set<String> schedules = configurationService.getValues(AppConstants.DEBITCUTIN_ONLINE_TIME);
			for (String schedule : schedules) {
				CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(schedule);
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.MILLISECOND, 0);
				f = calendar.getTime();
				f2 = f;
				calendar.add(Calendar.SECOND, s * -1);
				f = calendar.getTime();
				int n = s + 1;
				for (int i = 0; i < n; i++) {
					f = cronSequenceGenerator.next(f);
					value = f.equals(f2);
					if (value) {
						logger.info(CATEGORY, null, clazz.getCanonicalName(), methodName,
								"Corte por deuda online" + schedule, "", true, "0", 0L);
						return value;
					}
				}
			}
		} catch (Exception ex) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, ex.getMessage(), 0L, ex);
		}
		return value;
	}

	public boolean ownMe(String operatorRN) {
		boolean value = false;
		List<Operator> operators = operatorRepository.findAll();
		for (Operator operator : operators) {
			value = (("TIGO".equals(operator.getName())) && operatorRN.equals(operator.getRoutingNumber()));
			if (value)
				break;
		}
		return value;
	}

	public boolean executeValidateRegex(String data, String regex) {
		boolean result = false;
		String methodName = "executeValidateRegex";
		try {
			Pattern queryLangPattern = Pattern.compile(regex);
			Matcher matcher = queryLangPattern.matcher(data);
			result = matcher.matches();
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getCanonicalName(), methodName, AppConstants.ERROR, e.getMessage(), 0L, e);
			result = false;
		}
		return result;
	}
}
