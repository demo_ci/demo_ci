package com.tigo.ea.porta.debitcut.service.rule;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.porta.debitcut.service.core.CatalogueBusinessService;
import com.tigo.ea.porta.debitcut.service.dto.BodyRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutResponse;
import com.tigo.ea.porta.debitcut.service.dto.HeaderResquest;
import com.tigo.ea.porta.debitcut.service.util.AppConstants;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.dto.SateliteDto;

@Component
public class ValidateDebitCut{
	
	private final Class<?> clazz = this.getClass();

	private static final String CATEGORY = "service";
	
	@Autowired
	private Environment env;
	
	@Autowired
	private LoggerUtil logger;

	@Autowired
	private CatalogueBusinessService catalogueBusinessService;
	


	// Validamos el request especifico de cada servicio
	public DebitCutResponse execute(DebitCutRequest request) {
		DebitCutResponse response;
		try {
			if (request != null) {
				response = this.validateHeader(request);
				if (response != null) {
					return response;
				}
				// validamos los parametros comunes en los servicios
				response = validateBasicFields(request);
				// validamos si no falta ningun parametro comun
				if (response != null) {
					return response;
				}
				//validamos si el satelite existe 
				response = validateCredentials(request);
				if(response != null) {
					return response;
				}
			} else {
				return new DebitCutResponse("95", "Request body not found");
			}
		} catch (Exception e) {
			return new DebitCutResponse("94", "Execute body validation error");
		}
		return null;
	}

	// METODO PARA REALIZAR VALIDACIONES MEDIANTE EXPRESIONES REGULARES
	private boolean executeValidateRegex(String data, String regex) {
		boolean result = false;
		try {
			Pattern queryLangPattern = Pattern.compile(regex);
			Matcher matcher = queryLangPattern.matcher(data);
			result = matcher.matches();
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	private DebitCutResponse validateCredentials(DebitCutRequest request) {
		String codeService = env.getProperty(AppConstants.PORTA_CODE_DB);
		String msjCredentialNotFound = env.getProperty(AppConstants.PORTA_MSJ_DB);
		Boolean autenticated = false;
		SateliteDto satelite = null;
		for (Map.Entry<Long, SateliteDto> currentSatelite: catalogueBusinessService.getCatalogueSatelite().entrySet()) {
			satelite = currentSatelite.getValue();
		    if(request.getBody().getChannel().equals(satelite.getRequestChannel()) && request.getBody().getUser().equals(satelite.getRequestUser())){
		    	autenticated = true;
		    }
		}
		if(!autenticated) {
			return new DebitCutResponse(codeService, String.format(msjCredentialNotFound, "User o Channel no existe"));
		}
		return null;
	}
	
	private DebitCutResponse validateHeader(DebitCutRequest request) {
		String codeRequired = env.getProperty(AppConstants.PORTA_CODE_MANDATORY);
		String codeFormat = env.getProperty(AppConstants.PORTA_CODE_FORMAT);
		String msjRequired = env.getProperty(AppConstants.PORTA_MSJ_MANDATORY);
		String msjFormat = env.getProperty(AppConstants.PORTA_MSJ_FORMAT);
		String codeService = env.getProperty(AppConstants.PORTA_CODE_SERVICE);
		String msjService = env.getProperty(AppConstants.PORTA_MSJ_SERVICE);
		String methodName= "validateHeader";
		try {
			HeaderResquest header = request.getHeader();
			if (header.getConsumerID() != null) {
				boolean isOk = executeValidateRegex(header.getConsumerID(),
						env.getProperty(AppConstants.PORTA_REGEX_HEADER_CONSUMERID));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "ConsumerID"));
				}
			}
			if (header.getCorrelationID() != null) {
				boolean isOk = executeValidateRegex(header.getCorrelationID(),
						env.getProperty(AppConstants.PORTA_REGEX_HEADER_CORRELATIONID));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "CorrelationID"));
				}
			}
			if (header.getCountry() != null) {
				boolean isOk = executeValidateRegex(header.getCountry().toString(),
						env.getProperty(AppConstants.PORTA_REGEX_HEADER_COUNTRY));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "Country"));
				}
			}
			if (header.getTransaccionID() == null) {
				return new DebitCutResponse(codeRequired, String.format(msjRequired, "TransaccionID"));
			} else {
				boolean isOk = executeValidateRegex(header.getTransaccionID(),
						env.getProperty(AppConstants.PORTA_REGEX_HEADER_TRANSACTIONID));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "TransaccionID"));
				}
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getName(), methodName, AppConstants.ERROR, String.format(msjService, "validateHeader"), 0L);
			return new DebitCutResponse(codeService, String.format(msjService, "validateHeader"));
		}
		return null;
	}

	private DebitCutResponse validateBasicFields(DebitCutRequest request) {
		String methodName = "validateBasicFields";
		try {
			String codeRequired = env.getProperty(AppConstants.PORTA_CODE_MANDATORY);
			String codeFormat = env.getProperty(AppConstants.PORTA_CODE_FORMAT);
			String msjRequired = env.getProperty(AppConstants.PORTA_MSJ_MANDATORY);
			String msjFormat = env.getProperty(AppConstants.PORTA_MSJ_FORMAT);
			
			BodyRequest body = request.getBody();
			// validamos el usuario
			if (body.getUser() == null || body.getUser().isEmpty()) {
				return new DebitCutResponse(codeRequired, String.format(msjRequired, "user"));
			} else {
				boolean isOk = executeValidateRegex(body.getUser(), env.getProperty(AppConstants.PORTA_REGEX_BODY_USER));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "user"));
				}
			}

//			 validamos el canal
			 if (body.getChannel() == null || body.getChannel().isEmpty()) {
				 return new DebitCutResponse(codeRequired,
						 String.format(msjRequired, "channel"));
			 } else{
				boolean isOk = executeValidateRegex(body.getChannel(), env.getProperty(AppConstants.PORTA_REGEX_BODY_CHANNEL));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "channel"));
				}
			}

			// validamos la fecha
			if (body.getDateTime() == null || body.getDateTime().isEmpty()) {
				return new DebitCutResponse(codeRequired, String.format(msjRequired, "datetime"));
			} else {
				boolean isOk = executeValidateRegex(body.getDateTime(), env.getProperty(AppConstants.PORTA_REGEX_BODY_DATE));
				if (!isOk) {
					return new DebitCutResponse(codeFormat, String.format(msjFormat, "datetime"));
				} else {
					try {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						sdf.setLenient(false);
						sdf.parse(body.getDateTime());
					} catch (Exception e) {
						logger.error(CATEGORY, clazz.getName(), methodName, AppConstants.ERROR, String.format(msjFormat, "datetime"),
								0L);
						return new DebitCutResponse(codeFormat, String.format(msjFormat, "datetime"));
					}
				}
			}
			// validamos el number de telefono
			int detailcount = body.getMsisdnList().getMsisdn().size();
			if (detailcount > 0) {
				for (int i = 0; i < detailcount; i++) {
					// validamos el number de telefono
					if (body.getMsisdnList().getMsisdn().get(i) == null
							|| body.getMsisdnList().getMsisdn().get(i).isEmpty()) {
						return new DebitCutResponse(codeRequired, String.format(msjRequired, AppConstants.MSISDN_MSJ));
					} else {
						boolean isOk =true;
						isOk = executeValidateRegex(body.getMsisdnList().getMsisdn().get(i) + "",
								env.getProperty(AppConstants.PORTA_REGEX_BODY_MSISDN));
						if (!isOk) {
							// result.append(" number =
							
							return new DebitCutResponse(codeFormat, String.format(msjFormat, AppConstants.MSISDN_MSJ));
						}else {
							TransactionIdUtil.setMsisdn(body.getMsisdnList().getMsisdn().get(i) != null ? body.getMsisdnList().getMsisdn().get(i):"");
						}
					}
				}
			} else {
				logger.error(CATEGORY, clazz.getName(), methodName, AppConstants.ERROR,  String.format(msjRequired, AppConstants.MSISDN_MSJ), 0L);
				return new DebitCutResponse(codeRequired, String.format(msjRequired, AppConstants.MSISDN_MSJ));
			}
		} catch (Exception e) {
			logger.error(CATEGORY, clazz.getName(), methodName, AppConstants.ERROR, e.getMessage(), 
					0L, e);
			return new DebitCutResponse("96", "Execute basic validation error");
		}
		return null;
	}
}