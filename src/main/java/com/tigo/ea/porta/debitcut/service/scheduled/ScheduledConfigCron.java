package com.tigo.ea.porta.debitcut.service.scheduled;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import com.tigo.ea.porta.debitcut.service.dto.ExecuteAgendedRequest;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;

/**
 * The Class ScheduledConfigCron.
 *
 * @author HTC-AlexCst
 */
@Service
public class ScheduledConfigCron implements ScheduledConfigCronService {

	private static final Logger log = LoggerFactory.getLogger(ScheduledConfigCron.class);
	private final Class<?> clazz = this.getClass();
	private final String CATEGORY = "service";
	private final String CRON_NAME = "CronDebitCut";
	private static final String DEBTCUT_CRON_ACTIVE = "debtcut.cron.active";

//	private String CRON_EXPRESION = "0 5 * ? * * *";
//	 private String CRON_EXPRESION = "0 49 * ? * *";
	// 0 0 0 ? JAN *
	// private String CRON_EXPRESION = "0 0 6-10 ? * MON,WED,TUE";
	 private String CRON_EXPRESION = "porta.cron.debitcut";

	@Autowired
	private Environment env;

	@Autowired
	private LoggerUtil logger;
	
	@Autowired
	@Qualifier("cronSchedulerConfig")
	private TaskScheduler taskScheduler;

	@Autowired
	private ExecuteAgendedRequest executeAgendedRequest;

	// Mapa que contiene los cron activos en el boot
	public static Map<String, ScheduledFuture<Object>> cronThreadList = new ConcurrentHashMap<>();

	// Mapa que contiene como llave nombre del cron y la expresion con la cual fue
	// creado
	public static Map<String, String> sateliteCron = new ConcurrentHashMap<>();

	/*
	 * No se usan try catch porque si algo fallo no tiene sentido que el boot se
	 * ejecute correctamente (non-Javadoc)
	 * 
	 * @see
	 * com.tigo.ea.porta.bccs.core.service.scheduled.ScheduledConfigCronService#
	 * initConfigureCronTreads()
	 */
	@SuppressWarnings("unchecked")
	public void initConfigureCronTreads() {
		String methodName = "initConfigureCronTreads";
		Boolean isActive = env.getProperty(DEBTCUT_CRON_ACTIVE, Boolean.class);
		// Creamos el schedulerCron para el satelite
		ScheduledFuture<Object> schedulerCron = (ScheduledFuture<Object>) taskScheduler.schedule(new Runnable() {
			@Override
			public void run() {
				// proceso que ejecutara el cron cuando se active
				if (isActive) {
					executeAgendedRequest.excecuteAgendedTransactions();
				} else {
					log.info("Procesar solicitudes agendadas desactivado.");
				}
			}
		}, new CronTrigger(env.getProperty(CRON_EXPRESION)));
		
		if (schedulerCron != null) {
			// Guardamos nombre del cron con la expresion que fue creado
			sateliteCron.put(CRON_NAME, env.getProperty(CRON_EXPRESION));
			// Guardamos schedulerCron creado por si necesita ser refrescado o eliminado
			cronThreadList.put(CRON_NAME, schedulerCron);

			logger.info(CATEGORY, null, clazz.getName(), methodName, CRON_NAME,
					"Cron creado con expresion: " + env.getProperty(CRON_EXPRESION), true, "0", 0L);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tigo.ea.porta.bccs.core.service.scheduled.ScheduledConfigCronService#
	 * reloadConfigureCronTreads()
	 */
	public void reloadConfigureCronTreads() {
		try {
			// obtenemos la expresion CRON con la cual se creo el ThreadCron
			String cronExpresion = sateliteCron.get(CRON_NAME);
			// evaluamos la expresion cron ejecutada version la actualizacion recibida
			if (!cronExpresion.equals(CRON_EXPRESION)) {
				// si las expresiones son distintas eliminamos el cron y creamos uno nuevo

				// Obtenemos el Cron activo
				ScheduledFuture<Object> cronConfig = cronThreadList.get(CRON_NAME);

				// cancelamos cron actual
				cronConfig.cancel(true);

				// invocamos creacion de cron con la nueva expresion
				initConfigureCronTreads();
			}
		} catch (Exception e) {
			// Si algo falla durante la carga de la cache detenemos la ejecucion de la
			// Virtual Machine Java para el boot
			Runtime.getRuntime().exit(0);
		}
	}
}