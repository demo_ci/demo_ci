package com.tigo.ea.porta.debitcut.service.scheduled;

/**
 * The Interface ScheduledConfigCronService.
 *
 * @author HTC-Daniel
 */
public interface ScheduledConfigCronService {
	
	/**
	 * Inits the configure cron treads.
	 * No se usan try catch porque si algo fallo no tiene sentido que el boot se ejecute correctamente
	 * ya que trabaja en base a configuraciones desde REDIS
	 */
	public void initConfigureCronTreads();
	
	/**
	 * Reload configure cron treads.
	 */
	public void reloadConfigureCronTreads();
}
