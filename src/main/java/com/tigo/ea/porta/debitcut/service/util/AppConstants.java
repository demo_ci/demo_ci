package com.tigo.ea.porta.debitcut.service.util;

public class AppConstants {

	// LOGGER LEVEL
	public static final String CATEGORY_TARGET = "target";
	public static final String CATEGORY_HANDLER_SOAP = "handler-soap";

	public static final String ESTADO_SOLICITUD = "Estado de la solicitud: %s";
	public static final String ESTADO_INICIAL = "Estado de la solicitud: %s. Estado inicial.";
	
	public static final String ERROR = "ERROR";
	public static final String RESPONSE = "Response";
	public static final String FAIL_MSJ = "Falla en consulta.";
	public static final String MSISDN_MSJ = "msisdn";
	
	//dateADADO
	public static final String DATE_ADADO = "dateADADO";
	
	//Queue
	public static final  String SPINTERFACE_QUEUE = "portationprocess-request";
		
	//dateBp
	public static final String DATE_BP = "dateBp";
	
	//originalNpOrderId
	public static final String ORIGINAL_NP_ORDERID = "originalNpOrderId";
	//portRequest
	public static final String PORT_REQUEST = "portRequest";
	public static final String PORTA_TIGO_ROUTING_NUMBER = "porta.tigo.routing.number";

	public static final String AUTHENTICATION_CIPHER_SECRET_KEY = "authentication.cipher.secret.key";

	public static final String AUTHENTICATION_NP_SP_USERNAME_KEY = "authentication.np-sp.username.key";
	public static final String AUTHENTICATION_NP_SP_USERNAME_VALUE = "authentication.np-sp.username.value";
	public static final String AUTHENTICATION_NP_SP_PASS_KEY = "authentication.np-sp.password.key";
	public static final String AUTHENTICATION_NP_SP_PASS_VALUE = "authentication.np-sp.password.value";
	public static final String AUTHENTICATION_SP_NP_USERNAME_VALUE = "authentication.sp-np.username.value";
	public static final String AUTHENTICATION_SP_NP_PASS_VALUE = "authentication.sp-np.password.value";
	public static final String AUTHENTICATION_SP_NP_AUTHORIZATION = "authentication.sp-np.Authorization";

	// PLATFORM BCCS
	public static final String SERVICE_BCCS_PORTABILITY_WSDL = "porta.service.bccs.portability.wsdl";
	public static final String SERVICE_BCCS_PORTABILITY_TRGT_NAMESPACE = "porta.service.bccs.portability.targetnamespace";
	public static final String SERVICE_BCCS_PORTABILITY_SERVICENAME = "porta.service.bccs.portability.servicename";
	public static final String SERVICE_BCCS_PORTABILITY_PORTNAME = "porta.service.bccs.portability.portname";
	public static final String SERVICE_BCCS_PORTABILITY_USER = "porta.service.bccs.portability.user";
	public static final String SERVICE_BCCS_PORTABILITY_PASS = "porta.service.bccs.portability.password";
	public static final String SERVICE_BCCS_WSDL = "wsdl.bccs";

	public static final String SOAP_CONFIG_ADD_SECURITY = "soapHandler.addSecurity";
	public static final String SOAP_CONFIG_PRINT_REQUEST = "soapHandler.printSoapMessage.request";
	public static final String SOAP_CONFIG_PRINT_RESPONSE = "soapHandler.printSoapMessage.response";
	public static final String SOAP_CONFIG_PRINT_FAULT = "soapHandler.printSoapMessage.fault";

	// porta constants
	public static final String DEBITCUTIN_ONLINE_TIME = "debitcutin.online.time";
	public static final String PLATFORM_BCCS = "BCCS";
	public static final String PLATFORM_BCCS_EXECUTE_TRANSACTION = "executeTransaction";
	public static final String PORTA_ADDITIONAL_PARAMETER_PORTREQUEST_ID = "porta.additional.parameter.portrequest.id";

	//porta codes
	public static final String PORTA_CODE_OBSERVED = "porta.code.observed";
	public static final String PORTA_CODE_RULE = "porta.code.rule";
	public static final String PORTA_CODE_SERVICE = "porta.code.service";
	public static final String PORTA_CODE_SUCCESSFUL = "porta.code.successful";
	public static final String PORTA_CODE_DB = "porta.code.db";
	public static final String PORTA_CODE_ERROR = "porta.code.error";
	public static final String PORTA_CODE_MANDATORY = "porta.code.mandatory";
	public static final String PORTA_CODE_FORMAT = "porta.code.format";
	
	
	public static final String PORTA_CREDENTIALS_NOT_FOUND = "porta.credentials.not.found";
	
	// porta messages
	public static final String PORTA_MSJ_OBSERVED = "porta.msj.observed";
	public static final String PORTA_MSJ_RULE = "porta.msj.rule";
	public static final String PORTA_MSJ_SERVICE = "porta.msj.service";
	public static final String PORTA_MSJ_DB = "porta.msj.db";
	public static final String PORTA_MSJ_ERROR = "porta.msj.error";
	public static final String PORTA_MSJ_SUCCESSFUL = "porta.msj.successful";
	public static final String PORTA_MSJ_MANDATORY = "porta.msj.mandatory";
	public static final String PORTA_MSJ_FORMAT = "porta.msj.format";

	public static final String PORTA_PREFIX_DEVCUT_IN = "porta.prefix.devcut.in";
	public static final String PORTA_PREFIX_DEVCUT_IN_BCCS_CODE = "porta.prefix.devcut.in.bccs.code";
	public static final String PORTA_PREFIX_DEVCUT_OUT = "porta.prefix.devcut.out";
	public static final String PORTA_PREFIX_PORT_IN = "porta.prefix.port.in";
	public static final String PORTA_PREFIX_REVERSION_IN = "porta.prefix.reversion.in";
	public static final String PORTA_PREFIX_RETURNNUMBER_IN = "porta.prefix.returnnumber.in";
	public static final String PORTA_PREFIX_PORT_OUT = "porta.prefix.port.out";
	public static final String PORTA_PREFIX_DEVOLUTION_CR = "porta.prefix.devolution.in.cr";
	public static final String PORTA_PREFIX_DEVOLUTION_IN = "porta.prefix.devolution.in";



	// public static final String PORTA_STATUS_ERROR =
	// "porta.status.error";porta.tigo.routing.number

	public static final String PORTA_STATUS_ACCEPTED = "porta.status.accepted";
	public static final String PORTA_STATUS_INVALID = "porta.status.invalid";
	public static final String PORTA_STATUS_OBSERVED = "porta.status.observed";
	public static final String PORTA_STATUS_PENDING = "porta.status.pending";
	public static final String PORTA_STATUS_PROCESS = "porta.status.process";
	public static final String PORTA_STATUS_REJECTED = "porta.status.rejected";
	public static final String PORTA_STATUS_SUCCESSFUL = "porta.status.successful";
	public static final String PORTA_STATUS_TIMEOUT = "porta.status.timeout";

	public static final String PORTA_STATUS_VALID = "porta.status.valid";
	public static final String PORTA_STATUS_CORTADO = "porta.status.cortado";
	public static final String PORTA_STATUS_AGENDADO = "porta.status.agendado";
	public static final String PORTA_STATUS_DEVUELTO = "porta.status.devuelto";
	public static final String PORTA_STATUS_ERROR = "porta.status.error";
	
	public static final String PORTA_REGEX_RANGE_ENTEL = "porta.regex.range.entel";
	public static final String PORTA_REGEX_RANGE_TIGO = "porta.regex.range.tigo";
	public static final String PORTA_REGEX_RANGE_VIVA = "porta.regex.range.viva";
	public static final String REGEX_DONOR_P = "porta.portinout.regex.donor";
	public static final String REGEX_ADITIONAL_PARAMETER_NAME = "porta.regex.body.additional.parameter.name";
	public static final String REGEX_ADITIONAL_PARAMETER_VALUE = "porta.regex.body.additional.parameter.value";
	public static final String PORTA_REGEX_BODY_CHANNEL = "porta.regex.body.channel";
	public static final String PORTA_REGEX_BODY_DATE = "porta.regex.body.date";
	public static final String PORTA_REGEX_BODY_MSISDN = "porta.regex.body.msisdn";
	public static final String PORTA_REGEX_BODY_USER = "porta.regex.body.user";
	public static final String REGEX_OPERATOR = "porta.regex.body.operator";
	public static final String PORTA_REGEX_HEADER_CONSUMERID = "porta.regex.header.consumerid";
	public static final String PORTA_REGEX_HEADER_CORRELATIONID = "porta.regex.header.correlationid";
	public static final String PORTA_REGEX_HEADER_COUNTRY = "porta.regex.header.country";
	public static final String PORTA_REGEX_HEADER_TRANSACTIONID = "porta.regex.header.transactionid";
	
	public static final String PORTA_GATEWAY_CHANNEL_WEB= "porta.gateway.channel.web";
	public static final String PORTA_GATEWAY_USER_WEB = "porta.gateway.user.web";

}
