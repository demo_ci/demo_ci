package com.tigo.ea.porta.debitcut.service.util;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.htc.ea.connections.beans.ConnectionData;
import com.htc.ea.connections.factory.ConnectionRabbitManager;
import com.htc.ea.util.category.EventCategoryService;
import com.htc.ea.util.convert.LookupConverter;
import com.tigo.ea.porta.debitcut.service.core.CatalogueBusinessService;
import com.tigo.ea.porta.model.commons.Constants;
import com.tigo.ea.porta.model.dto.SateliteDto;

@Component("appUtilLocal")
public class AppUtil {

	@Autowired
	private LookupConverter lookupConverter;

	@Autowired
	private CatalogueBusinessService catalogueBusinessService;

	@Autowired
	private Environment env;

	@Autowired
	private LoggerUtil loggerUtil;

	protected Logger loggerFile = LoggerFactory.getLogger(AppUtil.class);
	private static final Logger log = LoggerFactory.getLogger(AppUtil.class);

	public static final String UNKNOWNHOST = "unknownHost";

	@Autowired
	private EventCategoryService eventCategoryService;

	/****
	 * 
	 * @param category
	 *            categoria del log
	 * @param data
	 *            datos que se requiere guardar
	 * @param clazz
	 *            clase que ejecuta el metodo de log
	 * @param serviceName
	 *            servicio/metodo que ejecuta
	 * @param msg
	 *            mensaje general del log
	 * @param detail
	 *            detalles del log
	 * @param msisdn
	 *            identificador relevante del la transaccion usualmente el numero de
	 *            cuenta puede ser otro
	 * @param duration
	 *            tiempo de proceso
	 */



	/*
	 * @param category categoria del log
	 * 
	 * @param data datos que se requiere guardar
	 * 
	 * @param clazz clase que ejecuta el metodo de log
	 * 
	 * @param serviceName servicio/metodo que ejecuta
	 * 
	 * @param msg mensaje general del log
	 * 
	 * @param detail detalles del log
	 * 
	 * @param msisdn identificador relevante del la transaccion usualmente el numero
	 * de cuenta puede ser otro
	 * 
	 * @param duration tiempo de proceso
	 */


	public boolean logging(String eventCategory, String eventLevel) {
		Boolean loggingEnabled = Boolean.valueOf(env.getProperty(com.htc.ea.util.util.Constants.LOGGING_ENABLED));
		Boolean eventLevelEnabled = eventCategoryService.verifyEventCategory(eventCategory, eventLevel);
		return loggingEnabled && eventLevelEnabled;
	}

	// spn
	public String getHostAddressFromHostName(String url) {
		InetAddress address;
		try {
			address = InetAddress.getByName(new URL(url).getHost());
			return address.getHostAddress();
		} catch (Exception e) {
			return UNKNOWNHOST;
		}
	}

	public static String getConsumerLocation() {
		String endUserLocation = UNKNOWNHOST;
		HttpServletRequest context = HttpServletContextUtil.getHttpServletRequestContext();
		if (context != null) {
			String ip = context.getHeader("Remote_Addr");
			if (ip != null) {
				endUserLocation = ip;
			}
		}
		// Si no hay balanceador
		if (endUserLocation.equals(UNKNOWNHOST)) {
			endUserLocation = getServerLocation();
		}

		return endUserLocation;
	}

	public static String getServerLocation() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return UNKNOWNHOST;
	}

	public String getParamFromHeaders(HttpHeaders httpHeaders, String nameParam) {
		String param = "";
		Map<String, String> tempMap = httpHeaders.toSingleValueMap();
		param = tempMap.get(nameParam);

		return param;
	}

	public String getQuerySQLFormated2(String querySql, Object[] params, Set<Integer> indexNotIncluded) {
		String querySqlFormated = querySql;
		if (indexNotIncluded == null)
			indexNotIncluded = new HashSet<>();
		for (int i = 0; i < params.length; i++) {
			if (indexNotIncluded.contains(i))
				continue;
			else {
				querySqlFormated = querySqlFormated.replaceFirst("\\?", "%s");
			}
		}
		return querySqlFormated;
	}

	public boolean logging() {
		return Boolean.parseBoolean(env.getProperty("logging.enabled"));
	}

	public static String getMethodName() {
		return Thread.currentThread().getStackTrace()[2].getMethodName();
	}

	public String getHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return "UnknownHost";
		}
	}

	public static void delay(long timeout) {
		long elapsedTime = 0;
		long startTime = System.currentTimeMillis();
		if (log.isInfoEnabled())
			log.info(String.format("Init delay (ms): 0 , timeout configured in:  %s", timeout));
		while (elapsedTime < timeout) {
			elapsedTime = System.currentTimeMillis() - startTime;
		}
		if (log.isInfoEnabled())
			log.info(String.format("End delay (ms): %s", elapsedTime));
	}

	public boolean isIntegerNumber(String data) {
		try {
			String regex = "^-*[0-9]+$";
			java.util.regex.Pattern queryLangPattern = java.util.regex.Pattern.compile(regex);
			java.util.regex.Matcher matcher = queryLangPattern.matcher(data);
			return matcher.matches();
		} catch (Exception e) {
			return false;
		}
	}

	public String translateSpanish(String code, String message, String source, String methodName) {
		String app = com.tigo.ea.porta.model.commons.Constants.APP_TARGET;
		String topic = com.tigo.ea.porta.model.commons.Constants.TOPICID_REASONSPANISHDESC;
		String[] values = new String[] { message, code };
		List<String> valuesList = new LinkedList<>(Arrays.asList(values));
		String formatTranslateMessage = null;
		String translateMessage = null;
		int count = 0;
		for (String key : valuesList) {
			++count;
			formatTranslateMessage = lookupConverter.convertFrom(app, topic, key);
			if (formatTranslateMessage == null) {
				if (count == 2) {
					String msg = "Conversion " + topic;
					String detail = String.format(
							"No fue posible obtener la conversion (traducci�n en espa�ol) del mensaje: %s|%s, con t�pico: %s, appTarget: %s y key: %s",
							code, message, topic, app, key);
					loggerUtil.error("service", source, methodName, msg, detail, null);
				}
			} else {
				if (count == 2) {
					String regex = env.getProperty(String.format("%s%s", Constants.REGEX_REJECT_REASON_CODE, key));
					if (regex != null) {
						Pattern pattern = Pattern.compile(regex);
						Matcher matcher = pattern.matcher(message);
						if (matcher.find()) {
							translateMessage = message.replaceAll(regex, formatTranslateMessage);
						} else {
							// loggear
							String msg = "Conversion " + topic + "";
							String detail = String.format(
									"La regex %s no coincide con el patr�n de mensaje comparado: %s", regex,
									formatTranslateMessage);
							loggerUtil.error("service", source, methodName, msg, detail, null);
						}
					} else {
						translateMessage = formatTranslateMessage;
					}
				} else {
					translateMessage = formatTranslateMessage;
				}
				break;
			}
		}
		return translateMessage;
	}

	public boolean validateQueueProducer(ConnectionRabbitManager connectionRabbitManager,
			ConnectionData connectionDataSelected, String messagingType) {
		boolean result = true;
		int validateFailed = 0;
		if (connectionDataSelected == null)
			validateFailed++;
		if (connectionDataSelected != null && !connectionDataSelected.getType().equals("producer"))
			validateFailed++;
		if (connectionRabbitManager.getConnectionMessagingManager().get(messagingType) == null)
			validateFailed++;
		if (validateFailed > 0)
			result = false;
		return result;
	}

	public ConnectionData getMessagingDataSelected(ConnectionRabbitManager connectionRabbitManager,
			String messagingType) {
		ConnectionData messagingDataSelected = connectionRabbitManager.getConnectionMessagingDataSet().stream()
				.filter(q -> q.getItemType().equals(messagingType)).findAny().orElse(null);
		return messagingDataSelected;
	}

	public SateliteDto getSateliteById(Long id) {
		return catalogueBusinessService.getSatelite(id);
	}

}
