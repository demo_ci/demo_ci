package com.tigo.ea.porta.debitcut.service.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.htc.ea.util.util.TransactionIdUtil;


public class HttpServletInterceptor extends GenericFilterBean   {

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		TransactionIdUtil.begin();
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletContextUtil.setHttpServletRequestContext(req);
        arg2.doFilter(arg0, arg1);
        TransactionIdUtil.end();
	}
}
