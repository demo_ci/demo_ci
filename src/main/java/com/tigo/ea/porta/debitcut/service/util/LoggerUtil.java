package com.tigo.ea.porta.debitcut.service.util;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.htc.ea.util.category.EventCategoryService;
import com.htc.ea.util.log.EventData;
import com.htc.ea.util.log.EventLevel;
import com.htc.ea.util.log.LoggingServiceFacade;
import com.htc.ea.util.util.TransactionIdUtil;

/**
 * The Class LoggerUtil.
 */
@Component("loggerUtilLocal")
public class LoggerUtil {
	
	@Autowired
	private Environment env;
	@Autowired
	private LoggingServiceFacade loggingServiceFacade;
	@Autowired
	private EventCategoryService eventCategoryService;
	@Autowired
	@Qualifier("appUtilLocal")
	private AppUtil appUtil;
	
	

	/**
	 * Info.
	 *
	 * @param category the category
	 * @param data the data
	 * @param source the source
	 * @param serviceName the service name
	 * @param msg the msg
	 * @param detail the detail
	 * @param successful the successful
	 * @param responseCode the response code
	 * @param duration the duration
	 */
	public void info(String category, Object data, String source, String serviceName, String msg, String detail, Boolean successful, String responseCode, Long duration){
		if (logging(category, EventLevel.INFO.name())) {
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			String msisdn = TransactionIdUtil.getMsisdn() != null ?  TransactionIdUtil.getMsisdn() : "";
			String endUserLocation = getConsumerLocation();
			String serverLocation = appUtil.getHostAddress();
			String referenceId = TransactionIdUtil.getTarget() != null ? TransactionIdUtil.getTarget() : "";
	    	loggingServiceFacade.log(EventData.builder()
	    			.sequence(sequence)
	    			.category(category)
	    			.data(data)
	    			.dataType((data!=null ? "xml" : null))
					.level(EventLevel.INFO.name())
					.endUser(msisdn)
					.source(source)
					.name(serviceName)
					.message(msg)
					.detail(detail)
					.referenceId(referenceId)
					.originReferenceId(refId)
					.referenceId(refId)
					.successful(successful)
					.responseCode(responseCode)
					.duration(duration)
					.endUserLocation(endUserLocation)
					.serverLocation(serverLocation)
					.automatic(false)
					.build());
		}
    }


	/**
	 * Debug.
	 *
	 * @param category the category
	 * @param data the data
	 * @param source the source
	 * @param serviceName the service name
	 * @param msg the msg
	 * @param detail the detail
	 * @param successful the successful
	 * @param responseCode the response code
	 * @param duration the duration
	 */
	public void debug(String category, Object data, String source, String serviceName, String msg, String detail, Boolean successful, String responseCode, Long duration){
		if (logging(category, EventLevel.DEBUG.name())){
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			String msisdn = TransactionIdUtil.getMsisdn() != null ?  TransactionIdUtil.getMsisdn() : "";
			String endUserLocation = getConsumerLocation();
			String referenceId = TransactionIdUtil.getTarget() != null ? TransactionIdUtil.getTarget() : "";
			String serverLocation = appUtil.getHostAddress();
			loggingServiceFacade.log(EventData.builder()
					.sequence(sequence)
					.category(category)
					.data(data)
					.dataType((data!=null ? "xml" : null))
					.level(EventLevel.DEBUG.name())
					.endUser(msisdn)
					.source(source)
					.name(serviceName)
					.message(msg)
					.detail(detail)
					.referenceId(referenceId)
					.originReferenceId(refId)
					.referenceId(refId)
					.successful(successful)
					.responseCode(responseCode)
					.duration(duration)
					.endUserLocation(endUserLocation)
					.serverLocation(serverLocation)
					.automatic(false)
					.build());
		}
    }
	
	
	/**
	 * Service exception.
	 *
	 * @param category the category
	 * @param source the source
	 * @param serviceName the service name
	 * @param msg the msg
	 * @param detail the detail
	 * @param duration the duration
	 * @param exception the exception
	 * @param responseCode the response code
	 */
	public void serviceException(String category, String source, String serviceName, String msg, String detail, Long duration, Throwable exception, String responseCode){
		if (logging(category, EventLevel.ERROR.name())){
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			String msisdn = TransactionIdUtil.getMsisdn() != null ?  TransactionIdUtil.getMsisdn() : "";
			String referenceId = TransactionIdUtil.getTarget() != null ? TransactionIdUtil.getTarget() : "";
			String endUserLocation = getConsumerLocation();
			String serverLocation = appUtil.getHostAddress();
	    	loggingServiceFacade.log(EventData.builder()
					.sequence(sequence)
	    			.category(category)
					.level(EventLevel.ERROR.name())
					.endUser(msisdn)
					.source(source)
					.name(serviceName)
					.message(msg)
					.detail(detail)
					.referenceId(referenceId)
					.originReferenceId(refId)
					.referenceId(refId)
					.successful(false)
					.responseCode(responseCode)
					.duration(duration)
					.endUserLocation(endUserLocation)
					.serverLocation(serverLocation)
					.automatic(false)
					.exception(exception)
					.build());
		}
    }
	
	
	
	/**
	 * Error.
	 *
	 * @param category the category
	 * @param source the source
	 * @param serviceName the service name
	 * @param msg the msg
	 * @param detail the detail
	 * @param duration the duration
	 */
	public void error(String category, String source, String serviceName, String msg, String detail, Long duration){
		if (logging(category, EventLevel.ERROR.name())){
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			String msisdn = TransactionIdUtil.getMsisdn() != null ?  TransactionIdUtil.getMsisdn() : "";
			String referenceId = TransactionIdUtil.getTarget() != null ? TransactionIdUtil.getTarget() : "";
			String endUserLocation = getConsumerLocation();
			String serverLocation = appUtil.getHostAddress();
	    	loggingServiceFacade.log(EventData.builder()
					.sequence(sequence)
	    			.category(category)
					.level(EventLevel.ERROR.name())
					.endUser(msisdn)
					.source(source)
					.name(serviceName)
					.message(msg)
					.detail(detail)
					.referenceId(referenceId)
					.originReferenceId(refId)
					.referenceId(refId)
					.successful(false)
					.responseCode(null)
					.duration(duration)
					.endUserLocation(endUserLocation)
					.serverLocation(serverLocation)
					.automatic(false)
					.build());
		}
    }
	

	/**
	 * Error.
	 *
	 * @param category the category
	 * @param source the source
	 * @param serviceName the service name
	 * @param msg the msg
	 * @param detail the detail
	 * @param duration the duration
	 * @param exception the exception
	 */
	public void error(String category, String source, String serviceName, String msg, String detail, Long duration, Throwable exception){
		if (logging(category, EventLevel.ERROR.name())){
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			String msisdn = TransactionIdUtil.getMsisdn() != null ?  TransactionIdUtil.getMsisdn() : "";
			String referenceId = TransactionIdUtil.getTarget() != null ? TransactionIdUtil.getTarget() : "";
			String endUserLocation = getConsumerLocation();
			String serverLocation = appUtil.getHostAddress();
	    	loggingServiceFacade.log(EventData.builder()
					.sequence(sequence)
	    			.category(category)
					.level(EventLevel.ERROR.name())
					.endUser(msisdn)
					.source(source)
					.name(serviceName)
					.message(msg)
					.detail(detail)
					.referenceId(referenceId)
					.originReferenceId(refId)
					.referenceId(refId)
					.successful(false)
					.responseCode(null)
					.duration(duration)
					.endUserLocation(endUserLocation)
					.serverLocation(serverLocation)
					.automatic(false)
					.exception(exception)
					.build());
		}
    }
	
	/**
	 * Logging.
	 *
	 * @param eventCategory the event category
	 * @param eventLevel the event level
	 * @return true, if successful
	 */
	public boolean logging(String eventCategory, String eventLevel) {
		Boolean loggingEnabled = Boolean.valueOf(env.getProperty(com.htc.ea.util.util.Constants.LOGGING_ENABLED));
		Boolean eventLevelEnabled = eventCategoryService.verifyEventCategory(eventCategory, eventLevel);
		return loggingEnabled && eventLevelEnabled;		
	}	
		
	/**
	 * Gets the consumer location.
	 *
	 * @return the consumer location
	 */
	public static String getConsumerLocation(){
		String endUserLocation = "UnknownHost";
		HttpServletRequest context = HttpServletContextUtil.getHttpServletRequestContext();
		if (context != null) {
			String ip = context.getHeader("Remote_Addr");
			if(ip != null){
				endUserLocation = ip;
			}
		}
		//Si no hay balanceador
		if (endUserLocation.equals("UnknownHost")){
			endUserLocation = getServerLocation();
		}

		return endUserLocation;
	}


	
	/**
	 * Gets the server location.
	 *
	 * @return the server location
	 */
	public static String getServerLocation(){
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return "UnknownHost";
	}
		
	
	
	/**
	 * Gets the ip from url string.
	 *
	 * @param url the url
	 * @return the ip from url string
	 */
	public String getIpFromUrlString(String url){
		InetAddress address;
		try {
			address = InetAddress.getByName(new URL(url).getHost());
			return address.getHostAddress();
		} catch (Exception e) {
			error("consumer-queue", this.getClass().getName(), "getIpFromUrlString", "Error obteniendo HostAddress", "No se pudo obtener la IP del recurso solicitado: "+ url, null, e);	
		} 
		return "localhost";
    }
	
	
	/**
	 * Builds the get rest URL.
	 *
	 * @param url the url
	 * @param values the values
	 * @return the string
	 */
	public String buildGetRestURL(String url, Object... values){
		List<Object> params = new ArrayList<Object>();
		String buildUrl = url;
		if(values != null){
			for (Object aux : values) {
				if(aux != null){
					params.add(aux);
				}
			}
			if(!params.isEmpty()){
				UriComponents uriComponents =
						UriComponentsBuilder
							.fromUriString(url)
							.build()
							.expand(params.toArray())
							.encode();
				buildUrl = uriComponents.toUriString();
			}
		}
		return buildUrl;
	}

	
	
	/**
	 * Parses the string to number.
	 *
	 * @param number the number
	 * @return the integer
	 */
	public Integer parseStringToNumber(String number){
		Integer numb = 0;
		if(number != null && !number.isEmpty()){
			try {
				numb = Integer.parseInt(number);
			} catch (Exception e) {				
				error("consumer-queue", this.getClass().getName(), "parseStringToNumber", "Error parsing Str2Number", "No se pudo parsear el Transaction Reference ID: "+ number, null, e);
			}
		}
		return numb;
	}

	
	
	/**
	 * Parses the transaction result to integer.
	 *
	 * @param result the result
	 * @return the integer
	 */
	public Integer parseTransactionResultToInteger(String result){
		Integer numb = 1;
		if(result != null && !result.isEmpty()){
			try {
				if(result.equals("Ok")){
					return numb = 0;
				}
			} catch (Exception e) {
				error("consumer-queue", this.getClass().getName(), "parseTransactionResultToInteger", "Error obteniendo HostAddress", "\"No se pudo parsear el resultado de la transaccion: "+ result, null, e);
			}
		}
		return numb;
	}
}