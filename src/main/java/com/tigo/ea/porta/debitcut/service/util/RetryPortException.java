package com.tigo.ea.porta.debitcut.service.util;

public class RetryPortException extends Exception {
	private static final long serialVersionUID = 1L;

	private final String code;
	private final String description;
	private final Integer retry;

	public RetryPortException(String code, String description, Integer retry) {
		super();
		this.code = code;
		this.description = description;
		this.retry = retry;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public Integer getRetry() {
		return retry;
	}

}
