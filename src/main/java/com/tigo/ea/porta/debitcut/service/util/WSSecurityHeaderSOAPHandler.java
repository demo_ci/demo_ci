package com.tigo.ea.porta.debitcut.service.util;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import com.tigo.xmlns.responseheader.v3.ResponseHeader;

/**
 * The Class WSSecurityHeaderSOAPHandler.
 *
 * @author HTC-AlexCst
 */
@SuppressWarnings("unused")
public class WSSecurityHeaderSOAPHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Logger logger = LoggerFactory.getLogger(WSSecurityHeaderSOAPHandler.class);

	private Environment env;

	private LoggerUtil loggerUtil;

	private static final String CATEGORY = "handler-soap";
	private final Class<?> clazz = this.getClass();

	private static final String SOAP_ELEMENT_PASSWORD = "Password";
	private static final String SOAP_ELEMENT_USERNAME = "Username";
	private static final String SOAP_ELEMENT_USERNAME_TOKEN = "UsernameToken";
	private static final String SOAP_ELEMENT_SECURITY = "Security";
	private static final String NAMESPACE_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String PREFIX_SECURITY = "wsse";

	private String usernameText;
	private String passwordText;

	/**
	 * Native constructor.
	 */
	public WSSecurityHeaderSOAPHandler() {
		super();
	}

	/**
	 * Use this constructor if webServices required ws-security.
	 *
	 * @param usernameText
	 *            the username text
	 * @param passwordText
	 *            the password text
	 * @param env
	 *            the env
	 */
	public WSSecurityHeaderSOAPHandler(String usernameText, String passwordText, Environment env,
			LoggerUtil loggerUtil) {
		this.usernameText = usernameText;
		this.passwordText = passwordText;
		this.env = env;
		this.loggerUtil = loggerUtil;
	}

	/**
	 * Use this constructor if webServices NOT required ws-security.
	 *
	 * @param env
	 *            the env
	 */
	public WSSecurityHeaderSOAPHandler(Environment env) {
		this.env = env;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.
	 * MessageContext)
	 */
	@Override
	public boolean handleMessage(SOAPMessageContext soapMessageContext) {
		Boolean outboundProperty = (Boolean) soapMessageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		String methodName = "handleMessage";
		// VERIFICAMOS SI EL MENSAJE SOAP ES DE ENTRADA O SALIDA
		if (outboundProperty.booleanValue()) {
			try {
				// verificamos si incluye o no ws-security
				if (env.getProperty(AppConstants.SOAP_CONFIG_ADD_SECURITY, Boolean.class)) {
					SOAPEnvelope soapEnvelope = soapMessageContext.getMessage().getSOAPPart().getEnvelope();

					SOAPHeader header = soapEnvelope.getHeader();
					if (header == null) {
						header = soapEnvelope.addHeader();
					}

					SOAPElement soapElementSecurityHeader = header.addChildElement(SOAP_ELEMENT_SECURITY,
							PREFIX_SECURITY, NAMESPACE_SECURITY);

					SOAPElement soapElementUsernameToken = soapElementSecurityHeader
							.addChildElement(SOAP_ELEMENT_USERNAME_TOKEN, PREFIX_SECURITY);
					SOAPElement soapElementUsername = soapElementUsernameToken.addChildElement(SOAP_ELEMENT_USERNAME,
							PREFIX_SECURITY);
					soapElementUsername.addTextNode(this.usernameText);

					SOAPElement soapElementPassword = soapElementUsernameToken.addChildElement(SOAP_ELEMENT_PASSWORD,
							PREFIX_SECURITY);
					soapElementPassword.addTextNode(this.passwordText);
				}
			} catch (Exception e) {
				throw new RuntimeException("Error on SOAPHandlerClient: " + e.getMessage());
			}

			try {
				// evaluate for print soap Request
				if (env.getProperty(AppConstants.SOAP_CONFIG_PRINT_REQUEST, Boolean.class)) {
					String xmlResponse = printFormattedXML(soapMessageContext.getMessage());
					logger.info(xmlResponse);
					loggerUtil.info(CATEGORY, null, clazz.getCanonicalName(), methodName, "Request <SOAPHandlerASCP>",
							xmlResponse, true, "0", 0L);
				}
			} catch (Exception e) {
				loggerUtil.error(CATEGORY, clazz.getCanonicalName(), methodName, "ERROR", e.getMessage(), 0L, e);
			}

		} else {

			// Soap message is RESPONSE

			try {
				// evaluate for print soap Response
				if (env.getProperty(AppConstants.SOAP_CONFIG_PRINT_RESPONSE, Boolean.class)) {
					String xmlRequest = printFormattedXML(soapMessageContext.getMessage());
					logger.info(xmlRequest);
					loggerUtil.info(CATEGORY, null, clazz.getCanonicalName(), methodName,
							"Response <SOAPHandlerASCP>", xmlRequest, true, "0", 0L);
				}
			} catch (Exception e) {
				loggerUtil.error(CATEGORY, clazz.getCanonicalName(), methodName, "ERROR", e.getMessage(), 0L, e);
			}
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext)
	 */
	@Override
	public void close(MessageContext context) {
		// Do nothing because of X and Y.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext)
	 */
	@Override
	public boolean handleFault(SOAPMessageContext context) {
		// Soap message is FAULT
		boolean result = false;
		try {
			// evaluate for print soap Response
			if (env.getProperty(AppConstants.SOAP_CONFIG_PRINT_FAULT, Boolean.class)) {
				logger.info("Soap Fault Response----------------------------------------");
				logMessage(context.getMessage());
			}
			result = convertSOAPFaultToResponseHeader(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders()
	 */
	@Override
	public Set<QName> getHeaders() {
		HashSet<QName> headers = null;
		try {
			// si requiere seguridad agregamos los headers necesarios
			if (env.getProperty(AppConstants.SOAP_CONFIG_ADD_SECURITY, Boolean.class)) {
				QName securityHeader = new QName(
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
						"Security");
				headers = new HashSet<QName>();
				headers.add(securityHeader);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return headers;
	}

	/**
	 * Print Xml from soapMessage in/out/fault only need soapMessage for print in
	 * console.
	 *
	 * @param msg
	 *            the msg
	 */
	private void logMessage(final SOAPMessage msg) {
		try {
			// Write the message to the output stream
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			msg.writeTo(baos);
			baos.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * retorna una cadena XML contenida entre la etiqueta inicial y etiqueta final.
	 *
	 * @param xmlInicial
	 *            -- <ejemplo><nodo1><nodo2>Node to return</nodo2></nodo1></ejemplo>
	 * @param etiquetaInicio
	 *            -- <nodo2>
	 * @param etiquetaFin
	 *            -- </nodo2>
	 * @return -- <nodo2>Node to return</nodo2>
	 */
	private static String getTagXML(String xmlInicial, String etiquetaInicio, String etiquetaFin) {
		String resultado = "";
		try {
			if (xmlInicial != null && etiquetaInicio != null && etiquetaFin != null && !xmlInicial.isEmpty()
					&& !etiquetaInicio.isEmpty() && !etiquetaFin.isEmpty()) {
				int pos1 = xmlInicial.indexOf(etiquetaInicio);
				String cadena1 = xmlInicial.substring(pos1, xmlInicial.length());
				int pos2 = cadena1.indexOf(etiquetaFin);
				String cadena2 = cadena1.substring(0, pos2 + etiquetaFin.length());
				resultado = cadena2;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultado;
	}

	/**
	 * Convierte un String XML a objeto.
	 *
	 * @param objectClass
	 *            the object class
	 * @param xml
	 *            the xml
	 * @return the class
	 * @throws Exception
	 *             the exception
	 */
	private static Class<?> xmlToObject(Class<?> objectClass, String xml) throws Exception {
		Class<?> ap;

		try {
			java.io.StringReader reader = new java.io.StringReader(xml);
			JAXBContext context = JAXBContext.newInstance(objectClass);
			Unmarshaller un = context.createUnmarshaller();
			ap = (Class<?>) un.unmarshal(reader);
		} catch (JAXBException e) {
			ap = null;
			throw new JAXBException(e);
		} catch (Exception e) {
			ap = null;
			throw new Exception(e);
		}

		return ap;
	}

	/**
	 * Prints the formatted XML.
	 *
	 * @param message
	 *            the message
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String printFormattedXML(SOAPMessage message) throws Exception {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		message.writeTo(bout);
		String xml = bout.toString();

		Source xmlInput = new StreamSource(new StringReader(xml));
		StringWriter stringWriter = new StringWriter();
		StreamResult xmlOutput = new StreamResult(stringWriter);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setAttribute("indent-number", 5);
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(xmlInput, xmlOutput);
		String xmlString = xmlOutput.getWriter().toString();

		return xmlString;
	}

	private String printSOAPMessage(SOAPMessageContext messageContext) {
		String result = "";
		try {
			SOAPMessage msg = messageContext.getMessage();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			msg.writeTo(out);
			String strMsg = new String(out.toByteArray());
			result = prettyFormat(strMsg, 2);
		} catch (Exception ex) {
			logger.error("{}", ex);
		}
		return result;
	}

	private String prettyFormat(String input, int indent) {
		String result = "";
		try {
			Source xmlInput = new StreamSource(new StringReader(input));
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", indent);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(xmlInput, xmlOutput);
			result = xmlOutput.getWriter().toString();
		} catch (Exception e) {
			logger.error("Error in Handler-Soap => {}", e.getMessage());
		}
		return result;
	}

	/**
	 * Log message.
	 *
	 * @param msg
	 *            the msg
	 * @return the string
	 */
	private String getLogMessage(final SOAPMessage msg) {
		String xml = "";
		try {
			// Write the message to the output stream
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			msg.writeTo(baos);
			xml = baos.toString();
			baos.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return xml;
	}

	/**
	 * Gets the debug XML.
	 *
	 * @param xmlInicial
	 *            the xml inicial
	 * @param etiquetaInicio
	 *            the etiqueta inicio
	 * @param etiquetaFin
	 *            the etiqueta fin
	 * @return the debug XML
	 */
	public static String getDebugXML(String xmlInicial, String etiquetaInicio, String etiquetaFin) {
		String resultado = "";

		try {
			if (xmlInicial != null && etiquetaInicio != null && etiquetaFin != null && !xmlInicial.isEmpty()
					&& !etiquetaInicio.isEmpty() && !etiquetaFin.isEmpty()) {
				int pos1 = xmlInicial.indexOf(etiquetaInicio);
				String cadena1 = xmlInicial.substring(pos1, xmlInicial.length());
				int pos2 = cadena1.indexOf(etiquetaFin);
				String cadena2 = cadena1.substring(0, pos2 + etiquetaFin.length());

				resultado = cadena2;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultado;
	}

	/**
	 * Xml to object.
	 *
	 * @param objectClass
	 *            the object class
	 * @param xml
	 *            the xml
	 * @return the com.tigo.xmlns.responseheader.v 3 . response header
	 * @throws Exception
	 *             the exception
	 */
	public static com.tigo.xmlns.responseheader.v3.ResponseHeader xmlToObject2(Class<?> objectClass, String xml)
			throws Exception {
		ResponseHeader ap;

		try {
			java.io.StringReader reader = new java.io.StringReader(xml);
			JAXBContext context = JAXBContext.newInstance(objectClass);
			Unmarshaller un = context.createUnmarshaller();

			ap = (ResponseHeader) un.unmarshal(reader);

		} catch (JAXBException e) {
			ap = null;
			throw new JAXBException(e);
		} catch (Exception e) {
			ap = null;
			throw new Exception(e);
		}
		return ap;
	}

	public boolean convertSOAPFaultToResponseHeader(SOAPMessageContext context) {
		try {
			String xml = printSOAPMessage(context);
			String res = getDebugXML(xml, "<cmn:ResponseHeader xmlns:cmn=\"http://xmlns.tigo.com/ResponseHeader/V3\">",
					"</cmn:ResponseHeader>");
			ResponseHeader resHeader = xmlToObject2(ResponseHeader.class, res);

			context.getMessage().getSOAPPart().getEnvelope().getBody().detachNode();
			SOAPBody body = context.getMessage().getSOAPPart().getEnvelope().addBody();

			SOAPElement soapElementResponseBody = body.addChildElement("getPendingInvoicesQuantityResponse", "sch",
					"http://xmlns.tigo.com/Portability/PortabilityProcess/V1/schema");
			SOAPElement soapElementResponseHeader = soapElementResponseBody.addChildElement("ResponseHeader", "v3",
					"http://xmlns.tigo.com/ResponseHeader/V3");
			SOAPElement soapElementGeneralResponse = soapElementResponseHeader.addChildElement("GeneralResponse", "v3");
			SOAPElement soapElementCorrelationID = soapElementGeneralResponse.addChildElement("correlationID", "v3");
			soapElementCorrelationID.addTextNode(resHeader.getGeneralResponse().getCorrelationID() != null
					? resHeader.getGeneralResponse().getCorrelationID().getValue()
					: null);
			SOAPElement soapElementStatus = soapElementGeneralResponse.addChildElement("status", "v3");
			soapElementStatus.addTextNode(resHeader.getGeneralResponse().getStatus() != null
					? resHeader.getGeneralResponse().getStatus().getValue().toString()
					: null);
			SOAPElement soapElementCode = soapElementGeneralResponse.addChildElement("code", "v3");
			soapElementCode.addTextNode(resHeader.getGeneralResponse().getCode() != null
					? resHeader.getGeneralResponse().getCode().getValue()
					: null);
			SOAPElement soapElementCodeType = soapElementGeneralResponse.addChildElement("codeType", "v3");
			soapElementCodeType.addTextNode(resHeader.getGeneralResponse().getCodeType() != null
					? resHeader.getGeneralResponse().getCodeType().getValue()
					: null);
			SOAPElement soapElementDescription = soapElementGeneralResponse.addChildElement("description", "v3");
			soapElementDescription.addTextNode(resHeader.getGeneralResponse().getDescription() != null
					? resHeader.getGeneralResponse().getDescription().getValue()
					: null);
			logMessage(context.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
