package com.tigo.ea.porta.debitcut.service.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class WsAuthentication {

	private static final Logger loggerFile = LoggerFactory.getLogger(WsAuthentication.class);
	private String CLASS_NAME = this.getClass().getCanonicalName();
	private static final String CATEGORY = "service";
	
	@Autowired
	Environment env;

	@Autowired
	private LoggerUtil logger;
	
	public boolean validateCredentials() {
		HttpServletRequest context = HttpServletContextUtil.getHttpServletRequestContext();
		String username = context.getHeader(env.getProperty(AppConstants.AUTHENTICATION_NP_SP_USERNAME_KEY));
		String password = context.getHeader(env.getProperty(AppConstants.AUTHENTICATION_NP_SP_PASS_KEY));
		String methodName = "validateCredentials";
		String message = "Autenticaci�n by HttpHeaders NP-SP";
		String userSaved = env.getProperty(AppConstants.AUTHENTICATION_NP_SP_USERNAME_VALUE);
		String passSaved = env.getProperty(AppConstants.AUTHENTICATION_NP_SP_PASS_VALUE);
		
		Boolean result = false;
		if(username==null || password==null) {
			logger.error(CATEGORY, CLASS_NAME, methodName, message, "Usuario y/o contrase�a inv�lidas", 401L);
		}
		if ((username!=null && password!=null) && (username.equals(userSaved) && password.equals(passSaved))) {
			result = true;			
			logger.info(CATEGORY, null, CLASS_NAME, methodName, message, "Credenciales v�lidas y autenticaci�n correcta",
					true, "0", 0L);
		} else {
			logger.error(CATEGORY, CLASS_NAME, methodName, message, "Usuario y/o contrase�a inv�lidas", 401L);
		}
		return result;
	}
	
	@SuppressWarnings("unused")
	public String getCredentialsAuthorization() {
		String userSavedEncrypt = env.getProperty(AppConstants.AUTHENTICATION_SP_NP_USERNAME_VALUE);
		String passSavedEncrypt = env.getProperty(AppConstants.AUTHENTICATION_SP_NP_PASS_VALUE);
		String encodeAuthorizationSaved = env.getProperty(AppConstants.AUTHENTICATION_SP_NP_AUTHORIZATION);
		
		String token = String.format("%s:%s", userSavedEncrypt, passSavedEncrypt);
		String encodeAuthorizationCalculated = Base64.encodeBase64String(token.getBytes());
		String authorization = null;
		if (encodeAuthorizationSaved.equalsIgnoreCase(encodeAuthorizationCalculated)) {
			authorization = String.format("Basic %s", encodeAuthorizationCalculated);	
			loggerFile.info("Obteniendo credenciales para autenticaci�n de SP-NP");			
		} else {
			loggerFile.error("Error obteniendo credenciales para autenticaci�n de SP-NP");
		}
		return authorization;
	}
}