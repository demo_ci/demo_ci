package com.tigo.ea.porta.debitcut.service.config;

public class RunArgumentsaCastillo {
	public static void setupConfig() {
		StringBuilder sb = new StringBuilder();
		sb.append("9010").append(",");
		sb.append("tigo-ea-porta-debitcut-service").append(",");
		sb.append("production").append(",");
		sb.append("C:\\log-app").append(",");
		sb.append("C:\\devsoft\\Tigo-portability-2\\portability-config\\logback\\porta-debitcut-service.production-logback.xml").append(",");
		sb.append("C:\\devsoft\\Tigo-portability-2\\portability-config\\production\\porta-debitcut-service.production.yml").append(",");
		sb.append("GMT-04:00").append(",");
		sb.append("UTF-8");
				
		String[] arguments = sb.toString().split(",");
		System.setProperty("server.port", arguments[0]);
		System.setProperty("application.id", arguments[1]);
		System.setProperty("app.env", arguments[2]);
		System.setProperty("app.log", arguments[3]);
		System.setProperty("logging.config", arguments[4]);
		System.setProperty("spring.config.location", arguments[5]);
		System.setProperty("local.timezone", arguments[6]);
		System.setProperty("file.encoding", arguments[7]);
	}
}
