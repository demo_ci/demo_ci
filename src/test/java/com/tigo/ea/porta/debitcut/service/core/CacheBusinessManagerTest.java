package com.tigo.ea.porta.debitcut.service.core;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import com.tigo.ea.porta.debitcut.service.config.AppConfig;
import com.tigo.ea.porta.debitcut.service.config.RunArgumentsaCastillo;
import com.tigo.ea.porta.debitcut.service.core.CatalogueBusinessService;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.model.dto.SateliteDto;


@RunWith(SpringRunner.class)
@Import({ AppConfig.class })
@SpringBootTest
public class CacheBusinessManagerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(CacheBusinessManagerTest.class);
	
	@Autowired
	private Environment env;	
	@Autowired
	private CatalogueBusinessService catalogueBusinessService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RunArgumentsaCastillo.setupConfig();
	}	
	
	@Test
	public void testCacheBusinessManager() {
		try {
			run();
			logger.info("Catalogo satelites (sin refresh):");
			for (Map.Entry<Long, SateliteDto> pair: catalogueBusinessService.getCatalogueSatelite().entrySet()) {
			    logger.info("{} => {}", pair.getKey(), pair.getValue());
			}
			AppUtil.delay(10000L);
			Long id = 4L;
			logger.info("Consultamos un satelite (sin refresh): {}", id);
			SateliteDto satelite = catalogueBusinessService.getSatelite(id);
			logger.info("Salatelite encontrado: {}", satelite);  
			AppUtil.delay(20000L);
			id = 100L;
			logger.info("Consultamos un satelite (con refresh): {}", id);
			satelite = catalogueBusinessService.getCatalogueSatelite().get(id);
			logger.info("Salatelite encontrado: {}",satelite);			
			logger.info("Catalogo satelites (con refresh):");
			for (Map.Entry<Long, SateliteDto> pair: catalogueBusinessService.getCatalogueSatelite().entrySet()) {
			    logger.info("{} => {}", pair.getKey(), pair.getValue());
			}
			id = 4L;
			logger.info("Consultamos un satelite (con refresh): {}", id);
			satelite = catalogueBusinessService.getSatelite(id);
			logger.info("Salatelite encontrado: {}", satelite);  
		} catch (Exception e) {
			logger.error("Error en CacheBusinessManager junit",e);
		}
	}
	
	private void run() throws Exception {
		String applicationId = env.getProperty("application.id");
		String environment = env.getProperty("app.env");
		String version = env.getProperty("application.version");
		String app = String.format("%s-%s in %s", applicationId, version, environment);
		logger.info("{} Started CacheBusinessManager", app);
	}	
}