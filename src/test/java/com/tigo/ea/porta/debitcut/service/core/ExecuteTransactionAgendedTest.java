package com.tigo.ea.porta.debitcut.service.core;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import com.tigo.ea.porta.debitcut.service.config.AppConfig;
import com.tigo.ea.porta.debitcut.service.config.RunArgumentsaCastillo;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutSendASCPRequest;
import com.tigo.ea.porta.debitcut.service.dto.DebitCutSendASCPResponse;
import com.tigo.ea.porta.debitcut.service.dto.ExecuteService;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;
import com.tigo.ea.porta.debitcut.service.util.LoggerUtil;
import com.tigo.ea.porta.model.dao.Portrequest;
import com.tigo.ea.porta.model.repository.PortRequestRepository;

@RunWith(SpringRunner.class)
@Import({ AppConfig.class })
@SpringBootTest
public class ExecuteTransactionAgendedTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RunArgumentsaCastillo.setupConfig();
	}
	
	@Autowired
	@Qualifier("appUtilLocal")
	private AppUtil appUtil;
	
	@Autowired
	private LoggerUtil logger;
	
	@Autowired
	private Environment env;
	
	@Autowired 
	private PortRequestRepository portRequestRepository;
	
	@Autowired
	private ExecuteService executeService;
	
	@Test
	public void excecuteTransactions() {
		List<Portrequest> portrequestList = new ArrayList<>();
//		portrequestList = portRequestRepository.findRequestAgended();
		DebitCutSendASCPResponse response = null;
		DebitCutSendASCPRequest request = new DebitCutSendASCPRequest();
//		
		
		
		
	
			
		
//		System.out.println(portrequestList.toString());
	}
	
}
