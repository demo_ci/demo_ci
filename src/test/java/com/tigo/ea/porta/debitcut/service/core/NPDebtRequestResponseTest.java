package com.tigo.ea.porta.debitcut.service.core;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com._4gtss.npc.soap.service.consumer.NPDebtRequest1;
import com._4gtss.npc.soap.service.consumer.NPDebtRequestResponse;
import com._4gtss.npc.soap.service.consumer.NpDebtRequest;
import com._4gtss.npc.soap.service.consumer.NpcServiceImpl;
import com._4gtss.npc.soap.service.consumer.PhoneNumberList;
import com._4gtss.npc.soap.service.consumer.Range;
import com.tigo.ea.porta.debitcut.service.config.AppConfig;
import com.tigo.ea.porta.debitcut.service.config.RunArgumentsaCastillo;
import com.tigo.ea.porta.debitcut.service.util.AppUtil;


@RunWith(SpringRunner.class)
@Import({ AppConfig.class })
@SpringBootTest
public class NPDebtRequestResponseTest {

	@Autowired
	@Qualifier("appUtilLocal")
	private AppUtil appUtil;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RunArgumentsaCastillo.setupConfig();
	}
	
	@Autowired
	private NpcServiceImpl npcServiceImpl;
	
	@Test
	public void npDebtRequestResponse() {
		NpDebtRequest npDebtRequest = new NpDebtRequest();
		npDebtRequest.setOriginalNpOrderId("333441");
		npDebtRequest.setSenderId("8903");
		PhoneNumberList phone = new PhoneNumberList();
		Range ranges = new Range();
		ranges.setPhoneNumberEnd("76756434");
		ranges.setPhoneNumberStart("76756434");
		phone.getRanges().add(ranges);
		npDebtRequest.setPhoneNumberList(phone);
		
		NPDebtRequestResponse response = new NPDebtRequestResponse();
	
		NPDebtRequest1 request = new NPDebtRequest1();
		request.setArg0(npDebtRequest);
		
		JAXBContext jaxbContext;
		Marshaller jaxbMarshaller;
		String xmlString;
		StringWriter sw;
		try {
			jaxbContext = JAXBContext.newInstance(NpDebtRequest.class);
			jaxbMarshaller = jaxbContext.createMarshaller();
			sw = new StringWriter();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(npDebtRequest, sw);
			xmlString = sw.toString();
			System.out.print("request--------------->");
			System.out.print(xmlString);
			System.out.print("request--------------->");
			
			response = npcServiceImpl.npDebtRequest(request);
			
			jaxbContext = JAXBContext.newInstance(NPDebtRequestResponse.class);
			jaxbMarshaller = jaxbContext.createMarshaller();
			sw = new StringWriter();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(response , sw);
			xmlString = sw.toString();
			System.out.print("response--------------->");
			System.out.print(xmlString);
			System.out.print("response--------------->");
		}catch(Exception e) {
			e.printStackTrace();
		}

	}
}
